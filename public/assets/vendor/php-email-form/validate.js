/**
* PHP Email Form Validation - v2.3
* URL: https://bootstrapmade.com/php-email-form/
* Author: BootstrapMade.com
*/
!(function($) {
  "use strict";

  $('form.php-email-form').submit(function(e) {
    e.preventDefault();
    
    var f = $(this).find('.form-group'),
      ferror = false,
      ferror1 = false,
      emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;

    f.children('input').each(function() { // run all inputs
     
      var i = $(this); // current input
      var rule = i.attr('data-rule');
      var rule1 = i.attr('data-rule1');
      // console.log(i, rule, rule1)

      // first rule of input
      if (rule !== undefined) {
        var ierror = false; // error flag for current input
        var pos = rule.indexOf(':', 0);
        if (pos >= 0) {
          var exp = rule.substr(pos + 1, rule.length);
          rule = rule.substr(0, pos);
        } else {
          rule = rule.substr(pos + 1, rule.length);
        }
        
        switch (rule) {
          case 'required':
            if (i.val().trim() === '') {
              ferror = ierror = true;
            }
            break;

          case 'minlen':
            if (i.val().trim().length < parseInt(exp)) {
              ferror = ierror = true;
            }
            break;
          
          case 'minlenWithoutRequired':
            if (i.val().length > 1 && i.val().length < parseInt(exp)) {
              ferror = ierror = true;
            }
            break;

          case 'email':
            if (!emailExp.test(i.val())) {
              ferror = ierror = true;
            }
            break;

          case 'checked':
            if (! i.is(':checked')) {
              ferror = ierror = true;
            }
            break;

          case 'regexp':
            exp = new RegExp(exp);
            if (!exp.test(i.val())) {
              ferror = ierror = true;
            }
            break;
        }
        i.next('.validate').html((ierror ? (i.attr('data-msg') !== undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');
      }

      // second rule of input
      if (rule1 !== undefined) {
        var ierror1 = false; // error flag for current input
        var pos1 = rule1.indexOf(':', 0);
        if (pos1 >= 0) {
          var exp1 = rule1.substr(pos1 + 1, rule1.length);
          rule1 = rule1.substr(0, pos1);
        } else {
          rule1 = rule1.substr(pos1 + 1, rule1.length);
        }
        
        switch (rule1) {
          case 'maxyear':
            let current_year = new Date().getFullYear();
            if (i.val()*1 > current_year*1) {
              ferror1 = ierror1 = true;
            }
            break;
        }
        i.parent().find('.validate1').html((ierror1 ? (i.attr('data-msg1') !== undefined ? i.attr('data-msg1') : 'wrong Input') : '')).show('blind');
      }

    });
    f.children('textarea').each(function() { // run all inputs

      var i = $(this); // current input
      var rule = i.attr('data-rule');

      if (rule !== undefined) {
        var ierror = false; // error flag for current input
        var pos = rule.indexOf(':', 0);
        if (pos >= 0) {
          var exp = rule.substr(pos + 1, rule.length);
          rule = rule.substr(0, pos);
        } else {
          rule = rule.substr(pos + 1, rule.length);
        }

        switch (rule) {
          case 'required':
            if (i.val() === '') {
              ferror = ierror = true;
            }
            break;

          case 'minlen':
            if (i.val().length < parseInt(exp)) {
              ferror = ierror = true;
            }
            break;
        }
        i.next('.validate').html((ierror ? (i.attr('data-msg') != undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');
      }
    });
    if (ferror) return false;
    if (ferror1) return false;

    var this_form = $(this);
    var action = $(this).attr('action');

    if( ! action ) {
      this_form.find('.loading').slideUp();
      this_form.find('.error-message').slideDown().html('The form action property is not set!');
      return false;
    }
    
    this_form.find('.sent-message').slideUp();
    this_form.find('.error-message').slideUp();
    this_form.find('.loading').slideDown();

    if ( $(this).data('recaptcha-site-key') ) {
      var recaptcha_site_key = $(this).data('recaptcha-site-key');
      grecaptcha.ready(function() {
        grecaptcha.execute(recaptcha_site_key, {action: 'php_email_form_submit'}).then(function(token) {
          php_email_form_submit(this_form,action,this_form.serialize() + '&recaptcha-response=' + token);
        });
      });
    } else {
      php_email_form_submit(this_form,action,this_form.serialize());
    }
    
    return true;
  });

  function php_email_form_submit(this_form, action, data) {
    $.ajax({
      type: "POST",
      url: action,
      data: data,
      timeout: 40000,
      beforeSend:  function() {				
        $(".btn, input, select, textarea").prop( "disabled", true );
      }
    }).done( function(msg){
      console.log(msg)
      $(".btn, input, select, textarea").prop( "disabled", false );
      this_form.find('.loading').slideUp();

      if (msg.status == 200) {
        this_form.find('.sent-message').slideDown();
        this_form.find("input:not(input[type=submit]), textarea").val('');
        // $('#myToast').toast({delay: 5000}); $("#myToast").toast('show');
        // redirect for estimate page
        if(action.search("estimate")*1 > -1) {
            var refresh = window.location.protocol + "//" + window.location.host + "/scope-of-work/" + msg.id;    
            window.location.href = refresh;
        } // end redirect
      } else {
        if(msg.status==400) {
          for (let property in msg.message) {
              this_form.find('.error-message').slideDown().html(msg.message[property]);
          }
        } else {
          if(!msg) {
            msg = 'Form submission failed and no error message returned from: ' + action + '<br>';
          }
          this_form.find('.error-message').slideDown().html(msg);
        }
      } 
      setTimeout(function(){ this_form.find('.sent-message').slideUp(); }, 5000);
    }).fail( function(data){
      console.log(data);
      var mess_err = "";
      try {
        mess_err = JSON.parse(data.responseText).message;
      } catch(e) {
        mess_err = 'Unknown Error';
      }
      this_form.find('.loading').slideUp();
      this_form.find('.error-message').slideDown().html(mess_err);
      $(".btn, input, select, textarea").prop( "disabled", false );
    });
  }

})(jQuery);
