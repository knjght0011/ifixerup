<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CategoriesController;
use App\Http\Controllers\API\GroupController;
use App\Http\Controllers\API\ItemsController;
use App\Http\Controllers\API\MapController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\PropertyGroupController;
use App\Http\Controllers\API\SearchController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login', [AuthController::class,'login']);

//Route::middleware(['auth:sanctum'])->group(function () {
//    Route::get('/users', );
//});
Route::group(['prefix'=>'items','as'=>'items.'],function(){
    Route::get('lists', [ItemsController::class,'getLists'])->name('lists');
    Route::get('detail-item-type', [ItemsController::class,'detailItemType'])->name('detailItemType');
});
Route::group(['prefix'=>'products','as'=>'products.'],function(){
    Route::get('lists', [ProductController::class, 'getLists'])->name('lists');
});
Route::group(['prefix'=>'categories','as'=>'categories.'],function(){
    Route::get('lists', [CategoriesController::class, 'getLists'])->name('lists');
});
Route::group(['prefix'=>'group','as'=>'group.'],function(){
    Route::get('lists', [GroupController::class, 'getLists'])->name('lists');
    Route::get('detail/{id}', [GroupController::class, 'getItems'])->name('detail');
});
Route::group(['prefix'=>'property','as'=>'property.'],function(){
    Route::post('addflow', [PropertyGroupController::class, 'creatNewWalk'])->name('addwalk');
    //Route::get('addflow', [PropertyGroupController::class, 'creatNewWalk'])->name('addwalk');
	Route::post('addgroup', [PropertyGroupController::class, 'addGroupToWalk'])->name('addGroupToWalk');
	//Route::get('addgroup', [PropertyGroupController::class, 'addGroupToWalk'])->name('addGroupToWalk');
	Route::get('loaditem', [PropertyGroupController::class, 'loadItem'])->name('loadItem');
	Route::get('loaditemdesc', [PropertyGroupController::class, 'loadItemDesc'])->name('loadItemDesc');
	Route::get('loadProduct', [PropertyGroupController::class, 'loadProduct'])->name('loadProduct');
	Route::post('updateqty', [PropertyGroupController::class, 'updateQty'])->name('updateQty');
	Route::post('updatelabor', [PropertyGroupController::class, 'updateLabor'])->name('updateLabor');
	Route::post('approveditem', [PropertyGroupController::class, 'approvedItem'])->name('approvedItem');
	Route::post('unApprovedItem', [PropertyGroupController::class, 'unApprovedItem'])->name('unApprovedItem');
	Route::post('deleteitem', [PropertyGroupController::class, 'deleteItem'])->name('deleteItem');
	Route::post('additem', [PropertyGroupController::class, 'addItem'])->name('addItem');
	Route::post('updateitem', [PropertyGroupController::class, 'updateItem'])->name('updateItem');
	Route::post('addcomment', [PropertyGroupController::class, 'addComment'])->name('addComment');
	Route::post('updatemeasurement', [PropertyGroupController::class, 'updateMeasurement'])->name('updateMeasurement');
	Route::post('addmeasure', [PropertyGroupController::class, 'addMeasure'])->name('addMeasure');
	Route::post('delmeasurement', [PropertyGroupController::class, 'delMeasurement'])->name('delMeasurement');

	Route::get('quick-review',[PropertyGroupController::class,'quickReview'])
    ->name('quickReview');
	Route::get('budget',[PropertyGroupController::class,'budget'])
    ->name('budget');


	//Route::get('additem', [PropertyGroupController::class, 'addItem'])->name('addItem');//test

	//Route::get('updateItemPrice', [PropertyGroupController::class, 'updateItemPrice'])->name('updateItemPrice'); //test
});
Route::group(['prefix'=>'maps','as'=>'api.map.'],function(){
   Route::get('search',[SearchController::class,'search'])->name('search');
});


















