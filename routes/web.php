<?php

use App\Http\Controllers\Admin\GroupController;
use App\Http\Controllers\Admin\HVI\AllHome\MetroController;
use App\Http\Controllers\Admin\HVI\AllHome\ZipCodeController;
use App\Http\Controllers\Admin\HVI\HomeValueIndexController;
use App\Http\Controllers\Admin\HVIAllHomeMetroController;
use App\Http\Controllers\Admin\iValuationController as AdminiValuationController;
use App\Http\Controllers\Admin\PropertyController;
use App\Http\Controllers\Admin\Voyager\UserController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\UserLoginController;
use App\Http\Controllers\CrawController;
use App\Http\Controllers\iValuationController;
use App\Http\Controllers\User\Auth\EstimateController;
use App\Http\Controllers\User\Auth\ProfileController;
use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\User\SearchController;
use Goutte\Client;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get(
        's', function (\Illuminate\Http\Request $request) {
    $site = 'https://www.zillow.com/search/GetSearchPageState.htm?searchQueryState={"pagination":{},"usersSearchTerm":"29014 42nd Ave S Auburn, WA 98001","mapBounds":{"west":-122.28265732526779,"east":-122.27935284376144,"south":47.34055668528492,"north":47.34151998871271},"isMapVisible":true,"filterState":{"sortSelection":{"value":"globalrelevanceex"},"isAllHomes":{"value":true}},"isListVisible":true,"mapZoom":18}&wants={"cat1":["listResults","mapResults"],"cat2":["total"]}&requestId=3';

    $crawler = \Illuminate\Support\Facades\Http::get($site);
//    $client = new Client();
//    $crawler = $client->request('GET', $site);
    dd($crawler->collect()->toArray());
}
);
Route::group(['prefix' => 'craw'], function () {
    Route::get('category', [CrawController::class, 'category']);
    Route::get('sub-category/{code}', [CrawController::class, 'subCategory']);
    Route::get('get-product/{code}', [CrawController::class, 'getProductByCategory']);
});
Route::group(['prefix' => 'ivaluation'], function () {
    Route::get('step-1', [iValuationController::class, 'category']);
});
Route::get('/', 'App\Http\Controllers\User\HomeController@index')->name('home');
Route::post('/contact', 'App\Http\Controllers\User\HomeController@contact')->name('contact');
Route::get('/direct', 'App\Http\Controllers\User\HomeController@direct')->name('direct');
Route::get('/about-us', 'App\Http\Controllers\User\HomeController@getListTeams')->name('aboutus');
Route::get('blogs/{title}/{id}', [HomeController::class, 'blogs'])->name('blogs');
Route::get('/our-work', 'App\Http\Controllers\User\HomeController@getListProjects')->name('project');
Route::get('/scope-of-work/{id}', 'App\Http\Controllers\User\HomeController@getProperty')->name('property.get');
Route::get(
        'estimate', function () {
    return view('landingpage.estimate.index');
}
);
Route::post('estimate', [HomeController::class, 'estimate'])->name('estimate');
Route::put('update-property/{id}', [HomeController::class, 'updateProperty'])->name('updateProperty');

Route::put('update-property-image/{id}', [HomeController::class, 'updatePropertyImage'])->name('updatePropertyImage');
Route::post('add-property-image/{id}', [HomeController::class, 'addPropertyImage'])->name('addPropertyImage');
Route::get('delete-property-image/{id}', [HomeController::class, 'deletePropertyImage'])->name('deletePropertyImage');

Route::group(['prefix' => 'ivaluation', 'as' => 'ivaluation.'], function () {
    Route::get('/', function () {
        return view('landingpage.valuation.index');
    })->name('ivaluation');
//    Route::get('step-1', [iValuationController::class, 'step1'])
//            ->name('step1');
//    Route::get('step-2', [iValuationController::class, 'step2'])
//            ->name('step2');
    Route::get('calculate', [iValuationController::class, 'calculate'])->name('calculate');
    Route::get('result/{id}', [iValuationController::class, 'result'])->name('result');

});


Route::prefix('')->group(
        function () {
            Route::get('register', [RegisterController::class, 'showRegistrationForm'])
                    ->name('user.register');
            Route::post('register', [RegisterController::class, 'register'])
                    ->name('user.register');
            Route::get('login', [UserLoginController::class, 'showLoginForm'])->name('user.login');
            Route::post('login', [UserLoginController::class, 'login'])->name('user.login.submit');
            Route::get('logout', [UserLoginController::class, 'logout'])->name('user.logout');
            Route::post('logout', [UserLoginController::class, 'logout'])->name('user.logout.submit');
        }
);

Route::group(['middleware' => ['customer']], function () {
    Route::group(
            ['prefix' => 'profile'], function () {
        Route::get('/', [ProfileController::class, 'index'])->name('user.profile');
        Route::post('update-profile', [ProfileController::class, 'update'])->name(
                'user.profile.update'
        );
        Route::post('update-password', [ProfileController::class, 'update'])->name(
                'user.password.update'
        );
    });
    Route::get('list-estimate', [EstimateController::class, 'index'])->name('user.list-estimate');
    Route::get('estimate/{id}', [EstimateController::class, 'walkProperty'])
            ->name('user.walkProperty');
    Route::get('walked/{id}', [EstimateController::class, 'walkedProperty'])
            ->name('user.walkedProperty');
    Route::get('settings', [ProfileController::class, 'logout'])->name('user.settings');
});
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('search', [SearchController::class, 'index'])
        ->name('search');

Route::group(
        ['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Voyager::routes();

    Route::group(
            [], function () {
        Route::resource('gift', PropertyController::class)->names('admin.gift');

        Route::get('hvi', [MetroController::class, 'index'])->name('admin.hvi');
        Route::get('ivaluation', [AdminiValuationController::class, 'index'])->name('admin.ivaluation');

        Route::put('update-property-image/{id}', [PropertyController::class, 'updatePropertyImage'])
                ->name('voyager.property.updatePropertyImage');
        Route::post('add-property-image/{id}', [PropertyController::class, 'addPropertyImage'])
                ->name('voyager.property.addPropertyImage');
        Route::get('delete-property-image/{id}', [PropertyController::class, 'deletePropertyImage'])
                ->name('voyager.property.deletePropertyImage');

        Route::post('property/create-walk/{id}', [PropertyController::class, 'createWalk'])
                ->name('voyager.property.createWalk');

        Route::get('property/walked/{id}', [PropertyController::class, 'walked'])
                ->name('voyager.property.walked');
        Route::get('property/export/{id}', [PropertyController::class, 'export'])
                ->name('voyager.property.export');

        Route::get('home-value-index-all-home-metro', [MetroController::class, 'index'])
                ->name('admin.hvi.allhome.metro.index');

        Route::get('home-value-index-all-home-zipcode', [ZipCodeController::class, 'index'])
                ->name('admin.hvi.allhome.zipcode.index');
        Route::group(
                ['prefix' => 'home-value-index', 'as' => 'admin.hvi.'], function () {
            Route::group(
                    ['prefix' => 'all-home', 'as' => 'allhome.'], function () {
                Route::get('metro/view/{id}', [MetroController::class, 'view'])
                        ->name('metro.view');
                Route::post('metro/import', [MetroController::class, 'import'])
                        ->name('metro.import');


                Route::get('zipcode/view/{id}', [ZipCodeController::class, 'view'])
                        ->name('zipcode.view');
                Route::post('zipcode/import', [ZipCodeController::class, 'import'])
                        ->name('zipcode.import');
            }
            );
        }
        );

//    Route::resource('group', GroupController::class);

        Route::get('profile', [UserController::class, 'profile'])
                ->name('voyager.profile');
    }
    );
}
);
