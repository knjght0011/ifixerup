<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Gate::define('browse_home-value-index-all-home-metro', function ($user) {
            return $user->hasPermission('browse_home-value-index-all-home-metro');
        });
        Gate::define('browse_home-value-index-all-home-zipcode', function ($user) {
            return $user->hasPermission('browse_home-value-index-all-home-zipcode');
        });
        Gate::define('browse_hvi', function ($user) {
            return $user->hasPermission('browse_hvi');
        });
        Gate::define('browse_customer', function ($user) {
            return $user->hasPermission('browse_customer');
        });
    }
}
