<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
//        \URL::forceScheme('https');
        Paginator::defaultView('vendor.pagination.bootstrap-4');
        Validator::extend('smaller_than_current_year', function($attribute, $value, $parameters, $validator) {
                return $value <= Carbon::now()->year;
        });
        Validator::extend('bigger_than_current_date', function($attribute, $value, $parameters, $validator) {
                return Carbon::now()->diffInDays($value,false)>0 ;
        });
    }
}
