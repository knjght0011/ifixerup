<?php

namespace App\Exports\Property;

use App\Http\Controllers\API\PropertyGroupController;
use App\Models\Group;
use App\Models\PropertyGroupItemWalk;
use App\Models\PropertyGroupWalk;
use App\Models\PropertyProduct;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportPropertyWalk implements FromView
{


    protected $property,$exportType;
    public function __construct($property,$exportType='property_excel')
    {
        $this->property = $property;
        $this->exportType = $exportType;
    }
    public function view(): View
    {
        $propertyId = $this->property->id;

        if($this->exportType=='approved_items' ||$this->exportType=='approved_items_pdf')
            $groups = PropertyGroupItemWalk::where('property_id',$propertyId)
                ->where('approved',1)
                ->groupBy('group_walk_id')
                ->pluck('group_walk_id');
        else
            $groups = PropertyProduct::where('property_id', $propertyId)
                ->where('qty', '>', 0)
                ->groupBy('group_walk_id')
                ->pluck('group_walk_id');
        $summary = [
            'measuredSQFT' => (new PropertyGroupController())->calculateMeasure($propertyId),
            'totalPrice'   => PropertyGroupItemWalk::where('property_id', $propertyId)
                ->sum('price'),
        ];
        $result = [];
        foreach ($groups as $group) {
            $groupWalk = Group::find(PropertyGroupWalk::findOrFail($group)->group_id)->toArray();
            $groupWalk['measure'] = (new PropertyGroupController())->calculateMeasure($propertyId, $group);
            $products = PropertyProduct::where('group_walk_id', $group)
                ->where('property_id', $propertyId)
                ->join('products', 'products.id', '=', 'product_id')
                ->where('qty', '>', 0)
                ->get()->toArray();
//            dd($products);
            $customItems =  PropertyGroupItemWalk::where('group_walk_id', $group)
                ->where('property_id', $propertyId)
                ->whereNull('item_id')
                ->where('price', '>', 0)
                ->get()->toArray();
            $result[] = [
                'group'    => $groupWalk,
                'products' => $products,
                'customItems'=>$customItems
            ];
        }

        return view('vendor.voyager.property.export.property_walk')
            ->with(['property'=>$this->property,'result'=>$result,'summary'=>$summary]);
    }

}
