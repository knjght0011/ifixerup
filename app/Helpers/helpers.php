<?php

use Illuminate\Support\Carbon;

if (!function_exists('morphDate')) {
    function morphDate($timestamp, $format = 'm/d/Y H:i:s'): string
    {
        if ($timestamp instanceof Carbon) {
            return $timestamp->format($format);
        } else {

            return $timestamp;
        }
    }
}
if (!function_exists('getUserTimeZone')) {
    function getUserTimeZone(): string
    {
        if (session()->get('timezone') && session()->get('timezone') != '') {
            return session()->get('timezone');
        }
        $ip = env('APP_ENV') == 'local' ? '27.64.56.64' : $_SERVER['REMOTE_ADDR'];

        $ipInfo = file_get_contents(
                'http://ip-api.com/json/'.$ip);
        $ipInfo = json_decode($ipInfo);
        $timezone = $ipInfo->timezone;
        $date = Carbon::now($timezone);

        return $date->format('P');
    }
}
if (!function_exists('convertSystemTzToUserTz')) {
    function convertSystemTzToUserTz($timestamp, $format = 'Y-m-d H:i:s', $resultFormat = 'm/d/Y H:i:s'): ?string
    {
        try {
            $date = \Carbon\Carbon::createFromFormat($format, $timestamp);
            return $date->timezone(getUserTimeZone())->format($resultFormat);
        } catch (\Exception $e) {
            return $timestamp;
        }

    }
}
if (!function_exists('formatCurrency')) {
    /**
     * @param $number
     *
     * @return bool
     */
    function formatCurrency($number, $decimal=NAN)
    {
        if (is_numeric($number)) {
            return (env('CURRENCY_PREFIX', '$').
                    number_format($number, $decimal!=NAN?$decimal:env('CURRENCY_DECIMAL', 2)).env(
                            'CURRENCY_SUFFIX', ''
                    ));
        }

        return $number;
    }
}

