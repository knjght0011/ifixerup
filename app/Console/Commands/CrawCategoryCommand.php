<?php

namespace App\Console\Commands;

use App\Http\Controllers\CrawController;
use App\Models\ProductCategory;
use Illuminate\Console\Command;

class CrawCategoryCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'craw:category';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (ProductCategory::where('depth',1)->get() as $category) {
            $craw = new CrawController();
            $craw->subCategory($category->code);
        }
        return 0;
    }
}
