<?php

namespace App\Console\Commands;

use App\Http\Controllers\CrawController;
use App\Models\ProductCategory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CrawProductCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'craw:product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (ProductCategory::where('depth',3)
                     ->whereNull('passed')->get() as $category) {
            $category->products()->delete();
            $craw = new CrawController();
            $craw->getProductByCategory($category->code);
            $category->update(['passed'=>1]);
//            sleep(10);
            Log::info('Done '.$category->code.' - '.$category->name.' - '.$category->id);
        }
        return 0;
    }
}
