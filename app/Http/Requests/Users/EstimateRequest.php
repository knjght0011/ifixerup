<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class EstimateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_name'    => 'string|max:255|nullable',
            'address'         => 'string|max:255',
            'city'            => 'string|max:255',
            'state'           => 'string|max:255',
            'zip_code'        => 'numeric|max:99999|min:1',
            'county'          => 'string|max:255|nullable',
            'main_square_ft'  => 'numeric|max:999999|min:0|nullable',
            'number_bedrooms' => 'numeric|max:999999|min:0|nullable',
            'year_build'      => 'numeric|min:1900|nullable|smaller_than_current_year',
            'due_date'        => 'nullable|bigger_than_current_date',
            'gate_code'       => 'string|max:10|nullable',
            'lock_box'        => 'string|max:10|nullable',
            'comments'        => 'string|max:255|nullable',
            'images.*' => 'nullable|mimes:jpg,jpeg,png,bmp'


        ];
    }

    public function messages()
    {
        return
            [
                'smaller_than_current_year' => Str::title(':attribute').' must be smaller than the current year.',
                'bigger_than_current_date'  => ':attribute must be greater than the current date.',
                'zip_code.min'=>'The zip code must be at least 0xxxx.',
                'images.*.mimes' => 'Only jpeg,png and bmp images are allowed'
            ];
    }

}
