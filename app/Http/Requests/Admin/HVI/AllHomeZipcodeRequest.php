<?php

namespace App\Http\Requests\Admin\HVI;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AllHomeZipcodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() && Auth::user()->role_id== Role::getRoleIdByName('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'import'=>[
                'required',
                'file',
                'mimetypes:text/csv,text/plain,application/csv,text/comma-separated-values,text/anytext
                ,application/octet-stream,application/txt,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,
                application/vnd.ms-excel'
            ]
        ];
    }

    public function messages()
    {
        return ['mimetypes'=>'Import file must be CSV or XLSX file.'];
    }
}
