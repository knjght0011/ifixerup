<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('voyager.login');
        }
    }
    public function handle($request, Closure $next, ...$guards)
    {
        if(
            ($request->routeIs('user.login')
                || $request->routeIs('user.login.submit')
            )
            && Auth::guest())
        {

            return $next($request);
        }
        else
        {
            if(Auth::user()) {
                if (Auth::user()->active == User::DEACTIVATED) {
                    Auth::logout();

                    return redirect(route('user.login'))->withErrors('Your account has been deactivated.');
                }
                else
                    return $next($request);
            }
            else
            {
                return redirect()->to(route('user.login'))
                    ->withErrors(
                        'Please login to access this content. You are not logged in or the login session has expired!');
            }
        }
    }
}
