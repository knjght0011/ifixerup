<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('voyager.login');
        }
    }
    public function handle($request, Closure $next, ...$guards)
    {
        if(
            ($request->routeIs('voyager.login')
                || $request->routeIs('voyager.postlogin')
                || $request->routeIs('voyager.dashboard')
                || $request->routeIs('voyager.voyager_assets')
            )
            && Auth::guest())
        {

            return $next($request);
        }
        else
        {
            if(Auth::user()) {
                if (Auth::user()->active == User::DEACTIVATED) {
                    Auth::logout();

                    return redirect(route('voyager.login'))->withErrors('Your account has been deactivated.');
                }
                else
                    return $next($request);
            }
            else
            {
                return redirect()->to(route('voyager.login'))
                    ->withErrors(
                        'Please login to access this content. You are not logged in or the login session has expired!');
            }
        }
    }
}
