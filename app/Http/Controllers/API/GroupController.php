<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\GroupItem;
use App\Models\ItemsModel;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function getLists(Request $request)
    {
        $item = Group::orderBy('name', 'asc');
        if ($request->type) {
            $item = $item->where('group_type_id', $request->type);
        }
        if ($request->search) {
            $item = $item->where('name', 'like', "%$request->search%");
        }
		if ($request->items) {
            $item = $item->whereNotIn('id', explode(',', $request->items));
        }
        $item = $item->pluck('name','id');

        return response()->json(['result'=>true,'data' => $item]);
    }

    public function getItems($groupId,Request $request)
    {
        $group = Group::find($groupId);
        if(!$group)
            return response()->json(['messages'=>'Group Not Found'],400);
        $items = GroupItem::join('items','item_id','=','items.id')
            ->where('group_id',$group->id);
        if($request->search)
            $items = $items->where('name', 'like', "%$request->search%");
        $items = $items->get();
        return response()->json(['result'=>true,'data'=>$items]);
    }
}
