<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $endpoint = "https://maps.googleapis.com/maps/api/place/autocomplete/json?components=country:us&key="
            .setting('site.map_api_key')."&input=";
        $client = Http::get($endpoint.$request->search);
        $result = [];
        if($client->status()==200) {

            if($client->collect()->get('status')=='OK')
           {
               $i=0;
               foreach ($client->collect()->get('predictions') as $item) {
                   $result[urlencode($item['description'])]=$item['description'];
                   $i++;
                   if($i==4)
                       break;
               }
           }
        }
        return response()->json(['status'=>'success','data'=>$result]);


    }
}
