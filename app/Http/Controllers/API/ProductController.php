<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function getLists(Request $request)
    {
        $product = Product::orderBy('id','desc')->orderBy('name','asc');
        if($request->sku)
            $product = $product->where('SKU','like',"%$request->sku%");
        if($request->category)
        {
            $category = ProductCategory::find($request->category);
            if($category->depth==1)
            {
                $subCategory=$category->subCategory->map(function($subCat){
                   return $subCat->subCategory->map(function($lastCat){
                       return $lastCat->id;
                   });
                });
                $categories = (call_user_func_array('array_merge',$subCategory->toArray()));
            }
            elseif($category->depth==2)
            {
                $subCategory=$category->subCategory->map(function($subCat){
                        return $subCat->id;
                });
                $categories = $subCategory->toArray();
            }
            else
                $categories = [$request->category];
            $product = $product->whereIn('category_id',$categories);
        }
        if($request->name)
            $product = $product->where('name','like',"%$request->name%");
        if($request->get('except') && $except=explode(',',$request->get('except')))
            $product = $product->whereNotIn('id',$except);

        if($request->limit)
            $product = $product->take($request->limit)->get();
        else
            $product = $product->get();

        return response()->json(['data'=>$product]);
    }
}
