<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function getLists(Request $request)
    {
        $categories = ProductCategory::orderBy('id','desc')->orderBy('name','asc');
        if($request->search)
            $categories = $categories->where('name','like',"%$request->search%");


        if($request->select2) {
            $categories = $categories->pluck('name', 'id');
        }
        else {
            $categories = $categories->get();
        }

        return response()->json(['data'=>$categories]);
    }
}
