<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\GroupItem;
use App\Models\ItemDescription;
use App\Models\ItemProduct;
use App\Models\ItemsModel;
use App\Models\Property;
use Illuminate\Http\Request;
use App\Models\Flow;
use App\Models\FlowGroupItem;
use App\Models\PropertyGroup;
use App\Models\PropertyGroupWalk;
use App\Models\PropertyGroupItem;
use App\Models\PropertyGroupItemWalk;
use App\Models\PropertyProduct;
use App\Models\PropertyGroupMeasure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PropertyGroupController extends Controller
{
    public function loadItemDesc(Request $request)
    {
        $resultItems = ItemDescription::where('item_id', $request->id)->get();

        return response()->json(
            ['data' => 'success', 'items' => $resultItems]
        );
    }

    public function creatNewWalk(Request $request)
    {
        // copy group of end flow to prperty */
        //$request->id = 11;
        $end_id_flow = Flow::max('id');
        $get_groups = FlowGroupItem::join('group', 'flow_group_items.group_id', '=', 'group.id')->where(
            'flow_id', $end_id_flow
        )->get()->toArray();
        if (count($get_groups) > 0) {
            foreach ($get_groups as $item) {
                /*PropertyGroup::insert([
                    'property_id' => $request->id,
                    'group_id' => $item['group_id']
                ]);

                //per group: copy items to property
                $get_items = GroupItem::where('group_id',$item['group_id'])->get()->toArray();
                if(count($get_items) > 0){
                    foreach($get_items as $ite){
                        PropertyGroupItem::insert([
                            'property_id' => $request->id,
                            'group_id' => $item['group_id'],
                            'item_id' => $ite['item_id']
                        ]);
                    }
                }
                */
                // clone group from flow
                $group_walk_id = PropertyGroupWalk::insertGetId(
                    [
                        'property_id'       => $request->id,
                        'group_id'          => $item['group_id'],
                        'maximum_per_house' => $item['maximum_per_house'],
                        'flooring'          => $item['flooring'],
                        'paint'             => $item['paint'],
                        'comment'           => $item['comment'],
                    ]
                );
                // clone item of group (in flow)
                //$get_itemss = PropertyGroupItem::where('group_id',$request->id)->where('property_id',$request->property_id)->get()->toArray();
                $get_itemss = GroupItem::where('group_id', $item['group_id'])->get()->toArray();
                if (count($get_itemss) > 0) {
                    foreach ($get_itemss as $ite) {
                        PropertyGroupItemWalk::insert(
                            [
                                'property_id'   => $request->id,
                                //'group_id' => $request->id, // dư
                                'item_id'       => $ite['item_id'],
                                'group_walk_id' => $group_walk_id,
                            ]
                        );
                        /*clone products in item to walk*/
                        $get_products = ItemProduct::where('item_id', $ite['item_id'])->get()->toArray();
                        if (count($get_products) > 0) {
                            foreach ($get_products as $it) {
                                PropertyProduct::insert(
                                    [
                                        'property_id'   => $request->id,
                                        'item_id'       => $ite['item_id'],
                                        'group_walk_id' => $group_walk_id,
                                        'product_id'    => $it['product_id'],
                                    ]
                                );
                            }
                        }
                    }
                }
                /*item measurement*/
                if ($item['flooring'] == 1) {
                    PropertyGroupMeasure::insert(
                        [
                            'property_id'   => $request->id,
                            'group_walk_id' => $group_walk_id,
                        ]
                    );
                }
            }

            return response()->json(['data' => 'success']);
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function addGroupToWalk(Request $request)
    {
        // $request->id = 26;
        // $request->property_id = 11;
        if ($request->id && $request->property_id) {
            $get_group = Group::where('id', $request->id)->first();
            $group_walk_id = PropertyGroupWalk::insertGetId(
                [
                    'property_id'       => $request->property_id,
                    'group_id'          => $request->id,
                    'maximum_per_house' => $get_group['maximum_per_house'],
                    'flooring'          => $get_group['flooring'],
                    'paint'             => $get_group['paint'],
                    'comment'           => $get_group['comment'],
                ]
            );
            /*clone item of group */
            //$get_items = PropertyGroupItem::where('group_id',$request->id)->where('property_id',$request->property_id)->get()->toArray();
            $get_items = GroupItem::where('group_id', $request->id)->get()->toArray();
            if (count($get_items) > 0) {
                foreach ($get_items as $ite) {
                    PropertyGroupItemWalk::insert(
                        [
                            'property_id'   => $request->property_id,
                            //'group_id' => $request->id, // dư
                            'item_id'       => $ite['item_id'],
                            'group_walk_id' => $group_walk_id,
                        ]
                    );
                    /*clone products in item to walk*/
                    $get_products = ItemProduct::where('item_id', $ite['item_id'])->get()->toArray();
                    if (count($get_products) > 0) {
                        foreach ($get_products as $it) {
                            PropertyProduct::insert(
                                [
                                    'property_id'   => $request->property_id,
                                    'item_id'       => $ite['item_id'],
                                    'group_walk_id' => $group_walk_id,
                                    'product_id'    => $it['product_id'],
                                ]
                            );
                        }
                    }
                }
            }
            /*item measurement*/
            if ($get_group['flooring'] == 1) {
                PropertyGroupMeasure::insert(
                    [
                        'property_id'   => $request->property_id,
                        'group_walk_id' => $group_walk_id,
                        'measurement_w' => 0,
                        'measurement_l' => 0,
                    ]
                );
            }
            $items = PropertyGroupWalk::where('id', $group_walk_id)->first();

            return response()->json(
                ['data' => 'success', 'group_walk_id' => $group_walk_id, 'comment' => $items['comment']]
            );
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function loadItem(Request $request)
    {
        $items = PropertyGroupItemWalk::leftJoin('items', 'property_group_items_walk.item_id', '=', 'items.id')
            ->where('group_walk_id', $request->group_id)->where('property_id', $request->property_id)->where(
                'item_id', '<>', null
            )->get(['items.*', 'property_group_items_walk.*', 'property_group_items_walk.id as group_items_walk_id'])
            ->toArray();
        $items_custom = PropertyGroupItemWalk::leftJoin('items', 'property_group_items_walk.item_id', '=', 'items.id')
            ->where('group_walk_id', $request->group_id)->where('property_id', $request->property_id)->where(
                'item_id', '=', null
            )->get(['items.*', 'property_group_items_walk.*', 'property_group_items_walk.id as group_items_walk_id'])
            ->toArray();
        $resultItems = array_merge($items, $items_custom);
        $groupWalkInfo = PropertyGroupWalk::find($request->group_id);
        /*get measurement*/
        $measurement = PropertyGroupMeasure::where('group_walk_id', $request->group_id)->where(
            'property_id', $request->property_id
        )->get()->toArray();

        return response()->json(
            ['data'        => 'success', 'items' => $resultItems, 'groupWalkInfo' => $groupWalkInfo,
             'measurement' => $measurement]
        );
    }

    public function loadProduct(Request $request)
    {
        $items = PropertyProduct::leftJoin('products', 'property_product.product_id', '=', 'products.id')
            ->where('property_product.property_id', $request->property_id)
            ->where('property_product.item_id', $request->item_id)
            ->where('property_product.group_walk_id', $request->group_walk_id)
            ->get(['products.*', 'property_product.*', 'property_product.id as property_product_id'])
            ->toArray();
//        dd($items);
//            $html = '';
//            foreach ($items as $item) {
//                $price_ch = $item['price'] + $item['labor'];
//                $image = $item['image']
//                    ? url('storage/'.$item['image'])
//                    : url(
//                        'storage/'.setting('site.product_def_image')
//                    );
//                $html .= '
//					<div class="col-md-4 col-sm-6 col-xs-12 col-lg-3  p-2" style=" min-height: 215px">
//					<div class="p-3" style="border: 1px solid gray;min-height: 215px;">
//					<div class="row">
//                        <div class="col-xs-6" align="center">
//                         <img class="img-fluid" style="max-width:75px" src="'.$image.'">
//                         </div>
//                         <div class="col-xs-6">
//                             <p id="price-'.$item["property_product_id"].'">Price: $<span>'.$price_ch.'</span></p>
//                                    Qty: <input id="qty-'.$item["property_product_id"].'" onchange="qtyChange('
//                            .$item["property_product_id"].')" type="number" class="form-control" name="qty" value="'
//                            .$item['qty'].'" >
//                                    <input id="firstprice-'.$item["property_product_id"].'" type="hidden" value="'
//                            .$item['price'].'" >
//                        </div>
//                    </div>
//
//					  <div style="width:100%; text-align:center">
//						 <p class="ng-binding">'.Str::limit($item['name'],55).'</p>
//					  </div>
//						 <label style="width:30%; padding: 10px; float: left">Labor</label> <input id="labor-'
//                    .$item["property_product_id"].'" onblur="laborChange('.$item["property_product_id"]
//                    .')" type="number" min="0" max="999999.99" step="0.01" name="labor" value="'.$item['labor'].'" class="form-control" style="width:70%; padding: 10px; float: left" >
//						</div>
//                    </div>
//				';
//            }

        return response()->json(['data' => 'success', 'items' => $items]);
    }

    public function updateQty(Request $request)
    {
        $query = PropertyProduct::where("id", $request->id)->first();
        $query->update(["qty" => $request->qty]);
        $new_price = $this->updateItemPrice($request->id);
        if ($query) {
            return response()->json(['data' => 'success', 'new_price' => $new_price,
                                     'updatedPrice' => ($query->product->price + $query->labor)*$query->qty]);
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function updateLabor(Request $request)
    {
        $query = PropertyProduct::where("id", $request->id)->first();
        $query->update(["labor" => $request->labor]);
        $new_price = $this->updateItemPrice($request->id);
        if ($query) {
            return response()->json(
                ['data' => 'success', 'new_price' => $new_price, 'updatedPrice' => ($query->product->price + $query->labor)*$query->qty]
            );
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function updateItemPrice($id)
    {
        $get_item_id = PropertyProduct::where('id', $id)->first();
        $items = PropertyProduct::leftJoin('products', 'property_product.product_id', '=', 'products.id')
            ->where('property_product.property_id', $get_item_id['property_id'])->where(
                'property_product.item_id', $get_item_id['item_id']
            )->where('property_product.group_walk_id', $get_item_id['group_walk_id'])->get()->toArray();
        $new_price = 0;
        foreach ($items as $item) {
            $new_price += ($item['labor'] + $item['price']) * $item['qty'];
        }
        PropertyGroupItemWalk::where('property_id', $get_item_id['property_id'])->where(
            'group_walk_id', $get_item_id['group_walk_id']
        )->where('item_id', $get_item_id['item_id'])->update(["price" => $new_price]);

        return $new_price;
    }

    public function approvedItem(Request $request)
    {
        $query = PropertyGroupItemWalk::where("id", $request->id)->update(["approved" => 1]);
        if ($query) {
            return response()->json(['data' => 'success']);
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function unApprovedItem(Request $request)
    {
        $query = PropertyGroupItemWalk::where("id", $request->id)->update(["approved" => 0]);
        if ($query) {
            return response()->json(['data' => 'success']);
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function deleteItem(Request $request)
    {
        $query = PropertyGroupItemWalk::find($request->id);
        $product = PropertyProduct::where('item_id', $query->item_id)
            ->where('group_walk_id', $query->group_walk_id)
            ->where('property_id', $query->property_id)
            ->delete();
        $query->delete();
        if ($query) {
            return response()->json(['data' => 'success']);
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function addItem(Request $request)
    {
        // $request->property_id = 10;
        // $request->group_walk_id = 29;
        // $request->item_name = 'acc';
        // $request->GL_code = '';
        // $request->description = '';
        // $request->price = 0;
        $des = "'description'";
        $name = "'item_name'";
        $GL_code = "'GL_code'";
        $price = "'price'";
        if ($request->property_id == '' || $request->group_walk_id == '') {
            return response()->json(['data' => 'error']);
        } else {
            $group_items_walk = PropertyGroupItemWalk::create(
                [
                    'property_id'   => $request->property_id,
                    'group_walk_id' => $request->group_walk_id,
                    'item_name'     => $request->item_name,
                    'GL_code'       => $request->GL_code,
                    'description'   => $request->description,
                    'price'         => $request->price,
                ]
            );
            if ($group_items_walk) {
                return response()->json(['data' => 'success', 'items' => $group_items_walk]);
            } else {
                return response()->json(['data' => 'error']);
            }
        }
    }

    public function updateItem(Request $request)
    {
        $query = PropertyGroupItemWalk::where("id", $request->id)->update([$request->key => $request->value]);
        if ($query) {
            return response()->json(['data' => 'success']);
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function addComment(Request $request)
    {
        $query = PropertyGroupWalk::where("id", $request->group_walk_id)->update(['comment_text' => $request->comment]);
        if ($query) {
            return response()->json(['data' => 'success']);
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function updateMeasurement(Request $request)
    {
        $query = PropertyGroupMeasure::find($request->id);
        $query->update([$request->key => $request->value]);
        if ($query) {
            $sumMeasure = $this->calculateMeasure($query->property_id, $query->group_walk_id);
            $totalMeasured = $this->calculateMeasure($query->property_id);
//            dd($sumMeasure);

            return response()->json(['data' => 'success', 'sumMeasure' => $sumMeasure,'totalMeasured'=>$totalMeasured]);
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function calculateMeasure($propertyId, $group = '')
    {
        $sumMeasure = 0;
        $measures = PropertyGroupMeasure::where("property_id", $propertyId);
        if ($group) {
            $measures = $measures->where('group_walk_id', $group);
        }
        $measures = $measures->get();
        foreach ($measures as $measure) {
            $sumMeasure += $measure->measurement_w * $measure->measurement_l;
        }

        return $sumMeasure;
    }

    public function addMeasure(Request $request)
    {
        $id = PropertyGroupMeasure::insertGetId(
            [
                'property_id'   => $request->property_id,
                'group_walk_id' => $request->group_walk_id,
                'measurement_w' => 0,
                'measurement_l' => 0,
            ]
        );
        if ($id) {
            return response()->json(
                ['data'       => 'success', 'id' => $id,
                 'sumMeasure' => $this->calculateMeasure($request->property_id, $request->group_walk_id),
                ]
            );
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function delMeasurement(Request $request)
    {
        $query = PropertyGroupMeasure::find($request->id);
        $property = $query->property;
        $group = $query->group_walk_id;
        $query->delete();
        $totalMeasured = $this->calculateMeasure($property->id);
        $sumMeasure = $this->calculateMeasure($property->id,$group);
        if ($query) {
            return response()->json(['data' => 'success','totalMeasured'=>$totalMeasured,'sumMeasure'=>$sumMeasure]);
        } else {
            return response()->json(['data' => 'error']);
        }
    }

    public function quickReview(Request $request)
    {
        $propertyId = $request->propertyId;
        $groups = PropertyGroupItemWalk::where('property_id', $propertyId)
            ->where(
                function ($q) {
                    return $q->where('price', '>', 0)->orWhereNotNull('description');
                }
            )
            ->groupBy('group_walk_id')
            ->pluck('group_walk_id');

        $result = [];
        $summary = [
            'countApproved'  => 0,
            'countDenied'    => 0,
            'totalBudget'    => 0,
            'approvedBudget' => 0,
        ];
        foreach ($groups as $group) {
            $groupWalk = Group::find(PropertyGroupWalk::findOrFail($group)->group_id);
            $items = PropertyGroupItemWalk::where('group_walk_id', $group)
                ->where('property_id', $propertyId)
                ->leftjoin('items', 'items.id', '=', 'item_id')
                ->where(
                    function ($q) {
                        return $q->where('price', '>', 0)->orWhereNotNull('description');
                    }
                )
                ->get(
                    ['property_group_items_walk.id', 'name', 'price', 'property_group_items_walk.gl_code', 'item_name',
                     'description', 'approved', 'item_id']
                )->toArray();
            foreach ($items as $key => $item) {
                if ($item['approved']) {
                    $summary['countApproved']++;
                    $summary['approvedBudget'] += $item['price'];
                } else {
                    $summary['countDenied']++;
                }
                $summary['totalBudget'] += $item['price'];

                $items[$key] += ['product_image' => PropertyProduct::where('property_id', $propertyId)
                    ->where('qty', '>', 0)
                    ->where('group_walk_id', $group)
                    ->where('item_id', $item['item_id'])
                    ->join('products', 'products.id', '=', 'product_id')
                    ->pluck('image')->toArray()];
            }
            $result[] = [
                'group_walk' => $group,
                'group'      => $groupWalk,
                'items'      => $items,
            ];
        }

        return response()
            ->json(['data' => 'success', 'items' => $result,'summary'=>$summary]);
    }

    public function budget(Request $request)
    {
        $propertyId = $request->propertyId;

        $groups = PropertyGroupItemWalk::where('property_id', $propertyId)
            ->where(
                function ($q) {
                    return $q->where('price', '>', 0)->orWhereNotNull('description');
                }
            )
            ->groupBy('group_walk_id')
            ->pluck('group_walk_id');
        $summary = [
            'measuredSQFT' => $this->calculateMeasure($propertyId),
            'totalPrice'   => PropertyGroupItemWalk::where('property_id', $propertyId)
                ->sum('price'),
        ];
        $result = [];
        foreach ($groups as $group) {
            $groupWalk = Group::find(PropertyGroupWalk::findOrFail($group)->group_id)->toArray();
            $groupWalk['measure'] = $this->calculateMeasure($propertyId, $group);
            $products = PropertyProduct::where('group_walk_id', $group)
                ->where('property_id', $propertyId)
                ->leftjoin('products', 'products.id', '=', 'product_id')
                ->where('qty', '>', 0)
                ->get()->toArray();
//            dd($products);
            $customItems = PropertyGroupItemWalk::where('group_walk_id', $group)
                ->where('property_id', $propertyId)
                ->where(function($q){
                    return $q->whereNull('item_id')
                        ->orWhere(function($q2){
                            return $q2->where('price',0)->whereNotNull('description');
                        });
                })
                ->where(function($q){
                    return $q->where('price', '>', 0)
                        ->orWhereNotNull('description');
                })
                ->get();
            $customItems->map(function($item){
               if($item->item_id)
                   return $item->item_name = ItemsModel::find($item->item_id)->name;
            });
//            dd($customItems);
            $result[] = [
                'group'       => $groupWalk,
                'products'    => $products,
                'customItems' => $customItems,
            ];
        }

        return response()
            ->json(['data' => 'success', 'items' => $result, 'summary' => $summary]);
    }


}
