<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ItemForm;
use App\Models\ItemsModel;
use App\Models\ItemTypesModel;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    public function getLists(Request $request)
    {
        $item = ItemsModel::showed()->orderBy('name','asc');
        if($request->type)
            $item = $item->where('item_type_id',$request->type);

        if($request->search)
            $item = $item->where('name','like',"%$request->search%");

        if($request->get('except') && $except=explode(',',$request->get('except')))
            $item = $item->whereNotIn('id',$except);

        if($request->result=='collection')
            $item = $item->get();
        else
            $item = $item->pluck('name','id');
        return response()->json(['data'=>$item]);
    }

    public function detailItemType(Request $request)
    {
        $itemType = ItemTypesModel::find($request->itemType);

        if($itemType)
        {
            $itemType->form;
            return response()->json(['data'=>$itemType,]);
        }
        return response()->json(['messages'=>'Not found!'])->setStatusCode(400);
    }
}
