<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\API\PropertyGroupController;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\Users\EstimateRequest;
use App\Mail\RegisteredEmail;
use App\Models\Faq;
use App\Models\PropertyImages;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Contact;
use App\Models\Team;
use App\Models\Project;
use App\Models\Property;
use App\Models\SiteContent;
use App\Models\Testimonial;
use Illuminate\Support\Facades\Mail;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Page;

class HomeController extends Controller
{

    private $email = 'lebaotrang1810@gmail.com';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $projects = Project::orderBy('created_at', 'desc')->take(4)->get();
        $faqs = Faq::all();
        $pages = Page::all();
        return view('landingpage.home.index', compact('projects','faqs','pages'));
    }

    public function blogs($title, $id)
    {
        $page = Page::findOrFail($id);
        return view('landingpage.blogs.index',compact('page'));
    }

    public function contact(Request $request)
    {
        $rules = [
            'name'    => ['alpha_dash', 'max:250'],
            'email'   => ['email', 'max:250', 'required'],
            'message' => ['required', 'max:1000'],
        ];
        $validated = Validator::make($request->all(), $rules);

        if ($validated->fails()) {
            return response()->json(['status' => 400, 'message' => $validated->errors()]);
        }

        $order = array("\r\n", "\n", "\r");
        $replace = '<br />';
        $newstr = str_replace($order, $replace, $request->get('message'));
        $arr = [
            'name'    => $request->get('name'),
            'email'   => $request->get('email'),
            'message' => $newstr,
        ];
        $contact = Contact::create($arr);
        $email = $this->email;
        Mail::send(
            'landingpage.home.mail-template',
            array(
                'name'        => $request->get('name'),
                'email'       => $request->get('email'),
                'bodyMessage' => $newstr,
            ), function ($message) use ($request, $email) {
            $message->from('davidanthony1402@gmail.com');
            $message->to($email, $request->get('name'))->subject('A new contact messages from customers');
        }
        );
        // $this->sendMail('', $arr, 'A new contact messages from customers');
        if (Mail::failures()) {
            return response()->json(['status' => 400, 'message' => 'Sorry! Please try again later']);
        }

        return response()->json(['status' => 200, 'message' => 'Successfully contact']);
    }

    public function getListTeams(Request $request)
    {
        $teams = Team::orderBy('name', 'asc')->get();
        $content = $this->getContentBySlug('about_us_content');

        return view('landingpage.about.index', compact('teams', 'content'));
    }

    public function getListProjects(Request $request)
    {
        $projects = Project::orderBy('created_at', 'desc')->get();

        return view('landingpage.ourwork.index', compact('projects'));
    }

//    public function estimate(Request $request)
//    {
//         dd($request->all());
//        Session::put('timezone', $request->timezone);
//        $rules = [
//            'comments' => ['max:250'],
//        ];
//        $validated = Validator::make($request->all(), $rules);
//
//        if ($validated->fails()) {
//            return response()->json(['status' => 400, 'message' => $validated->errors()]);
//        }
//        $arr = [
//            "project_name"     => $request->project_name,
//            "address"          => $request->address,
//            "city"             => $request->city,
//            "state"            => $request->state,
//            "zip_code"         => $request->zip_code,
//            "county"           => $request->country, // county != country
//            "main_square_ft"   => $request->main_square_ft,
//            "year_build"       => $request->year_build,
//            "number_bathrooms" => $request->number_bathrooms,
//            "number_bedrooms"  => $request->number_bedrooms,
//            "comments"         => $request->comments,
//            "created_at"       => date('Y-m-d H:i:s')
//        ];
//        $property_id = Property::insertGetId($arr);
//        $email = $this->email;
//        Mail::send(
//            'landingpage.estimate.mail-template',
//            $arr, function ($message) use ($request, $email) {
//            $message->from('davidanthony1402@gmail.com');
//            $message->to($email, $request->get('project_name'))->subject('New estimate request from the customer.');
//        }
//        );
//        if (Mail::failures()) {
//            return response()->json(['status' => 400, 'message' => 'Sorry! Please try again later']);
//        }
//        if ($property_id) {
//            return response()->json(['status' => 200, 'message' => 'Estimate worked', 'id' => $property_id]);
//        } else {
//            return response()->json(['status' => 400, 'message' => 'Error']);
//        }
//    }

    public function estimate(EstimateRequest $request)
    {
        $property = Property::create($request->except(['_token','name','email','images']));
        if($request->upload_image && $request->images)
        {
            foreach ($request->images as  $image) {
                $filename=(rand(1,100)+time()).'.'.$image->getClientOriginalExtension();

                $image->storeAs(
                    'property/'.$property->id.'/',
                    $filename, ['disk' => 'public']
                );
                $filename = 'property/'.$property->id.'/'.$filename;
                $property->images()->save(new PropertyImages([
                    'image'=>$filename
                ]));
            }
        }
        $walk = (new PropertyGroupController())->creatNewWalk(new Request(['id'=>$property->id]));

        if(Auth::guest() || (Auth::check() && Auth::user()->hasRole(['Walker','Admin'])))
        {
            $user = User::where('email', $request->email)->first();
            if (!$user) {
                $password = time();
                $user = User::create(
                    [
                        'email'    => $request->email,
                        'name'     => $request->name,
                        'phone'     => $request->phone,
                        'password' => bcrypt($password),
                    ]
                );
                $user->setRole('customer');
                Mail::to($request->email)->send(new RegisteredEmail(['email'=>$request->email,'password'=>$password]));
            } else {
                $property->update(['owner_id' => $user->id]);
            }

            if( Auth::check()  && Auth::user()->hasRole('Walker'))
                $property->update(['walker_id'=>Auth::id()]);
        }

        if(Auth::guest())
        {
//            Auth::loginUsingId($user->id);
            $property->update(['owner_id' => $user->id]);
            return redirect(route('user.login'))
                ->with('alert-success','Please login to continue. If you do not have an account, we have created for you an account and sent password to your email, use it to login.')
                ->withInput(['email'=>$user->email,'redirect'=>route('user.walkProperty',$property->id)]);

        }
        elseif(Auth::check() && Auth::user()->hasRole(['Walker','Admin']))
        {
            return redirect(route('voyager.property.index'))
                ->with(
                    [
                        'message'    => "Created Property information",
                        'alert-type' => 'success',
                    ]
                );
        }
        elseif(Auth::check() && Auth::user()->hasRole('Customer'))
        {
            $property->update(['owner_id' => Auth::id()]);
        }
        return redirect(route('user.walkProperty',$property->id))
            ->with('success','Created property information.');
    }



    public function getProperty(Request $request, $id)
    {
        $property_info = Property::findOrFail($id);

        return view('landingpage.estimate.result', compact('property_info'));
    }

    public function getContentBySlug($slug)
    {
        $content = SiteContent::where('slug', $slug)->first();

        return $content;
    }

    public function direct() {
        $testimonials = Testimonial::all();
        $ipreforesure = $this->getContentBySlug('direct_ipreforesure_content');
        $traditional = $this->getContentBySlug('direct_traditional_content');
        return view('landingpage.direct.index', compact('ipreforesure', 'traditional', 'testimonials'));
    }

    public function updateProperty($id, EstimateRequest $request)
    {
        $property = Property::findOrFail($id);
        if($property->owner_id==Auth::id())
        {
            $property->update($request->except('_method','_token'));
            return back()->with([
               'message'=>'Updated property information.',
                'alert-type'=>'success',
            ]);
        }
        return back()->with([
            'message'=>'Unauthorized.',
            'alert-type'=>'error',
        ]);
    }
    public function updatePropertyImage($id, Request $request)
    {
        $property = Property::findOrFail($id);
        if($property->owner_id==Auth::id())
        {
            foreach ($property->images as $image) {
                $image->update([
                    'comment'=>$request->get('comment_'.$image->id)
                ]);
            }
            return back()->with([
                'message'=>'Updated.',
                'alert-type'=>'success',
            ]);
        }
        return back()->with([
            'message'=>'Unauthorized.',
            'alert-type'=>'error',
        ]);
    }
    public function addPropertyImage($id, ImageRequest $request)
    {
        $property = Property::findOrFail($id);
        if($property->owner_id==Auth::id() && $request->hasFile('image'))
        {
            $filename=(rand(1,100)+time()).'.'.$request->image->getClientOriginalExtension();

            $request->image->storeAs(
                'property/'.$property->id.'/',
                $filename, ['disk' => 'public']
            );
            $filename = 'property/'.$property->id.'/'.$filename;
            $property->images()->save(new PropertyImages([
                'image'=>$filename,
                'comment'=>$request->comment
            ]));

            return back()->with([
                'message'=>'Added an image.',
                'alert-type'=>'success',
            ]);
        }
        return back()->with([
            'message'=>'Unauthorized.',
            'alert-type'=>'error',
        ]);
    }

    public function deletePropertyImage($id)
    {
        $image = PropertyImages::findOrFail($id);
        if($image->property->owner_id==Auth::id())
        {
            $image->delete();
            return back()->with([
                'message'=>'Deleted an image.',
                'alert-type'=>'success',
            ]);
        }
        return back()->with([
            'message'=>'Unauthorized.',
            'alert-type'=>'error',
        ]);

    }
}
