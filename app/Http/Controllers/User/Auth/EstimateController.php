<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;
use App\Http\Controllers\API\PropertyGroupController;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Property;
use App\Models\PropertyGroupWalk;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;

class EstimateController extends Controller
{
    //
    public function index(Request $request)
    {
        $properties = Property::where('owner_id',Auth::id());

        if($request->search)
            $properties = $properties->where(function($q) use($request){
               return $q->where('project_name','like',"%$request->search%")
                   ->orWhere('property_code','like',"%$request->search%")
                   ->orWhere('address','like',"%$request->search%");
            });
        $properties = $properties->paginate(15);
        return view('landingpage.user.estimate-list',compact('properties'));
    }
    use BreadRelationshipParser;
    public function walkProperty($id)
    {
        $slug ='property';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != ''
                && method_exists(
                    $model, 'scope'.ucfirst($dataType->scope)
                )
            ) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }
        if($dataTypeContent->owner_id!=Auth::id())
            return back()->with(
                [
                    'message'    => 'Unauthorized!',
                    'alert-type' => 'error',
                ]
            );
        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field

        $this->removeRelationshipField($dataType, 'edit');
        $property_groups = PropertyGroupWalk::where('property_id', $id)->get();
        $groups = Group::all();

        //$active_groups = PropertyGroupWalk::leftJoin('group', 'property_group_walk.group_id', '=', 'group.id')->where('property_id',$id)->get();
        $active_groups = PropertyGroupWalk::join('group', 'property_group_walk.group_id', '=', 'group.id')
            ->where('property_id', $id)->get(
                ['group.*', 'property_group_walk.id as group_walk_id', 'property_group_walk.comment as setcomment']
            );
        $totalMeasured = (new PropertyGroupController())->calculateMeasure($dataTypeContent->id);
        return view('landingpage.estimate.walk',compact('dataTypeContent','id','dataType',
        'property_groups','groups','active_groups','totalMeasured'));
    }

    public function walkedProperty($id)
    {
        $property = Property::findOrFail($id);
        if($property->walked==0 && $property->owner_id==Auth::id()) {
            $property->update(['walked' => 1]);

            return back()->with(
                [
                    'message'    => 'Walked!',
                    'alert-type' => 'success',
                ]
            );
        }
        else
            return back()->with(
                [
                    'message'    => 'Unauthorized!',
                    'alert-type' => 'error',
                ]
            );
    }

}
