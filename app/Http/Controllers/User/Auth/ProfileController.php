<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    //
    public function index(Request $request)
    {
        return view('landingpage.user.profile');
    }

    public function update(Request $request)
    {
        $update['name']=$request->name;
        if($request->password)
            $update['password']=bcrypt($request->password);

        Auth::user()->update($update);
        return back()->with([
            'message'    => "Updated user information!",
            'alert-type' => 'success',
        ]);
    }

}
