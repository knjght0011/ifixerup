<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use Goutte\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CrawController extends Controller
{
    public function category()
    {
        ProductCategory::truncate();
        $site = "https://www.homedepot.com/c/site_map";
        $client = new Client();
        $crawler = $client->request('GET', $site);
        $crawler->filter('.etchRichText>p>a')->each(function ($parentCategory) {
            if ($parentCategory->children('strong')->getNode(0)) {
                $href = array_filter(explode('/', $parentCategory->extract(['href'])[0]));

                if ($href[3] == 'b') {
                    $code = $this->getCodeFromArray($href);
                    if ($code) {
                        $category = ProductCategory::where('code', $code)->firstOrCreate([], [
                            'code' => $code,
                            'name' => $parentCategory->children('strong')->text(),
                            'slug' => Str::slug($parentCategory->children('strong')->text())
                        ]);
                    }
                }
            }
        });
    }

    public function getCodeFromArray(array $array)
    {
        $code = last($array);
        if (strpos($code, '=ShowProducts'))
            $code = "";
        return $code;

    }

    public function subCategory($category)
    {

        $category = ProductCategory::where('code', $category)->first();
//        dd(1);
        $category->subCategory()->delete();
        $site = "https://www.homedepot.com/b/{$category->slug}/{$category->code}";
        $client = new Client();
        $crawler = $client->request('GET', $site);
//        DB::beginTransaction();
        $crawler->filter('.customNav__container')->each(function ($subCategory) use ($category) {
//            dd()
//                ,$subCategory->filter('.customNav__heading')->text());
//            save to product category
            $href = explode('/', $subCategory->filter('.customNav__heading')->extract(['href'])[0]);

            $code = $this->getCodeFromArray($href);
            $title = $subCategory->filter('.customNav__heading')->text();
            try {
                if ($code) {

                    $newCategory = $category->subCategory()->save(new ProductCategory([
                        'code' => $code,
                        'name' => $subCategory->filter('.customNav__heading')->text(),
                        'slug' => Str::slug($title),
                        'depth' => 2
                    ]));
                    $subCategory->filter('.list--type-plain>.list__item--padding-none>a')->each(function ($childCategory) use ($newCategory) {
                        $href = explode('/', $childCategory->extract(['href'])[0]);
                        $code = $this->getCodeFromArray($href);
                        $childTitle = $childCategory->text();
                        if ($code) {
                            ProductCategory::where('code', $code)->delete();
                            $newCategory->subCategory()->save(new ProductCategory([
                                'code' => $code,
                                'name' => $childCategory->text(),
                                'slug' => Str::slug($childTitle),
                                'depth' => 3
                            ]));
                        }
                    });
                }
            } catch (\Exception $exception) {
                Log::debug($exception->getMessage());
            }

        });
//        DB::commit();
    }

    public function getProductByCategory($category)
    {
        $category = ProductCategory::where('code', $category)->first();
        $site = "https://www.homedepot.com/b/{$category->slug}/{$category->code}";
//        if ($request->catStyle)
//            $site = $site . '?catStyle=' . $request->catStyle;
        return $this->getProduct($site, $category);
//        DB::commit();///;f;;
//        $crawler->filter('.pod-inner')->each(function($section){
//            $product = $section->filter('.pod-plp__description>a');
//            dd($product);
//            $image = $section->filter('.plp-pod__image>a>img')->extract(['src'])[0];

//        });
    }

    public function getProduct($site, $category)
    {
//        newloop:
        $client = new Client();
        $crawler = $client->request('GET', $site.'?catStyle=ShowProducts');
//        try {
        $nextSite = $crawler
            ->filterXpath('//*[@id="plp_core"]/div[4]/div/nav/ul/li/a[@title="Next"]')
            ->extract(['href', 'data-pagenumber']);
        do {
            $data = json_decode($crawler->filterXpath('//script[@type="application/ld+json"]')->text());
            if ($data) {
                foreach (($data->mainEntity->offers->itemOffered) as $product) {

                    try {
                        $filename = 'products/craw/' . time() . '.' . last(explode('.', $product->image));
                        $newCrawler = new \GuzzleHttp\Client();
                        $newCrawler->request('GET', $product->image,
                            ['sink' => storage_path('app/public/' . $filename)]);
                    } catch (\Exception $exception) {
                        Log::debug('Image Product');
                        $filename = null;
                    }


                    if (!Product::where('SKU', $product->sku)->first() && isset($product->offers->price)) {
                        Product::create([
                            'name' => $product->name,
                            'SKU' => $product->sku,
                            'description' => $product->description,
                            'price' => $product->offers->price,
                            'default_labor' => 0,
                            'default_hours' => 0,
                            'gl_code' => '',
                            'category_id' => $category->id,
                            'image' => $filename,
                            'brand' => is_string($product->brand) ? $product->brand : '',
                        ]);
                    }
                }
            }
            if (isset($nextSite[0]) && isset($nextSite[0][0])) {
                $site = "https://www.homedepot.com" . $nextSite[0][0];
                return $this->getProduct($site, $category);
            }
        } while (isset($nextSite[0]) && isset($nextSite[0][0]) && isset($nextSite[0][1]) && $nextSite[0][1] != 1);
//        }
//        catch (\Exception $exception)
//        {
//            Log::debug($exception->getMessage());
//        }
    }
}
