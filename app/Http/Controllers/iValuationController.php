<?php

namespace App\Http\Controllers;

use App\Mail\RegisteredEmail;
use App\Models\HVAllHomeZipcode;
use App\Models\iValuation;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use phpDocumentor\Reflection\Types\Object_;

class iValuationController extends Controller
{
    public function calculate(Request $request)
    {
        $address = $request->address;
        $endpoint = "https://maps.googleapis.com/maps/api/geocode/json?key="
                .setting('site.map_api_key')."&address=".$request->address;
        $client = Http::get($endpoint);
        try {
        $formatedAddress = $client->collect()->get('results')[0]['formatted_address'];
        $formatedAddress = str_replace(', USA', '', $formatedAddress);
        $formatedAddress = str_replace(',', '', $formatedAddress);
        $formatedAddress = str_replace(' ', '-', $formatedAddress);
        $property = iValuation::where('zaddress', $formatedAddress)
                ->where('recorded_at',Carbon::now()->toDateString())
                ->first();
        if (!$property) {

                $bounds = $client->collect()->get('results')[0]['geometry']['viewport'];
                $mapBound = '{"west":'.$bounds['southwest']['lng'].',"east":'.$bounds['northeast']['lng'].',"south":'.$bounds['southwest']['lat'].',"north":'.$bounds['northeast']['lat'].'}';
                $site = "https://www.zillow.com/search/GetSearchPageState.htm";
                $param = [
                        "searchQueryState" => '{"pagination":{},"usersSearchTerm":"'.$address.'","mapBounds":'.$mapBound.',"isMapVisible":true,"filterState":{"sortSelection":{"value":"globalrelevanceex"},"isAllHomes":{"value":true}},"isListVisible":true,"mapZoom":18}',
                        "wants"            => '{"cat1":["listResults","mapResults"],"cat2":["total"]}',
                        "requestId"        => 3
                ];
                $crawler = Http::get($site, $param);
                $property = null;
                DB::beginTransaction();
                foreach ($crawler->collect()->get('cat1')['searchResults']['mapResults'] as $item) {

                    $zAddress = explode('/', $item['detailUrl'])[2];
                    if (!iValuation::where('zaddress', $zAddress)
                            ->where('recorded_at',Carbon::now()->toDateString())->first()) {
                        $homeInfo = $item['hdpData']['homeInfo'];
                        $item['latLong'] = json_encode($item['latLong']);
                        unset($item['hdpData']);
                        $item = (array_merge($item, $homeInfo));
                        if (isset($item['listing_sub_type'])) {
                            $item['listing_sub_type'] = json_encode($item['listing_sub_type']);
                        }
                        $item['address'] = $zAddress;
                        $item['zaddress'] = $zAddress;
                        $item['price'] = $item['price']+($item['price']*(rand(8, 15) / 10)/100);
                        $item['price_from'] = $item['price'] - 0.08 * $item['price'];
                        $item['price_to'] = $item['price'] + 0.08 * $item['price'];
                        $item['recorded_at'] = Carbon::now()->toDateString();
                        $valuation = iValuation::create($item);
                        if ($zAddress == $formatedAddress) {
                            $property = $valuation;
                        }
                    }
                }
                DB::commit();
            }
        }
        catch (\Exception $exception) {
            return redirect(route('ivaluation.ivaluation'))
                    ->with([
                            'alert-type' => 'error',
                            'message'    => 'Could not connect to google service.'
                    ]);
        }
        return view('landingpage.valuation.calculate', compact('property'));
    }

    public function step2()
    {
        return view('landingpage.valuation.step2');
    }

//    public function calculate(Request $request)
//    {
//        $endpoint = "https://maps.googleapis.com/maps/api/geocode/json?key="
//                .setting('site.map_api_key')."&address=".$request->address;
//        $client = Http::get($endpoint);
//        $result = [];
//        if ($client->status() == 200) {
//
//            if ($client->collect()->get('status') == 'OK') {
//                foreach ($client->collect()->get('results')[0]['address_components'] as $item) {
//                    if ($item['types'][0] == 'postal_code') {
//                        $zipcode = $item['long_name'];
//                    }
//                }
//            }
//            if (isset($zipcode)) {
//                $priceET = HVAllHomeZipcode::where('regionname',$zipcode)->first();
//                $priceET = last(json_decode($priceET->value));
//                $priceET = $priceET*$request->main_square_ft;
//                $location = $request->lat.','.$request->lng;
//                $endpoint = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key="
//                        .setting('site.map_api_key')."&location=".$location."&radius=1000&
//                        type=school|park|parking|department_store|doctor|hospital|school|shopping_mall|university";
//                $client = Http::get($endpoint);
//                $count = count($client->collect()->get('results'));
//                $priceET = $priceET-($priceET*0.2);
//                if($count<=5)
//                    $priceET = $priceET-($priceET*0.1);
//                elseif($count<=10)
//                    $priceET = $priceET+($priceET*0.05);
//                else
//                    $priceET = $priceET+($priceET*0.1);
//
//                $priceET = $priceET/1000;
//
//                if (Auth::guest() || (Auth::check() && Auth::user()->hasRole(['Walker', 'Admin']))) {
//                    $user = User::where('email', $request->email)->first();
//                    if (!$user) {
//                        $password = time();
//                        $user = User::create(
//                                [
//                                        'email'    => $request->email,
//                                        'name'     => $request->name,
//                                        'phone'    => $request->phone,
//                                        'password' => bcrypt($password),
//                                ]
//                        );
//                        $user->setRole('customer');
//                        Mail::to($request->email)->send(new RegisteredEmail([
//                                'email'    => $request->email,
//                                'password' => $password
//                        ]));
//                    }
//
//                }
//                else
//                {
//                    $user = Auth::user();
//                }
//                $valuation = iValuation::create([
//                        'address'        => $request->address,
//                        'zipcode'        => $zipcode,
//                        'main_square_ft' => $request->main_square_ft,
//                        'bedroom'        => $request->number_bedrooms,
//                        'home_type'      => $request->home_type,
//                        'user_id'        => $user->id,
//                        'lat'        => $request->lat,
//                        'lng'        => $request->lng,
//                        'valuation'      => $priceET
//                ]);
//                return redirect(route('ivaluation.result', $valuation));
//            }
//            return redirect(route('ivaluation.ivaluation'))
//                    ->with([
//                            'alert-type'=>'error',
//                            'message'=>'Something went wrong.'
//                    ]);
//        }
//        return redirect(route('ivaluation.ivaluation'))
//                ->with([
//                    'alert-type'=>'error',
//                    'message'=>'Something went wrong.'
//                ]);
//    }

    public function result($id)
    {
        $valuation = iValuation::findOrFail($id);
        return view('landingpage.valuation.valuation', compact('valuation'));
    }
}
