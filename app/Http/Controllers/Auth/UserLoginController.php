<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserLoginController extends Controller
{
    //
    use AuthenticatesUsers;
    protected $redirectTo = '/';
    public function __construct()
    {
    }

    public function showLoginForm()
    {
        return view('landingpage.auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|required',
            'password' => 'required|min:4'
        ]);

        if(Auth::guard()->attempt(['email' => $request->email, 'password' => $request->password, 'active' => 1, 'role_id' => 2], $request->remember)) {
            if(Auth::user()->active==User::DEACTIVATED) {
                // dd('Account does not exist. Please, try again!');
                $this->guard()->logout();
                return redirect()->back()->withInput($request->only('email','remember'))
                    ->withErrors(['errors' => 'The account is deactivated, please connect your admin!']);
            }
            else {
                return $request->redirect ? redirect($request->redirect) : redirect()->route('user.profile');
            }
        }
        // if unsuccessful
        // dd('Account does not exist. Please, try again!');
        return redirect()->back()->withInput($request->only('email','remember'))->withErrors(['errors' => 'Account does not exist. Please, try again!']);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        return redirect()->to('/');
    }
}
