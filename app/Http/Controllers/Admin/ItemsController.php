<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\ItemDescription;
use App\Models\ItemProduct;
use App\Models\ItemTypeFormValue;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use TCG\Voyager\Alert;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;

use function Symfony\Component\String\s;

class ItemsController extends VoyagerBaseController
{
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        DB::beginTransaction();

        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
        if ($request->addedProduct && $addedProduct = explode(',', $request->addedProduct)) {
            foreach ($addedProduct as $item) {
                $data->products()->save(
                    new ItemProduct(
                        [
                            'product_id' => $item,
                            'labor'      => $request->get('labor_product_'.$item),
                            'hours'      => $request->get('hours_product_'.$item),
                            'gl_code'    => $request->get('glcode_product_'.$item),
                            'note'       => $request->get('note_product_'.$item),
                            'sort'       => $request->get('sort_product_'.$item, 0),
                        ]
                    )
                );
            }
        }

        foreach ($data->type->form as $field) {
            $value = $field->field_type=='checkbox'? (int) ($request->get($field->slug) == 'on'):$request->get($field->slug);

            ItemTypeFormValue::create([
                'item_id'=>$data->id,
                'item_type_form_id'=>$field->id,
                'value'=>$value
            ]);
        }
        if ($request->description) {
            foreach ($request->description as $item) {
                $data->description()->save(
                    new ItemDescription(
                        [
                            'description' => $item,
                        ]
                    )
                );
            }
        }
        DB::commit();
        event(new BreadDataAdded($dataType, $data));


        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with(
                [
                    'message'    => __('voyager::generic.successfully_added_new')
                        ." {$dataType->getTranslatedAttribute('display_name_singular')}",
                    'alert-type' => 'success',
                ]
            );
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = $model->findOrFail($id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        DB::beginTransaction();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);



        if ($request->addedProduct) {
            $newItems = explode(',', $request->addedProduct);
            $oldItems = $data->products()->pluck('product_id', 'sort')->toArray();

            $removedItems = array_diff($oldItems, $newItems);
            $addedItems = array_diff($newItems, $oldItems);

            $data->products()->whereIn('product_id', $removedItems)
                ->delete();
            foreach ($newItems as $sort => $newItem) {
                $groupItem = $data->products()->where('product_id', $newItem)
                    ->first();
                if ($groupItem) {
                    $groupItem->update(
                        [
                            'labor'   => $request->get('labor_product_'.$newItem),
                            'hours'   => $request->get('hours_product_'.$newItem),
                            'gl_code' => $request->get('glcode_product_'.$newItem),
                            'note'    => $request->get('note_product_'.$newItem),
                            'sort'    => $sort + 1,
                        ]
                    );
                } else {
                    $data->products()->save(
                        new ItemProduct(
                            [
                                'product_id' => $newItem,
                                'labor'      => $request->get('labor_product_'.$newItem),
                                'hours'      => $request->get('hours_product_'.$newItem),
                                'gl_code'    => $request->get('glcode_product_'.$newItem),
                                'note'       => $request->get('note_product_'.$newItem),
                                'sort'       => $sort + 1,
                            ]
                        )
                    );
                }
            }
        }
        elseif ($data->products)
        {
            $data->products()->delete();
        }

        foreach ($data->type->form as $field) {
            $value = $field->field_type=='checkbox'? (int) ($request->get($field->slug) == 'on'):$request->get($field->slug);
            $itemFormValue = ItemTypeFormValue::where([
                'item_id'=>$data->id,
                'item_type_form_id'=>$field->id,
            ])->first();
            if($itemFormValue)
                $itemFormValue->update([
                    'value'=>$value
                ]);
            else
                ItemTypeFormValue::create([
                    'item_id'=>$data->id,
                    'item_type_form_id'=>$field->id,
                    'value'=>$value
                ]);
        }

        if ($request->description) {
            $data->description()->delete();
            foreach ($request->description as $item) {
                $data->description()->save(
                    new ItemDescription(
                        [
                            'description' => $item,
                        ]
                    )
                );
            }
        }
        elseif ($data->description)
        {
            $data->description()->delete();
        }
        DB::commit();

        event(new BreadDataUpdated($dataType, $data));

        if (auth()->user()->can('browse', app($dataType->model_name))) {
            $redirect = redirect()->route("voyager.{$dataType->slug}.index");
        } else {
            $redirect = redirect()->back();
        }

        return $redirect->with(
            [
                'message'    => __('voyager::generic.successfully_updated')
                    ." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]
        );
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();


        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        $errors = new MessageBag();

        // add your error messages:
        foreach ($ids as $key => $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            // Check permission
            $this->authorize('delete', $data);

            if ($data->group&&$data->group->isNotEmpty()) {
                $group = Group::whereIn('id', $data->group()->pluck('group_id'))->pluck('name')->toArray();

                $errors->add('error', 'Can not delete '.$data->name.'. Please remove it from Group: '.implode(', ',
                        $group));

                unset($ids[$key]);
            }


            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->getTranslatedAttribute('display_name_plural')
            : $dataType->getTranslatedAttribute('display_name_singular');

//        dd($errors);
        $res = $data->destroy($ids);
        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
            if (count($errors) > 0) {
                return redirect()->route("voyager.{$dataType->slug}.index")->withErrors($errors);
            } else {
                $data = [
                    'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                    'alert-type' => 'success',
                ];

                return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
            }
        } else {
            if (count($errors) > 0) {
                return redirect()->route("voyager.{$dataType->slug}.index")->withErrors($errors);
            } else {
                $data = [
                    'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                    'alert-type' => 'error',
                ];

                return redirect()->route("voyager.{$dataType->slug}.index")->withErrors($data);
            }
        }
    }
}
