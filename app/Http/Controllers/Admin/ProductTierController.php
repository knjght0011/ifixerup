<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ItemsModel;
use App\Models\Product;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;
use Illuminate\Support\MessageBag;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Facades\Voyager;

class ProductTierController extends VoyagerBaseController
{
    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();


        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        $errors = new MessageBag();

        // add your error messages:
        foreach ($ids as $key => $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            // Check permission
            $this->authorize('delete', $data);

            if ($data->product&& $data->product->isNotEmpty()) {
                $product = Product::whereIn('id', $data->product()->pluck('product_id'))->pluck('name')->toArray();

                $errors->add('error', 'Can not delete '.$data->name.'. Please remove it from Product: '
                    .implode(', ',$product));

                unset($ids[$key]);
            }


            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->getTranslatedAttribute('display_name_plural')
            : $dataType->getTranslatedAttribute('display_name_singular');

//        dd($errors);
        $res = $data->destroy($ids);
        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
            if (count($errors) > 0) {
                return redirect()->route("voyager.{$dataType->slug}.index")->withErrors($errors);
            } else {
                $data = [
                    'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                    'alert-type' => 'success',
                ];

                return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
            }
        } else {
            if (count($errors) > 0) {
                return redirect()->route("voyager.{$dataType->slug}.index")->withErrors($errors);
            } else {
                $data = [
                    'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                    'alert-type' => 'error',
                ];

                return redirect()->route("voyager.{$dataType->slug}.index")->withErrors($data);
            }
        }
    }
}
