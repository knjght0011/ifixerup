<?php

namespace App\Http\Controllers\Admin\Voyager;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use TCG\Voyager\Http\Controllers\VoyagerAuthController as BaseVoyagerAuthController;

class VoyagerAuthController extends BaseVoyagerAuthController
{
    public function postLogin(Request $request)
    {
        Session::put('timezone', $request->timezone);
        $this->validateLogin($request);


        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function authenticated(Request $request, $user)
    {
//        dd($this->redirectPath());
        if ($user->active == User::DEACTIVATED) {
            $this->guard()->logout();

            $request->session()->invalidate();

            $request->session()->regenerateToken();

            return redirect(route('voyager.login'))->withInput()->withErrors('Your account has been deactivated.');
        } else {
            redirect()->intended($this->redirectPath());
        }
    }

    public function redirectTo()
    {
//        dd(route('voyager.property.index'));
        if (Auth::user()->role_id == Role::getRoleIdByName(Role::WALKER))
            return route('voyager.property.index');
        elseif (Auth::user()->role_id == Role::getRoleIdByName(Role::USER))
            return config('voyager.user.redirect', route('voyager.dashboard'));
        elseif (Auth::user()->role_id == Role::getRoleIdByName(Role::ADMIN))
            return config('voyager.user.redirect', route('voyager.dashboard'));
        else {
            return config('voyager.user.redirect', route('voyager.dashboard'));
        }
    }
}
