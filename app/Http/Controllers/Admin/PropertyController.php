<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Property\ExportAllProduct;
use App\Exports\Property\ExportApprovedItems;
use App\Exports\Property\ExportPropertyWalk;
use App\Exports\Property\ExportPropertyWalkAutoSize;
use App\Http\Controllers\API\PropertyGroupController;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Models\Property;
use App\Models\PropertyImages;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;
use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;

use App\Models\Flow;
use App\Models\Group;
use App\Models\GroupItem;
use App\Models\FlowGroupItem;
use App\Models\ItemsModel;
use App\Models\PropertyGroupWalk;
use App\Models\PropertyGroupItemWalk;

use App\Models\ItemProduct;
use App\Models\PropertyGroup;
use App\Models\PropertyGroupItem;
use App\Models\PropertyGroupMeasure;
use App\Models\PropertyProduct;
use TCG\Voyager\Events\BreadDataUpdated;

class PropertyController extends VoyagerBaseController
{
    public function createWalk($id)
    {
        $newWalk = (new PropertyGroupController())->creatNewWalk(new Request(['id'=>$id]));
        if(json_decode($newWalk->getContent())->data=='success')
            return back()->with([
               'message'=>'Created new walk!',
               'alert-type'=>'success',
               'walk'=>true
            ]);
        return back()->with([
            'message'=>'Something went wrong!',
            'alert-type'=>'error'
        ]);
    }
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

//        if ($request->addedProduct && $addedProduct = explode(',', $request->addedProduct)) {
//            foreach ($addedProduct as $item) {
//                $data->products()->save(
//                    new ItemProduct(
//                        [
//                            'product_id' => $item,
//                            'labor'      => $request->get('labor_product_'.$item),
//                            'hours'      => $request->get('hours_product_'.$item),
//                            'gl_code'    => $request->get('glcode_product_'.$item),
//                            'note'       => $request->get('note_product_'.$item),
//                            'sort'       => $request->get('sort_product_'.$item, 0),
//                        ]
//                    )
//                );
//            }
//        }

        event(new BreadDataAdded($dataType, $data));


        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with(
                [
                    'message'    => __('voyager::generic.successfully_added_new')
                        ." {$dataType->getTranslatedAttribute('display_name_singular')}",
                    'alert-type' => 'success',
                ]
            );
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = $model->findOrFail($id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

//        if ($request->addedProduct) {
//            $newItems = explode(',', $request->addedProduct);
//            $oldItems = $data->products()->pluck('product_id', 'sort')->toArray();
//
//            $removedItems = array_diff($oldItems, $newItems);
//            $addedItems = array_diff($newItems, $oldItems);
//
//            $data->products()->whereIn('product_id', $removedItems)
//                ->delete();
//            foreach ($newItems as $sort => $newItem) {
//                $groupItem = $data->products()->where('product_id', $newItem)
//                    ->first();
//                if ($groupItem) {
//                    $groupItem->update(
//                        [
//                            'labor'   => $request->get('labor_product_'.$newItem),
//                            'hours'   => $request->get('hours_product_'.$newItem),
//                            'gl_code' => $request->get('glcode_product_'.$newItem),
//                            'note'    => $request->get('note_product_'.$newItem),
//                            'sort'    => $sort + 1,
//                        ]
//                    );
//                } else {
//                    $data->products()->save(
//                        new ItemProduct(
//                            [
//                                'product_id' => $newItem,
//                                'labor'      => $request->get('labor_product_'.$newItem),
//                                'hours'      => $request->get('hours_product_'.$newItem),
//                                'gl_code'    => $request->get('glcode_product_'.$newItem),
//                                'note'       => $request->get('note_product_'.$newItem),
//                                'sort'       => $sort + 1,
//                            ]
//                        )
//                    );
//                }
//            }
//        }
        event(new BreadDataUpdated($dataType, $data));

        if (auth()->user()->can('browse', app($dataType->model_name))) {
            $redirect = redirect()->route("voyager.{$dataType->slug}.index");
        } else {
            $redirect = redirect()->back();
        }

        return $redirect->with(
            [
                'message'    => __('voyager::generic.successfully_updated')
                    ." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]
        );
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != ''
                && method_exists(
                    $model, 'scope'.ucfirst($dataType->scope)
                )
            ) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'edit', $isModelTranslatable);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        $property_groups = PropertyGroupWalk::where('property_id', $id)->get();
        $groups = Group::all();
        $totalMeasured = (new PropertyGroupController())->calculateMeasure($dataTypeContent->id);

        //$active_groups = PropertyGroupWalk::leftJoin('group', 'property_group_walk.group_id', '=', 'group.id')->where('property_id',$id)->get();
        $active_groups = PropertyGroupWalk::join('group', 'property_group_walk.group_id', '=', 'group.id')
            ->where('property_id', $id)->get(
                ['group.*', 'property_group_walk.id as group_walk_id', 'property_group_walk.comment as setcomment']
            );
        return Voyager::view(
            $view, compact(
            'dataType', 'dataTypeContent',
            'isModelTranslatable', 'property_groups', 'id', 'active_groups', 'groups',
                'totalMeasured'
        )
        );
    }

    public function walked($id)
    {
        $property = Property::findOrFail($id);
        $property->update(['walked' => 1]);

        return back()->with(
            [
                'message'    => 'Walked!',
                'alert-type' => 'success',
            ]
        );
    }

    public function export($id, Request $request)
    {
        $property = Property::findOrFail($id);
        if(!$property->walked)
            return back()->withErrors('Something went wrong!');
        switch ($request->export)
        {
            case 'property_excel':
            {
                return Excel::download(new ExportPropertyWalk($property),Str::slug(trim($property->project_name),'_').'.xlsx');
            }
            case 'pdf':
            {
                return Excel::download(new ExportPropertyWalk($property),Str::slug(trim($property->project_name),'_').'.pdf', \Maatwebsite\Excel\Excel::DOMPDF)
                    ;
            }
            case 'approved_items':
            {
                return Excel::download(new ExportPropertyWalk($property,'approved_items'),Str::slug(trim($property->project_name),
                        '_').'.xlsx');
            }
            case 'approved_items_pdf':
            {
                return Excel::download(new ExportPropertyWalk($property,'approved_items_pdf'),
                    Str::slug(trim($property->project_name),'_').'.pdf',
                    \Maatwebsite\Excel\Excel::DOMPDF);
            }
            default:{

            }
        }
    }
    public function updatePropertyImage($id, Request $request)
    {
        $property = Property::findOrFail($id);
            foreach ($property->images as $image) {
                $image->update([
                    'comment'=>$request->get('comment_'.$image->id)
                ]);
            }
            return back()->with([
                'message'=>'Updated.',
                'alert-type'=>'success',
            ]);

    }
    public function addPropertyImage($id, ImageRequest $request)
    {
        $property = Property::findOrFail($id);
        $property->upload_image?:$property->update(['upload_image'=>1]);
        if($request->hasFile('image'))
        {
            $filename=(rand(1,100)+time()).'.'.$request->image->getClientOriginalExtension();

            $request->image->storeAs(
                'property/'.$property->id.'/',
                $filename, ['disk' => 'public']
            );
            $filename = 'property/'.$property->id.'/'.$filename;
            $property->images()->save(new PropertyImages([
                'image'=>$filename,
                'comment'=>$request->comment
            ]));

            return back()->with([
                'message'=>'Added an image.',
                'alert-type'=>'success',
            ]);
        }
        return back()->with([
            'message'=>'Something went wrong.',
            'alert-type'=>'error',
        ]);
    }

    public function deletePropertyImage($id)
    {
        $image = PropertyImages::findOrFail($id);
            $image->delete();
            return back()->with([
                'message'=>'Deleted an image.',
                'alert-type'=>'success',
            ]);
           }
}
