<?php

namespace App\Http\Controllers\Admin\HVI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeValueIndexController extends Controller
{
    public function index()
    {
        return view('vendor.voyager.homevalue.index');
    }
}
