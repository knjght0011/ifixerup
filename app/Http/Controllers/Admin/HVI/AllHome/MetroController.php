<?php

namespace App\Http\Controllers\Admin\HVI\AllHome;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\HVI\AllHomeMetroRequest;
use App\Imports\HVAllHomeMetro;
use App\Models\HVAllHomeMetro as HVAllHomeMetroModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class MetroController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('browse_home-value-index-all-home-zipcode');
        $data = HVAllHomeMetroModel::orderBy('id', 'asc');
        $searchSelect = [
            'regionid'=>'RegionID',
            'sizerank'=>'SizeRank',
            'regionname'=>'RegionName',
            'regiontype'=>'RegionType',
            'statename'=>'StateName'
        ];
        if($request->s && $request->filter && $request->key)
        {
            $data = $data->where($request->key,$request->filter=='equals'?"=":"like",
                $request->filter=='equals'?$request->s:"%$request->s%");
        }

        $data = $data->paginate(15);

        return view('vendor.voyager.homevalue.allhome.metro.browse',
            compact('data','searchSelect'));
    }


    public function import(AllHomeMetroRequest $request)
    {
        HVAllHomeMetroModel::truncate();
        Session::forget('headding');
        Excel::import(new HVAllHomeMetro(), $request->file('import'));

        return redirect(route('admin.hvi.allhome.metro.index'))
            ->with('success', 'Imported Home Value Index - All Home - Metro!');
    }

    public function view($id)
    {
        $datum = HVAllHomeMetroModel::findOrFail($id);
        return view('vendor.voyager.homevalue.allhome.metro.read',compact('datum'));
    }
}
