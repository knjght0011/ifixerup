<?php

namespace App\Http\Controllers\Admin\HVI\AllHome;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\HVI\AllHomeZipcodeRequest;
use App\Imports\HVAllHomeZipcode as HVAllHomeZipcodeImport;
use App\Models\HVAllHomeMetro as HVAllHomeMetroModel;
use App\Models\HVAllHomeZipcode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class ZipCodeController extends Controller
{
    public function __construct()
    {
    }
    public function index(Request $request)
    {
        $this->authorize('browse_home-value-index-all-home-zipcode');
        $data = HVAllHomeZipcode::orderBy('id', 'asc');
        $searchSelect = [
            'regionid'=>'RegionID',
            'sizerank'=>'SizeRank',
            'regionname'=>'RegionName',
            'regiontype'=>'RegionType',
            'statename'=>'StateName',
            'state'=>'State',
            'city'=>'City',
            'Metro'=>'Metro',
            'countyname'=>'CountyName',
        ];
        if($request->s && $request->filter && $request->key)
        {
            $data = $data->where($request->key,$request->filter=='equals'?"=":"like",
                $request->filter=='equals'?$request->s:"%$request->s%");
        }

        $data = $data->paginate(15);

        return view('vendor.voyager.homevalue.allhome.zipcode.browse',
            compact('data','searchSelect'));
    }


    public function import(AllHomeZipcodeRequest $request)
    {
        HVAllHomeZipcode::truncate();
        Session::forget('headding');
        Excel::import(new HVAllHomeZipcodeImport(), $request->file('import'));

        return redirect(route('admin.hvi.allhome.zipcode.index'))
            ->with('success', 'Imported Home Value Index - All Home - Metro!');
    }

    public function view($id)
    {
        $datum = HVAllHomeZipcode::findOrFail($id);
        return view('vendor.voyager.homevalue.allhome.zipcode.read',compact('datum'));
    }
}
