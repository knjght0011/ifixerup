<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\iValuation;
use Illuminate\Http\Request;

class iValuationController extends Controller
{
    public function index(Request $request)
    {
        $data = iValuation::orderBy('id','desc');
        $data = $data->paginate(20);
        return view('vendor.voyager.ivaluation.index', compact('data'));
    }
}
