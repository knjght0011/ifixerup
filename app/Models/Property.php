<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Property extends Model
{
    protected $table = 'property';
    protected $appends
        = [
            'full_address',
        ];
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at',
    ];
    protected $fillable
        = [
            'project_name',
            'property_code',
            'address',
            'city',
            'state',
            'zip_code',
            'county',
            'main_square_ft',
            'number_bedrooms',
            'number_bathrooms',
            'year_build',
            'comments',
            'walk_assigned',
            'gate_code',
            'lock_box',
            'due_date',
            'deleted_at',
            'owner_id',
            'walked',
            'upload_image',
        ];

    public function walker(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'walk_assigned', 'id');
    }

    public function owner(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }

    public function items(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PropertyGroup::class, 'property_id', 'id');
    }

    public function getFullAddressAttribute()
    {
        return $this->address.', '.$this->city.', '.$this->state.', '.$this->zip_code.($this->zip_code);
    }
    public function scopeManageProperty($query)
    {
        if(Auth::user()->role_id== Role::getRoleIdByName(Role::WALKER))
            return $query->where('walk_assigned', Auth::user()->id);
    }

    public function images()
    {
        return $this->hasMany(PropertyImages::class)->orderBy('id','desc');
    }
}
