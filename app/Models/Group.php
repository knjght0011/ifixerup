<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Group extends Model
{
    protected $table='group';

    public function items(): HasMany
    {
        return $this->hasMany(GroupItem::class,'group_id','id')
            ->orderBy('sort');
    }
}
