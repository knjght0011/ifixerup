<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class PropertyImages extends Model
{
    use HasFactory;

    protected $table = 'property_image';
    protected $fillable = [
        'property_id',
        'image',
        'comment',

    ];

    public function getImageUrlAttribute()
    {
        if($this->image)
            if(Storage::disk('public')->exists($this->image))
                return url('storage/'.$this->image);

        return url('storage/'.setting('site.product_def_image'));
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}
