<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class FlowGroupItem extends Model
{
    protected $table = 'flow_group_items';
    protected $fillable
        = [
            "id",
            "flow_id",
            "group_id",
            "sort",
        ];
	public $timestamps = false;
	public function flow()
    {
        return $this->belongsTo(Flow::class,'flow_id','id');
    }
    public function group()
    {
        return $this->belongsTo(Group::class,'group_id','id');
    }
}
