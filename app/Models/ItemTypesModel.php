<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemTypesModel extends Model
{
    use HasFactory;

    const ALWAYS_ADDED=25;
    const BASIC_COMMENT = 26;
    const DEFAULT_PACKAGE_KIT = 27;
    const INCREMENT_ROW_TYPE = 28;
    const MULTIPLE_SKU_DISPLAY = 29;
    const OPTIONAL_INCREMENT_ROW_TYPE = 30;
    const PACKAGE_ROW_TYPE = 31;
    const WHOLE_BUDGET_PERCENT = 32;
    const WHOLE_HOUSE_SQFT = 33;


    protected $table = 'item_types';
    protected $fillable
        = [
            'id', 'name',
        ];

    public function form()
    {
        return $this->hasMany(ItemForm::class,'item_type_id','id')->orderBy('sort');
    }

}
