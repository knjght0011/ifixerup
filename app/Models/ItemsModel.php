<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemsModel extends Model
{
    use HasFactory;

    protected $table = 'items';
    protected $fillable
        = [
            "id",
            "name",
            "item_type_id",
            "required",
            "picture_required",
            "read_only",
            "non_budget",
            "gl_code",
            "category",
            "sub_category",
            "hidden",
            "created_at",
            "updated_at",
        ];

    public static function showed()
    {
        return self::where('hidden','=',0);
    }
    public static function notShowed()
    {
        return self::where('hidden','=',1);
    }
    public function type()
    {
        return $this->belongsTo(ItemTypesModel::class,'item_type_id','id');
    }

    public function group()
    {
        return $this->hasMany(GroupItem::class,'item_id','id');
    }
    public function products()
    {
        return $this->hasMany(ItemProduct::class,'item_id','id')->orderBy('sort');
    }

    public function description()
    {
        return $this->hasMany(ItemDescription::class,'item_id','id');
    }

    public function fieldValue($formId)
    {
        return ItemTypeFormValue::where('item_id',$this->id)->where('item_type_form_id',$formId)->first();
    }

}
