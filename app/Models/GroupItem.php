<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class GroupItem extends Model
{
    protected $table = 'group_items';
    protected $fillable
        = [
            "id",
            "group_id",
            "item_id",
            "sort",
        ];
public $timestamps = false;
    public function group()
    {
        return $this->belongsTo(Group::class,'group_id','id');
    }
    public function item()
    {
        return $this->belongsTo(ItemsModel::class,'item_id','id');
    }
}
