<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasFactory;

    protected $table = 'product_categories';
    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'code',
        'depth',
        'passed',
    ];
    public function parentId()
    {
        return $this->belongsTo(self::class);
    }

    public function subCategory()
    {
        return $this->hasMany(self::class,'parent_id','id');
    }

    public function products()
    {
        return $this->hasMany(Product::class,'category_id','id');
    }
}
