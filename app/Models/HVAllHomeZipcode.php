<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HVAllHomeZipcode extends Model
{
    use HasFactory;

    protected $table = 'home_value_all_home_zipcode';
    protected $fillable
        = [
            'regionid',
            'sizerank',
            'regionname',
            'regiontype',
            'statename',
            'state',
            'city',
            'metro',
            'countyname',
            'value',
            'created_at',
            'updated_at',
        ];
}
