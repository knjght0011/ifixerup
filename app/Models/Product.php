<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class Product extends Model
{
    protected $fillable = [
        'name',
        'SKU',
        'description',
        'price',
        'default_labor',
        'default_hours',
        'gl_code',
        'category_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'image',
        'brand',

    ];
    protected $appends = [
        'image_url'
    ];

    public function getImageUrlAttribute()
    {
        if ($this->image)
            if (Storage::disk('public')->exists($this->image))
                return url('storage/' . $this->image);

        return url('storage/' . setting('site.product_def_image'));
    }

    public function items()
    {
        return $this->hasMany(ItemProduct::class, 'product_id', 'id');
    }
}
