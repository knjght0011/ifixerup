<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PropertyGroup extends Model
{
    protected $table='property_group';
	public $timestamps = false;
	public function property()
    {
        return $this->belongsTo(Property::class,'property_id','id');
    }
    public function group()
    {
        return $this->belongsTo(Group::class,'group_id','id');
    }
}
