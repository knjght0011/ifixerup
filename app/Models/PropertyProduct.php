<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PropertyProduct extends Model
{
    protected $table = 'property_product';
    protected $fillable
        = [
            'property_id',
            'group_walk_id',
            'item_id',
            'product_id',
            'qty',
            'labor',
        ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
