<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ItemTypeFormValue extends Model
{
    protected $fillable = [
        'item_id',
        'item_type_form_id',
        'value'
    ];
    protected $table = 'item_type_form_value';

}
