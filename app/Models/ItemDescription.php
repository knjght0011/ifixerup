<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ItemDescription extends Model
{
    protected $table = 'item_description';
    protected $fillable
        = [
            'item_id',
            'description',
            'created_at',
            'deleted_at',
        ];
}
