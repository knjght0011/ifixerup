<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class iValuation extends Model
{
    use HasFactory;

    protected $table = 'ivaluation';
    protected $fillable = [
            "zpid","priceLabel","beds","baths","area","latLong","statusType","statusText","isFavorite",
            "isUserClaimingOwner","isUserConfirmedClaim","streetViewMetadataURL","streetViewURL","imgSrc",
            "visited","listingType","variableData","badgeInfo","detailUrl","pgapt","sgapt","has3DModel",
            "hasVideo","isHomeRec","address","hasAdditionalAttributions","isFeaturedListing","created_at",
            "updated_at","recorded_at","zipcode","city","state","latitude","longitude","price","dateSold",
            "bathrooms","bedrooms","livingArea","homeType","homeStatus","daysOnZillow","isFeatured",
            "shouldHighlight","zestimate","rentZestimate","listing_sub_type","isUnmappable",
            "isPreforeclosureAuction","homeStatusForHDP","priceForHDP","isNonOwnerOccupied","isPremierBuilder",
            "isZillowOwned","currency","country","taxAssessedValue","lotAreaValue",
            "lotAreaUnit","price_from","price_to","zaddress"
    ];

}
