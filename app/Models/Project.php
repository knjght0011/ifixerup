<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Project extends Model
{
    protected $fillable
        = [
            "id",
            "after",
            "before",
            "name",
            "description",
            "created_at",
            "updated_at",
        ];
}
