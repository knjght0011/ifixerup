<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTier extends Model
{


    protected $table='product_tiers';
	protected $fillable
        = [
            "id",
            "name",
            "created_at",
            "updated_at",
        ];

    public function product()
    {
        return $this->hasMany(ProductTierDetail::class,'product_tier_id','id');
    }
}
