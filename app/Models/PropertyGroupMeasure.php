<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PropertyGroupMeasure extends Model
{
    protected $table = 'property_group_measure';
    protected $fillable
        = [

            'property_id',
            'group_walk_id',
            'measurement_w',
            'measurement_l',
            'measurement_select',
            'keepReplace',
            'flooring_select',
        ];
    public static function measureType(): array
    {
        return [
        'Carpet'   => 'Carpet',
        'Tile'     => 'Tile',
        'Vinyl'    => 'Vinyl',
        'Laminate' => 'Laminate',
        'Concrete' => 'Concrete',
        'Hardwood' => 'Hardwood',
        ];
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

//    public function group()
//    {
//        return $this->belongsTo(PropertyGroup::class,)
//    }
}
