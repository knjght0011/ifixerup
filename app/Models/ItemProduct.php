<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ItemProduct extends Model
{
    protected $table ='item_products';
    protected $fillable
        = [
            "item_id",
            "product_id",
            "created_at",
            "updated_at",
            "labor",
            "hours",
            "gl_code",
            "note",
            "sort"
    ];
    public function item()
    {
        return $this->belongsTo(ItemsModel::class,'item_id','id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
