<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PropertyGroupItemWalk extends Model
{
    protected $table = 'property_group_items_walk';
    protected $appends = ['has_product'];
    protected $fillable
        =['property_id','item_id','group_walk_id','item_name','GL_code','description','price','approved'];

    public function getHasProductAttribute()
    {
        return !$this->item_id ? false : (bool)ItemProduct::where('item_id', $this->item_id)->count();
    }
}
