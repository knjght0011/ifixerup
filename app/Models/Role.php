<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends \TCG\Voyager\Models\Role
{
    const WALKER = 'walker';
    const ADMIN = 'admin';
    const USER = 'user';

    public static function getRoleIdByName($name)
    {
        $id = self::where('name', $name)->first();

        return $id ? $id->id : 0;
    }
}
