<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HVAllHomeMetro extends Model
{
    use HasFactory;

    protected $table = 'home_value_all_home_metro';
    protected $fillable
        = [
            'regionid',
            'sizerank',
            'regionname',
            'regiontype',
            'statename',
            'date',
            'value',
            'created_at',
            'updated_at',
        ];
}
