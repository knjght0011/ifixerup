<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemForm extends Model
{
    use HasFactory;
    protected $table ='item_type_form';

    public function itemType()
    {
        return $this->belongsTo(ItemTypesModel::class,'item_type_id','id');
    }

}
