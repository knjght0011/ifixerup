<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductTierDetail extends Model
{
    protected $table='product_tier_details';
    protected $fillable = [
        'product_id',
        'product_tier_id',
    ];
}
