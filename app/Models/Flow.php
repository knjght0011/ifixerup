<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Flow extends Model
{
    protected $table='flow';

    public function items(): HasMany
    {
        return $this->hasMany(FlowGroupItem::class,'flow_id','id')
            ->orderBy('sort');
    }
	public function user()
    {
        return $this->belongsTo(User::class,'update_by','id');
    }
}
