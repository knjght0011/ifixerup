<?php

namespace App\Imports;

use App\Models\HVAllHomeZipcode as HVAllHomeZipcodeModel;
use Illuminate\Database\Eloquent\Model as HVAllHomeMetroModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\RemembersChunkOffset;
use Maatwebsite\Excel\Concerns\RemembersRowNumber;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class HVAllHomeZipcode implements ToModel,WithBatchInserts,WithChunkReading
{
    use RemembersChunkOffset, RemembersRowNumber;
    public function model(array $array)
    {
        $chunkOffset = $this->getChunkOffset();
        $currentRows = $this->getRowNumber();
        if ($chunkOffset == 1 && !Session::get('headding') && $currentRows==1) {
            unset($array[0], $array[1], $array[2], $array[3], $array[4],
                $array[5],$array[6],$array[7],$array[8],
            );
            Session::put('headding', $array);
            return null;
        }
        else {
            $header = Session::get('headding');
            $result = [
                'regionid'   => $array[0],
                'sizerank'   => $array[1],
                'regionname' => $array[2],
                'regiontype' => $array[3],
                'statename'  => $array[4],
                'state'  => $array[5],
                'city'  => $array[6],
                'metro'  => $array[7],
                'countyname'  => $array[8],
            ];
            unset($array[0], $array[1], $array[2], $array[3], $array[4],$array[5],$array[6],$array[7],$array[8],);
            $result+=['value'=>json_encode(array_combine($header,$array))];
            return new HVAllHomeZipcodeModel($result);
        }
    }


    public function chunkSize(): int
    {
        return 1000;
    }

    public function batchSize(): int
    {
        return 1000;
    }
}
