<?php

namespace App\Imports;

use App\Models\HVAllHomeMetro as HVAllHomeMetroModel;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\RemembersChunkOffset;
use Maatwebsite\Excel\Concerns\RemembersRowNumber;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Row;

class HVAllHomeMetro implements ToModel,WithBatchInserts,WithChunkReading
{
    /**
     * @param array $array
     */
    use RemembersChunkOffset, RemembersRowNumber;
    public function model(array $array)
    {
        $chunkOffset = $this->getChunkOffset();
        $currentRows = $this->getRowNumber();
        if ($chunkOffset == 1 && !Session::get('headding') && $currentRows==1) {
            unset($array[0], $array[1], $array[2], $array[3], $array[4]);
            Session::put('headding', $array);
            return null;
        }
        else {
            $header = Session::get('headding');
            $result = [
                'regionid'   => $array[0],
                'sizerank'   => $array[1],
                'regionname' => $array[2],
                'regiontype' => $array[3],
                'statename'  => $array[4],
            ];
            unset($array[0], $array[1], $array[2], $array[3], $array[4]);
            $result+=['value'=>json_encode(array_combine($header,$array))];
            return new HVAllHomeMetroModel($result);
        }
    }


    public function chunkSize(): int
    {
        return 1000;
    }

    public function batchSize(): int
    {
        return 1000;
    }

}
