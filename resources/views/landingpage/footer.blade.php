<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 col-sm-12 text-center text-md-left mb-3">
                <img class="logo" src="/assets/img/logo-white.png" style=""/>
            </div>
            <div class="col-12 col-md-3 col-sm-4 text-center text-md-left">
                <h5 class="my-3"><strong>PARTNERS</strong></h5>
                <ul>
                    <li><a href="#" class="text-white">Lenders</a></li>
                    <li><a href="#" class="text-white">Builders</a></li>
                    <li><a href="" class="text-white">Brokerages</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3 col-sm-4 text-center text-md-left">
                <h5 class="my-3"><strong>RESOURCES</strong></h5>
                <ul>
                    <li><a href="#" class="text-white">Pricing</a></li>
                    <li><a href="#" class="text-white">FAQs</a></li>
                    <li><a href="#" class="text-white">How It Works</a></li>
                    <li><a href="#" class="text-white">Blogs</a></li>
                    <li><a href="#" class="text-white">Guides</a></li>
                    <li><a href="#" class="text-white">Webinars</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3 col-sm-4 text-center text-md-left">
                <h5 class="my-3"><strong>ABOUT</strong></h5>
                <ul>
                    <li>Mission</li>
                    <li>Culture</li>
                    <li>Careers</li>
                    <li>Press</li>
                </ul>
            </div>
        </div>
        <div class="row pt-4 p-0">
            <div class="col-12 col-md-3 text-center text-md-left">
                <div class="social-links">
                    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                    <a href="#" class="facebook"><i class="bx bxl-facebook-square"></i></a>
                    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                    <a href="#" class="linkedin"><i class="bx bxl-linkedin-square"></i></a>
                </div>
            </div>
            <div class="col-12 col-md-9">
                <div class="copyright">
                    Copyright &copy;. All Rights Reserved
                </div>
            </div>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
