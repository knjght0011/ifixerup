@extends('landingpage.app')
@section('title', $page->title)
@section('content')
<!-- ======= About Section ======= -->
<section id="about" class="container about bg-white mt-5">

    <div class="container">

        <div class="section-title d-block p-0" data-aos="fade-up" align="center">
            <h1 class="text-blue">{{ $page->title }}</h1>
        </div>
        <div class="row content mt-4" data-aos="fade-up">
            <div class="col-12">
                <div>
                   {!! $page->body !!}
                </div>
            </div>
        </div>
    </div>
</section><!-- End About Section -->
<script>
</script>
@endsection
