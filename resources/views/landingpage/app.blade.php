<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') - {{ env('APP_NAME') }}</title>
        <link rel="shortcut icon" type="image/x-icon" href="images/logo/favicon.ico">
        @include('landingpage.styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        @stack('css')
    </head>

    <body>
        @include('landingpage.navbar')

        @if(Request::is('/'))
            @include('landingpage.home.banner')
        @endif

        <main id="main" style="background-color: #f7f8fa; padding: 60px 0; min-height: 600px">
            <div class="bs-example">
                <div class="toast hide" id="myToast" style="position: fixed; top: 10px; right: 10px;  z-index: 999;">
                    <div class="toast-header">
                        <strong class="mr-auto"><i class="fa fa-grav"></i> Success</strong>
                        <!-- <small>11 mins ago</small> -->
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body">
                        <div>
                            Successfully estimated!
                            <!-- <a href="#">Click here!</a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div align="center" class="mt-1">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="notification">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            @yield('content')
        </main>


        @include('landingpage.footer')

        @include('landingpage.scripts')

    </body>
@stack('script')
    <script>

        @if(Session::has('alerts'))
        let alerts = {!! json_encode(Session::get('alerts')) !!};
        helpers.displayAlerts(alerts, toastr);
        @endif

        @if(Session::has('message'))

        // TODO: change Controllers to use AlertsMessages trait... then remove this
        var alertType = {!! json_encode(Session::get('alert-type', 'info')) !!};
        var alertMessage = {!! json_encode(Session::get('message')) !!};
        var alerter = toastr[alertType];
        if (alerter) {
            alerter(alertMessage);
        } else {
            toastr.error("toastr alert-type " + alertType + " is unknown");
        }
        @endif
    </script>
</html>
