
    {{--                <form action="{{ route('ivaluation.step1') }}" method="get">--}}
    <div class="input-group search mt-lg-3 mt-0 w-100">
        <div class="input-group-prepend">
                            <span class="input-group-text bg-white no-border">
                                <i class="fas fa-map-marker-alt"></i>
                            </span>
        </div>
        <input required autocomplete="off" type="text" class="form-control no-border"
               value="{{ Request::input('address') }}"
               placeholder="Enter your home address"
               id="search" name="search">
        {{--                        <div class="input-group-append">--}}
        {{--                            <button type="submit" class="input-group-text text-blue bg-white no-border">--}}
        {{--                                Get your home value--}}
        {{--                            </button>--}}
        {{--                        </div>--}}
    </div>
    {{--                </form>--}}
    <div class="row">
        <div class="search autocomplete-items mr-2 px-2" style="
    width: 100%;
    position: relative;
    height: auto;
" align="left">

        </div>
    </div>
@push('script')
    <script>
        var timeout = null;
        $('#search').keyup(function () {
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    data: {search:$('#search').val()},
                    url: '{{ route('api.map.search') }}',
                    success: function (result) {
                        var items  ='';
                        if(result.status=='success') {
                            $.each(result.data, function (key,value){
                                items+=' <a href="{{ route('ivaluation.calculate') }}?address='+key+'"><div class="p-3">'+ value +'</div></a>';
                            })
                        }
                        $('.autocomplete-items').html(items)
                    }
                });
            }, 500);
        });

    </script>
@endpush
