
    <section id="about" class="container about bg-white mt-5">
        <!-- <div class="container"> -->

        <div class="section-title text-center" data-aos="zoom-out">
            <h3 class="mb-5">Why sell to {{ env('APP_NAME') }}?</h3>
            <span>The perfect selling option for those who love <strong>convinience and control.</strong><br/>
                and most importantly, after selling your home. You will receive 50% of the amount.<br/>
                <strong>It is win-win cooperation</strong>
            </span>
        </div>

        <div class="row content mt-4 text-center profit" data-aos="fade-up">
            <div class="col-md-3 col-12">
                <img src="{{asset('assets/img/profit.png')}}" alt="" />
                <h5 class="my-3">50% Profit</h5>
            </div>
            <div class="col-md-3 col-12">
                <img src="{{asset('assets/img/repair.png')}}" alt="" />
                <h5 class="my-3">Skip showings and repairs</h5>
            </div>
            <div class="col-md-3 col-12">
                <img src="{{asset('assets/img/truck.png')}}" alt="" />
                <h5 class="my-3">Move on your schedule</h5>
            </div>
            <div class="col-md-3 col-12">
                <img src="{{asset('assets/img/support.png')}}" alt="" />
                <h5 class="my-3">24/7 support</h5>
            </div>
        </div>

        <div class="row p-4">
            <div class="col-lg-6 col-12 p-0">
                <div class="card">
                    <h5 class="card-header bg-blue text-white py-4">{{ env('APP_NAME') }}</h5>
                    <div class="card-body">
                        <p><i class="fa fa-check text-blue mr-2"></i> Receive an instant offer for your house from {{ env('APP_NAME') }}</p>
                        <p><i class="fa fa-check text-blue mr-2"></i> Request {{ env('APP_NAME') }} offer in one click</p>
                        <p><i class="fa fa-check text-blue mr-2"></i> No stress over repair on upgrades</p>
                        <p><i class="fa fa-check text-blue mr-2"></i> No need for any showings or open houses</p>
                        <p><i class="fa fa-check text-blue mr-2"></i> No need to clean or stage your house</p>
                        <p><i class="fa fa-check text-blue mr-2"></i> Get cash for your house as is.No loan issues</p>
                        <p><i class="fa fa-check text-blue mr-2"></i> Move on your terms. Closing dates are fast and flexible</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12 p-0">
                <div class="card">
                    <h5 class="card-header p-4">Traditional House Sale</h5>
                    <div class="card-body">
                        <p><i class="fa fa-times text-danger mr-2"></i> Wait days (somtimes months_ for offers from potential buyers</p>
                        <p><i class="fa fa-times text-danger mr-2"></i> Interview multiple agents until you find an experienced one</p>
                        <p><i class="fa fa-times text-danger mr-2"></i> Handle all necessary repairs and upgrades</p>
                        <p><i class="fa fa-times text-danger mr-2"></i> Keep your house clean and staged for selling</p>
                        <p><i class="fa fa-times text-danger mr-2"></i> Accommodate frequent showings and open houses</p>
                        <p><i class="fa fa-times text-danger mr-2"></i> Hope that loan-based offers don't fall through</p>
                        <p><i class="fa fa-times text-danger mr-2"></i> Wait on paperwork. Closing dates might affect your plans</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End About Section -->

    <section class="container bg-blue p-0">
        <div class="row content bg-primary" data-aos="fade-down">
            <div class="col-12 p-4 text-center">
                <h3 class="my-3 text-blue">Welcome to the <br/> Easiest way to sell your home</h3>
                <p class="text-white">Here's how we cut out the unnecessary steps to make selling your home easy, fast, and strees-free</p>
            </div>
        </div>
    </section>

    <section class="container bg-white provide">
        <div class="row text-center">
            <div class="col-lg-3 col-md-6 col-12">
                <div class="position-relative my-3">
                    <img src="{{asset('assets/img/step1.jpg')}}" alt="" />
                    <span class="bg-primary text-white py-2 px-3 step position-absolute">step 1</span>
                </div>
                <strong>Provide us your address and tell us about your home</strong>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="position-relative my-3">
                    <img src="{{asset('assets/img/step2.jpg')}}" alt="" />
                    <span class="bg-primary text-white py-2 px-3 step position-absolute">step 2</span>
                </div>
                <strong>Provide us your address and tell us about your home</strong>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="position-relative my-3">
                    <img src="{{asset('assets/img/step3.jpg')}}" alt="" />
                    <span class="bg-primary text-white py-2 px-3 step position-absolute">step 3</span>
                </div>
                <strong>Provide us your address and tell us about your home</strong>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="position-relative my-3">
                    <img src="{{asset('assets/img/step4.jpg')}}" alt="" />
                    <span class="bg-primary text-white py-2 px-3 step position-absolute">step 4</span>
                </div>
                <strong>Provide us your address and tell us about your home</strong>
            </div>
            <div class="col-12 my-3">
                <div class="btn btn-blue">
                    Request your offer today
                </div>
            </div>
        </div>
        <hr/>
    </section>
    <!-- ======= Testimonials Section ======= -->
    <div id="testimonials" class="container about bg-white testimonials bg-white mb-5">
        <div class="container pb-5">

            <div class="section-title text-center" data-aos="zoom-out">
                <h2>What sellers are saying</h2>
                <!-- <p>What they are saying about us</p> -->
            </div>

            <div class="owl-carousel testimonials-carousel" data-aos="fade-up">

            <div class="testimonial-item">
                <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    I would recommend it to anyone who doesn’t want any fuss. It’s cash and done. For the convenience of what we needed, it was totally worth it.
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                <!-- <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt=""> -->
                <div class="text-right">
                    <h3>Saul Goodman</h3>
                    <h4>Ceo &amp; Founder</h4>
                </div>
            </div>

            <div class="testimonial-item">
                <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                I would recommend it to anyone who doesn’t want any fuss. It’s cash and done. For the convenience of what we needed, it was totally worth it.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                <!-- <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt=""> -->
                <div class="text-right">
                    <h3>Sara Wilsson</h3>
                    <h4>Designer</h4>
                </div>
            </div>

            <div class="testimonial-item">
                <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                I would recommend it to anyone who doesn’t want any fuss. It’s cash and done. For the convenience of what we needed, it was totally worth it.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                <!-- <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt=""> -->
                <div class="text-right">
                    <h3>Jena Karlis</h3>
                    <h4>Store Owner</h4>
                </div>
            </div>

            <div class="testimonial-item">
                <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                <!-- <img src="assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt=""> -->
                <div class="text-right">
                    <h3>Matt Brandon</h3>
                    <h4>Freelancer</h4>
                </div>
            </div>

            <div class="testimonial-item">
                <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                <!-- <img src="assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt=""> -->
                <div class="text-right">
                    <h3>John Larson</h3>
                    <h4>Entrepreneur</h4>
                </div>
            </div>

            </div>

        </div>
    </div><!-- End Testimonials Section -->
