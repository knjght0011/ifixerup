@extends('landingpage.app')
@section('title', 'iValuation')

@section('content')
    <div class="col-md-6 offset-md-3 col-12 text-center my-3" id="heroCarousel">
        <h1>
            iValuation Result
        </h1>
        @include('landingpage.valuation.search')
    </div>

        <div class="row no-gutters">
            <div class="col-12 col-md-6  " style="text-align: -webkit-center;" data-aos="fade-right">
                <div id="map"></div>
            </div>
            <div class="col-12 col-md-6 bg-white p-4" style=" height:300px;" data-aos="fade-left">
                @if($property)
                    <div class="row no-gutters">
                        <div class="col-5">
                            <div class="d-flex flex-column justify-content-center align-items-center"
                                 style="height: 200px">

                                <h5 class="text-blue"> <i class="fas fa-wallet"></i></h5>
                                <h5 class="text-blue"> iValuation</h5>
                                <h1 style="font-family: Roboto; font-weight: bold">
                                    {{ formatCurrency($property->price,0) }}
                                </h1>
                            </div>
                        </div>
                        <div class="col-7" style="height: 300px">
                            <div class="d-flex flex-column justify-content-center align-items-center"
                                 style="height: 200px">
                                <p>
                                <h5 class="text-blue"> <i class="fas fa-exchange-alt"></i></h5>
                                <h5 class="text-blue"> iValuation range</h5>
                                <h4 style="font-family: Roboto; font-weight: bold">
                                    {{ formatCurrency($property->price_from,0) }} - {{ formatCurrency($property->price_to,0) }}
                                </h4>
                                </p>
                                <p>
                                <h5 class="text-blue"> <i class="fas fa-calculator"></i></h5>
                                <h5 class="text-blue"> iValuation per sqft</h5>
                                <h4 style="font-family: Roboto; font-weight: bold">
                                    {{ formatCurrency($property->price/$property->area,0) }}
                                </h4>
                                </p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>

    <style>
        #map {
            max-width: 600px;
            max-height: 300px;
        }
    </style>
@endsection
@push('script')
    <script>
        $(document).ready(function () {
            initMap();
            // getDataFromZillow();
        });

        function initMap() {
            geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                'address': "{{ Request::input('address') }}"
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    showMap(results[0].geometry.location.lat(), results[0].geometry.location.lng())
                } else
                    showMap()
            });
        }

        function showMap(lat = 40.7858267, lng = -74.0050448) {
            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 18,
                center: {lat: lat, lng: lng},
                mapTypeId: 'satellite'

            });
            // Create an array of alphabetical characters used to label the markers.
            const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            // Add some markers to the map.
            // Note: The code uses the JavaScript Array.prototype.map() method to
            // create an array of markers based on a given "locations" array.
            // The map() method here has nothing to do with the Google Maps API.
            new google.maps.Marker({
                position: {lat: lat, lng: lng},
                map
            });
            // const markers = locations.map((location, i) => {
            //     return new google.maps.Marker({
            //         position: location,
            //         label: labels[i % labels.length],
            //     });
            // });
            // // Add a marker clusterer to manage the markers.
            // new MarkerClusterer(map, markers, {
            //     imagePath:
            //         "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
            // });
        }
    </script>
@endpush
