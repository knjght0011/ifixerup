@extends('landingpage.app')
@section('title', 'iValuation')

@section('content')
    <div class="text-center py-4">
        <h1>
            iValuation Result
        </h1>
        <p>
            {{ Request::input('address') }}
        </p>
    </div>
    <div class="container ">
        <div class="row">
            <div class="card col-12 col-md-6 offset-md-3">
                <div class=" card-body">
                    <div class="row text-center">
                        <div class="col-12 mb-2" >
                            {{ $valuation->address }}
                        </div>
                        <p class="col-6">
                            <b> Main Square FT:</b> {{ number_format($valuation->main_square_ft) }}
                        </p>
                        <p class="col-6">
                            <b> Number Bedrooms:</b> {{ number_format($valuation->bedroom) }}
                        </p>
                        <p class="col-12">
                            <b> Home type:</b> {{ \App\Models\iValuation::home_type()[$valuation->home_type] }}
                        </p>
                        <p class="col-12">
                            <b class="h1 text-blue"> Home Valuation:</b>
                            <br>
                            <b class="h1">
                                {{ formatCurrency($valuation->valuation) }}
                            </b>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
