@extends('landingpage.app')
@section('title', 'iValuation')

@section('content')

    {{--    @include('landingpage.valuation.sale-offer')--}}
    <!-- ======= About Section ======= -->
    <section class=" bg-blue text-white">
        <div class="row content" data-aos="fade-up">
            <div class="col-md-6 offset-md-3 col-12 text-center my-3" id="heroCarousel">
                <h1>
                    iValuation Tools
                </h1>
            @include('landingpage.valuation.search')
            </div>
        </div>

    </section>
    <section id="about" class="container about bg-white mt-5">
        <div class="container">

            <div class="section-title text-center" data-aos="zoom-out">
                <h3 class="text-blue mb-5">Welcome To A New Era of Instant <br/> Home Valuation.</h3>
                <span>Real-time and convenient Home Value Estimates.</span>
            </div>

            <div class="row content mt-4" data-aos="fade-up">
                <div class="col-lg-10 col-12 offset-lg-1">
                    <div class="testimonial-item text-justify text-center">
                        <ul>
                            <li>
                                <span class="d-block"><i
                                        class="bx bxs-quote-left quote-icon-left position-relative"></i> Gone are the days when you have to reply on real estate agents and appraisers to determine the </span>
                                <span class="d-block ml-2"> fair market value of your home. It's 2021 and the real estate industry has advanced to enable you </span>
                                <span class="d-block">to check your home value online in seconds. <i
                                        class="bx bxs-quote-right quote-icon-right position-relative"></i></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-12 col-12 text-center my-4">
                    <a href="{{ route('ivaluation.ivaluation') }}" class="btn-outline-blue">
                        Start NOW <i
                            class='bx bxs-chevron-right ml-3'></i></a>
                </div>
            </div>
        </div>
    </section><!-- End About Section -->

    <!-- ======= Using======= -->
    <section class="container why-us bg-blue mt-5">
        <div class="container pb-2">
            <div class="row content" data-aos="fade-up">

                <div class="col-lg-12 col-12 mb-5">
                    <div class="text-center text-white">
                        <span class="badge badge-pill badge-primary py-2 px-3"><strong class="h5">Using</strong></span>
                    </div>
                </div>


                <div class="col-lg-4 col-12 text-white mb-5">
                    <div class=" text-white">
                        <span class="fa fa-check-circle text-primary fa-lg mr-2"></span>
                        <strong class="h4">Market trends</strong>
                    </div>
                </div>

                <div class="col-lg-4 col-12 text-white mb-4">
                    <div class=" text-white">
                        <span class="fa fa-check-circle text-primary fa-lg mr-2"></span>
                        <strong class="h4">Advanced algorithms</strong>
                    </div>
                </div>

                <div class="col-lg-4 col-12 text-white mb-4">
                    <div class=" text-white">
                        <span class="fa fa-check-circle text-primary fa-lg mr-2"></span>
                        <strong class="h4">Industry insights</strong>
                    </div>
                </div>

                <div class="col-lg-8 col-12 text-white mb-4">
                    <div class=" text-white">
                        <span class="fa fa-check-circle text-primary fa-lg mr-2"></span>
                        <strong class="h4">Estimates from local & national iValuation</strong>
                    </div>
                </div>

            </div>
        </div>
    </section><!-- End Why Us Section -->

    <section class="container bg-blue p-0">
        <div class="row content bg-primary" data-aos="fade-down">
            <div class="col-12 text-white p-4 text-center">
                We're now able to provide you with the current market value of your home using <br/> just your house
                address.
            </div>
        </div>
    </section>

    <section id="about" class="container about bg-white">
        <div class="container">
            <div class="section-title text-center" data-aos="zoom-out">
                <h3 class="text-blue">How our <br/> Home Value Estimator Works.</h3>
                <span>Introducing our iValuation proprietary online Home Value Estimator tool.</span>
            </div>
            <div class="col-lg-12 col-12 text-center my-3">
                <a href="{{ route('ivaluation.ivaluation') }}" class="btn-blue">Launch Tool</a>
            </div>
            <div class="col-lg-10 offset-md-1 col-12 text-center my-5 bg-light p-5">
                <div class="row">
                    <div class="col-lg-2 col-12 text-left">
                        <div class="circle-step mx-auto mb-2">1</div>
                    </div>
                    <div class="col-lg-6 col-12 text-justify">
                        <h3 class="text-primary">You Enter You Home <br/> Address</h3>
                        <p>To provide ypu with a free home value estimate, we <br/> begin by pulling market value data
                            in your area and <br/> accessing neighborhood statistics.</p>
                    </div>
                    <div class="col-lg-4 col-12 img-step d-none d-md-block">
                        <img src="/assets/img/global.png" alt="" class="img-fluid position-absolute"/>
                    </div>
                </div>
            </div>
            <br/>
            <div class="col-lg-10 offset-md-1 col-12 text-center my-5 bg-light p-5">
                <div class="row">
                    <div class="col-lg-4 col-12 img-step d-none d-md-block">
                        <img src="/assets/img/computer.png" alt="" class="img-fluid position-absolute"/>
                    </div>
                    <div class="col-lg-6 col-12 text-right">
                        <h3 class="text-primary">We Pull Real-Time <br/> Industry Data</h3>
                        <p>Proprietary algorithms, real estate insights,
                            and access to big data from local and national iValuation enables us to present you with the
                            most accurate home value estimate in seconds.</p>
                    </div>
                    <div class="col-lg-2 col-12 text-left">
                        <div class="circle-step mx-auto mb-2">2</div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="col-lg-10 offset-md-1 col-12 text-center my-5 bg-light p-5">
                <div class="row">
                    <div class="col-lg-2 col-12 text-left">
                        <div class="circle-step mx-auto mb-2">3</div>
                    </div>
                    <div class="col-lg-6 col-12 text-justify">
                        <h3 class="text-primary">You Get Your Home <br/> Value Estimate</h3>
                        <p>Find out what your home is worth based on fair market value assessments and see what iBuyers
                            would be willing to pay for your hourse.
                            If you choose to request a cash offer.</p>
                    </div>
                    <div class="col-lg-4 col-12 img-step d-none d-md-block">
                        <img src="/assets/img/invoice.png" alt="" class="img-fluid position-absolute"/>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End About Section -->

    <!-- ======= Using======= -->
    <section class="container why-us bg-blue">
        <div class="row content" data-aos="fade-up">
            <div class="col-lg-12 col-12 ">
                <div class="section-title text-center text-white" data-aos="zoom-out">
                    <h3 class="mb-4">Traditional Home Valuation <br/> vs. New Age Home iValuation</h3>
                    <span>Discover the difference between traditional home valuation and iBuyer home valuation.</span>
                </div>
            </div>
        </div>
    </section>

    <section class="container bg-white">
        <div class="row content" data-aos="fade-up">
            <div class="col-lg-12 col-12 text-center mb-4">
                <span>The problem with this old-school model is twofold</span>
            </div>
            <div class="d-lg-flex d-block">
                <div class="col-lg-6 col-12">
                    <div class="p-4 px-5 bg-light h-100 rounded">
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-12 p-0 text-sm-right">
                                <i class="fas fa-times-circle text-danger fa-2x"></i>
                            </div>
                            <div class="col-lg-11 col-md-11 col-12">
                                <h3>It's subjective</h3>
                                <p class="text-justify">Realtors (who aer trying to win your business) come to see your
                                    house
                                    in person and then share with you what they think you could get for it</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="col-lg-6 col-12">
                    <div class="p-4 px-5 bg-light h-100 rounded">
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-12 p-0 text-sm-right">
                                <i class="fas fa-times-circle text-danger fa-2x"></i>
                            </div>
                            <div class="col-lg-11 col-md-11 col-12">
                                <h3>Realtor tend to inflate the amount your home</h3>
                                <p class="text-justify">It's worth to win your business and because they hope to make a
                                    bigger commission on the sale.)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" data-aos="fade-up">
            <div class="container bg-primary m-3 rounded p-4">
                <div class="col-lg-12 col-12">
                    <div class="section-title text-center text-white" data-aos="zoom-out">
                        <h2 class="mb-4">Curious what your home is worth?</h2>
                        <span>Get your free online estimate in seconds using our <br/> Home Value Estimator tool.</span>
                    </div>
                </div>
                <div class="col-lg-12 col-12 text-center my-2">
                    <a href="{{ route('ivaluation.ivaluation') }}" class="btn-blue">Check Home Value</a>
                </div>
            </div>
        </div>
    </section><!-- End Why Us Section -->


    <!-- </main>End #main -->
@endsection
