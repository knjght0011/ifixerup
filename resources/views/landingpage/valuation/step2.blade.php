@extends('landingpage.app')
@section('title', 'iValuation')

@section('content')
    <div class="text-center py-4">
        <h1>
            Property Information
        </h1>
        <p>
            {{ Request::input('address') }}
        </p>
    </div>
    <div class="container ">
        <div class="row">
            <div class="card col-12 col-md-10 offset-md-1">
                <div class=" card-body">
                    {!! Form::open(['url'=>route('ivaluation.calculate')]) !!}
                    <input type="hidden" name="address" value="{{ Request::input('address') }}">
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <div class="form-group">
                                <label class="label-require" >Main Square FT </label>
                                <input type="number" required class="form-control" name="main_square_ft"
                                       value="{{ old('main_square_ft') }}"
                                       id="main_square_ft" placeholder="Main Square FT"
                                       oninput="this.value = this.value.replace(/[^0-9.]/g, '');"
                                       data-rule="regexp:^((\d+\.\d{1,2})|(\d+)|(\.\d{1,2}))$"
                                       data-msg="Please enter a valid Main Square FT" maxlength="10"/>
                                <div class="validate"></div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="form-group">
                                <label class="label-require">Number Bedrooms</label>
                                <input type="number" required class="form-control" name="number_bedrooms" id="number_bedrooms"
                                       placeholder="Number Bedrooms" data-rule="required"
                                       oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="3"
                                       data-msg="Please enter number bedrooms"/>
                                <div class="validate"></div>
                            </div>
                        </div>
                        <div class=" col-12">
                            <div class="form-group">
                                <label class="label-require">Your home type</label>
                                {!! Form::select('home_type',\App\Models\iValuation::home_type(),'',['class'=>'form-control ']) !!}
                            </div>
                        </div>
                        @if(\Auth::guest() || (\Auth::check() && !\Auth::user()->hasRole('Customer')))
                            <div class="col-12 ">
                                <h3>
                                    Your information
                                </h3>
                            </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="label-require">Email</label>
                                        <input type="email" required name="email" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="label-require">Name</label>
                                        <input type="text" maxlength="255" required name="name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="label-require">Phone</label>
                                        <input type="number" required name="phone" class="form-control">
                                    </div>
                                </div>
                        @endif
                        <div class=" col-12 text-center">
                            <button type="submit" class="btn btn-primary">
                                Get iValuation
                            </button>
                        </div>
                    </div>
                    <input type="hidden" name="lat" id="lat">
                    <input type="hidden" name="lng" id="lng">
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'address': "{{ Request::input('address') }}"
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                $('#lat').val(results[0].geometry.location.lat())
                $('#lng').val( results[0].geometry.location.lng())
            }
        })
    </script>
@endpush
