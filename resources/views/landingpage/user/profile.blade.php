@extends('landingpage.user.app')
@section('title', 'Homepage')

@section('authcontent')
    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h4 class="h4">Profile</h4>
    </div>
    <h2>Information</h2>
    <div class="row">
        <div class="col-12 ">
            <div class="card">
                <!-- <div class="card-header">
                  Featured
                </div> -->
                <div class="card-body">
                    <form action="{{ route('user.profile.update') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" aria-describedby="emailHelp"
                                   value="{{ auth()->user()->email }}" disabled>
                            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="{{ auth()->user()->name }}">
                        </div>

                        <div class="form-group">
                            <label>New Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password">
                            <p class="help-block">Leave it blank if you dont want to change it.</p>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-blue">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
