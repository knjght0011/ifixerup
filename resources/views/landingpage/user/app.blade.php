@extends('landingpage.app')
@section('title', 'Homepage')

@section('content')
<section id="project" class="container bg-white mt-5">
    <div class="container">


      <div class="row">

        <nav class="col-md-3 d-none d-md-block bg-white sidebar">
            <div class="flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Settings</h1>
            </div>
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('user.profile') }}">
                    <span data-feather="home"></span>
                    My Profile <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('user.list-estimate') }}">
                    <span data-feather="file"></span>
                    Estmate List
                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" href=/profile/settings">
                    <span data-feather="shopping-cart"></span>
                    Settings
                    </a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('user.logout') }}">
                    <span data-feather="users"></span>
                    Sign out
                    </a>
                </li>
                </ul>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-9 px-4">
            @yield('authcontent')
        </main>
      </div>

</div>
</section>
@endsection
