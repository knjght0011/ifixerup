@extends('landingpage.user.app')
@section('title', 'Homepage')

@section('authcontent')
    <style>
        th,td{
            text-align: center;
            vertical-align: middle;
        }
    </style>
    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h4 class="h4">List Estimate</h4>
    </div>
    {!! Form::open(['method'=>'GET']) !!}
    <div class="form-group ">
        <div class="d-flex">
            <input type="text" class="form-control col-8"
                   name="search" value="{{ Request::input('search') }}" placeholder="Search property code, property name or address.">
            <button type="submit" class="btn btn-success col-2">Search</button>
        </div>


    </div>
    {!! Form::close() !!}
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>No</th>
                <th>Project name</th>
                <th>Address</th>
                <th>Status</th>
                <th>Created At</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($properties as $property)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>
                        {{ $property->property_code?$property->property_code." - ":"" }}
                        {{ $property->project_name }}
                    </td>
                    <td>
                        {{ $property->full_address }}
                    </td>
                    <td>
                        @if($property->walked)
                            <span class="p-2" style="background: green; color: white">Walked</span>
                        @else
                            <span class="p-2" style="background: gray;color: white">Pending</span>
                        @endif
                    </td>
                    <td>
                        {{ morphDate($property->created_at) }}
                    </td>
                    <td>
                        @if(!$property->walked)
                        <a class="btn btn-success" href="{{ route('user.walkProperty',$property) }}">
                            Walk
                        </a>
                            @else
                            <a class="btn btn-success" href="{{ route('user.walkProperty',$property) }}">
                                Project Tools
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
