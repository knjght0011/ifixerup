@extends('landingpage.app')
@section('title', 'Our Work')

@section('content')
<section id="project" class="container bg-white mt-5">
    <div class="container">

      <div class="section-title d-block d-md-flex justify-content-between" data-aos="zoom-out">
        <h1 class="text-blue">Our past and current projects</h2>
      </div>

      <div class="row content" data-aos="fade-up">
        
        <div class="container">
          <div class=" masonry">

            @forelse($projects as $key => $item)
              <div class="item">
                <div class="inner">
                  <div class="comparison-slider-wrapper mt-0">
                    <div class="comparison-slider">
                    <div class="overlay">AFTER</div>
                    <img src="{{$item->after && file_exists(public_path(Storage::url($item->after))) ? Voyager::image($item->after) : asset('assets/img/no-img.png')}}" alt="Project After">
                    <div class="resize">
                    <div class="overlay">BEFORE</div>
                      <!-- <img src="./assets/img/mask.png" alt="marioPhoto 1"> -->
                      <img src="{{$item->before && file_exists(public_path(Storage::url($item->before))) ? Voyager::image($item->before) : asset('assets/img/no-img.png')}}" alt="Project Fefore">
                    </div>
                    <div class="divider"><div class="handle"></div></div>
                  </div>
                  </div>
                </div>
              </div>
            @empty
              <div class="col-lg-12 col-12">
                  <p>No projects found</p>
              </div>
            @endforelse

          </div>
        </div>
          
      </div>
  </section><!-- End Current Project Section -->
  @endsection