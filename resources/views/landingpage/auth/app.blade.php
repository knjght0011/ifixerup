<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') - {{ env('APP_NAME') }}</title>
        <link rel="shortcut icon" type="image/x-icon" href="images/logo/favicon.ico">
        @include('landingpage.styles')
    </head>
<body>
    <main class="bg-light">
        @yield('content')
    </main>
</body>
@yield('javascript')
</html>
