@extends('landingpage.auth.app')
@section('title', 'Register')
@section('content')
<div class="m-0 justify-content-center row" style="height: 100vh;">
   <div class="d-flex justify-content-center my-auto col-sm-8 col-md-4">
      <div class="bg-authentication login-card mb-0 w-100 card">

        <div class="col-md-10 mx-auto my-5">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="notification">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
			<div id="first">
				<div class="myform form ">
					 <div class="logo mb-3">
                        <div class="col-md-12 text-center">
                            <a href="/"><img src="/assets/img/logo-blue.png" alt="" class="img-fluid" style="width: 80px"></a>
						 </div>
						 <div class="col-md-12 text-center my-4">
							<h3>Welcome to {{ env('APP_NAME') }}</h3>
						 </div>
					</div>
                    <form method="POST" action="{{ route('user.register') }}">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1" class="label-require">Name</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-white"
                                          id="basic-addon1"><i class="far fa-user"></i></span>
                                </div>
                                <input placeholder="Enter Name"
                                        type="text"
                                       class="form-control @error('email') is-invalid @enderror"
                                       name="name" value="{{ old('name') }}"
                                       required autocomplete="email" autofocus>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1" class="label-require">Email</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-white" id="basic-addon1"><i class="far fa-envelope"></i></span>
                                </div>
                                <input placeholder="Enter Email" id="email" type="email" class="form-control " name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1" class="label-require">Password</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-white" id="basic-addon1"><i class="far fa-lock"></i></span>
                                </div>
                                <input placeholder="Enter Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group text-center ">
                            <button type="submit" class=" btn btn-block mybtn btn-primary btn-blue">
                                Register</button>
                        </div>
                        <!-- <div class="col-md-12 ">
                            <div class="login-or">
                                <hr class="hr-or">
                                <span class="span-or">or</span>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <p class="text-center">
                                <a href="javascript:void();" class="google btn mybtn"><i class="fa fa-google-plus">
                                </i> Signup using Google
                                </a>
                            </p>
                        </div>
                        <div class="form-group">
                            <p class="text-center">Don't have account? <a href="#" id="signup">Sign up here</a></p>
                        </div> -->
                    </form>
                    <div align="center">
                        <a href="{{ route('user.login') }}">
                            Login
                        </a>
                    </div>
				</div>
			</div>
		</div>
      </div>
   </div>
</div>
@endsection
