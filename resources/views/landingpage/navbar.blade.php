 <!-- ======= Header ======= -->
 <header id="header" class="fixed-top d-flex align-items-center w-100 header-transparent ">
    <div class="container d-flex align-items-center">

      <div class="logo mr-auto d-flex">
        <a href="/"><img src="/assets/img/logo-blue.png" alt="" class="img-fluid"></a>
        <h1 class="text-light my-auto mx-2"><a href="/" class="text-blue">iFixerup</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->

      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="{{Request::is('/') ? 'active' : ''}}"><a href="/">Home</a></li>
          <li class="{{Request::is('our-work') ? 'active' : ''}}"><a href="/our-work">Our Work</a></li>
          <li id="nav-contact"><a href="/#contact">Contact Us</a></li>
          <li class="{{Request::routeIs('ivaluation.*') ? 'active' : ''}}">
              <a href="{{ route('ivaluation.ivaluation') }}">
                  iValuation
              </a></li>
          <li class="active-always"><a href="/estimate">Get Estimates</a></li>

            @if(Auth::guest())
            <li class="{{Request::is('login') ? 'active' : ''}}"><a href="{{ route('user.login') }}">Login/Register</a></li>
            @elseif(Auth::check() && Auth::user()->hasRole(['Walker','Admin']))
            <li><a href="{{ route('voyager.dashboard') }}">Admin Dashboard  <i class="fas fa-sign-in-alt"></i></a></li>
            @else
                <li class="{{Request::is('user.profile') ? 'active' : ''}}" >

                    <a href="{{ route('user.profile') }}" >
                        {{ \Illuminate\Support\Str::limit(Auth::user()->name,10) }}

                        <i class="fas fa-sign-in-alt"></i></a></li>
            @endif
        </ul>
      </nav><!-- .nav-menu -->

    </div>
</header><!-- End Header -->
