<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex flex-column justify-content-end align-items-center">
    <div id="heroCarousel" class="carousel carousel-fade w-100" data-ride="carousel">

        <!-- Slide 1 -->
        <div class="carousel-item active">
            <div class="carousel-container">
                <img src="./assets/img/banner.jpg" class="d-block w-100 position-absolute" alt="..."
                     style="height: 90%; top: 65px; object-fit: cover;">
                <img src="./assets/img/mask.png" class="d-block w-100 position-absolute" alt="..."
                     style="height: 90%; top: 65px; object-fit: cover;">
                <h1 class="animate__animated animate__fadeInDown text-uppercase">Facing Preforeclosure or short
                    sale?</h1>
                <p class="animate__animated animate__fadeInUp text-uppercase">We pay your mortgage while we renovate
                    your house.</p>
                <p class="animate__animated animate__fadeInUp">We also help to renovate your house while your has the
                    fund limit before listing on the market.</p>
                <p class="animate__animated animate__fadeInUp">Our promise to help you getting the top of dollars on
                    your house.</p>
                <div class="w-100 justify-content-center" align="center">
                    <form action="{{ route('search') }}" method="get">
                        <div class="input-group search mt-lg-3 mt-0 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-white no-border"><i class="icofont-search"></i></span>
                            </div>
                            <input autocomplete="off" type="text" class="form-control no-border" aria-label="Amount (to the nearest dollar)"
                                   placeholder="Enter an address, city or ZIP code" id="search" name="search">
                            <div class="input-group-append">
                                <button type="submit" class="input-group-text text-blue bg-white no-border">Search</button>
                            </div>
                        </div>
                    </form>
                    <div class="search autocomplete-items" align="left">

                    </div>
                </div>
                <h5 class="animate__animated animate__fadeInUp text-white my-4">Call us today</h5>
                <button class="btn btn-radius btn-blue animate__animated animate__fadeInUp mb-2 call-us" type="button">
                    <i class="icofont-phone text-blue icofont-2x"></i> &nbsp; &nbsp; &nbsp; 1-987-654-3210
                </button>
            </div>
        </div>
        <!-- <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon bx bx-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon bx bx-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a> -->
    </div>

</section><!-- End Hero -->
@include('landingpage.search.common_js')
