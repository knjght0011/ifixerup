@extends('landingpage.app')
@section('title', 'Homepage')

@section('content')
    <!-- ======= About Section ======= -->
    <section id="about" class="container about bg-white">
        <div class="container">

            <div class="section-title d-block d-md-flex justify-content-between" data-aos="zoom-out">
                <h1 class="text-blue">How We Work</h1>
                <a href="/about-us" class="btn-outline-blue">Let's Start <i class='bx bxs-chevron-right ml-3'></i></a>
            </div>

            <div class="row content" data-aos="fade-up">
                <div class="col-lg-12 col-12">
                    <div class="testimonial-item">
                        <ul>
                            <li>
                                <i class="bx bxs-quote-left quote-icon-left"></i>
                                We pay your mortgage while we renovate your house. <br/>
                                With preforeclosure taken care of, it's just a matter of turning a bad situation into a
                                good opportunity. <br/>
                                We get paid when you sold your house.
                                <i class="bx bxs-quote-right quote-icon-right position-relative"></i>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
    </section><!-- End About Section -->

    <section id="tool" class="container tool">
        <!-- <div class="container"> -->
        <div class="row" data-aos="fade-up">
            <div class="col-12 p-0">
                <div class="card-deck d-block d-md-flex">

                    <div class="card no-border">
                        <img class="card-img-top w-50 mx-auto my-4" src="./assets//img/get-estimate.png"
                             alt="Card image cap">
                        <div class="card-body card-block text-center">
                            <h4 class="card-title text-blue">Get Estimates</h4>
                            <p class="card-text">Use your free online tool to get estimates <br/> and create a budget.
                            </p>
                        </div>
                        <div class="card-footer text-center bg-white no-border">
                            <a href="/estimate" class="btn btn-blue btn-block">LAUNCH TOOL</a>
                        </div>
                    </div>

                    <div class="card no-border">
                        <img class="card-img-top w-50 mx-auto my-4" src="./assets/img/renovate.png"
                             alt="Card image cap">
                        <div class="card-body card-block text-center">
                            <h4 class="card-title text-blue">Renovate</h4>
                            <p class="card-text">Our renovation experts work to ensure that your house will look
                                attractive to buyers once it is put on the market.</p>
                        </div>
                        <div class="card-footer text-center bg-white no-border">
                            <a href="/our-work" class="btn btn-blue btn-block">SEE OUR PROJECT</a>
                        </div>
                    </div>

                    <div class="card no-border">
                        <img class="card-img-top w-50 mx-auto my-4" src="./assets/img/sell.png" alt="Card image cap">
                        <div class="card-body card-block text-center">
                            <h4 class="card-title text-blue">Sell</h4>
                            <p class="card-text">Your house is now ready to be sold. <br/> We get paid when your house
                                is sold</p>
                        </div>
                        <div class="card-footer text-center bg-white no-border">
                            <a href="/about-us" class="btn btn-blue btn-block">LEARN MORE</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- </div> -->
    </section>

    <!-- ======= Direct Section ======= -->
    <section id="direct" class="direct container p-0 bg-white">
        <div class="aos-init aos-animate" data-aos="fade-up">
            <div class="row">
                <div class="col-lg-6 col-12" style="padding: 50px;">
                    <h1 class="text-blue">Preforeclosure Direct</h1>
                    <p>We directly buy your home so you will have money to move</p>
                    <p class="d-block d-md-flex justify-content-between">
                        <span class="d-block"><i class="ri-check-double-line text-blue"></i> No showings</span>
                        <span class="d-block"><i class="ri-check-double-line text-blue"></i> Flexible closing</span>
                        <span class="d-block"><i class="ri-check-double-line text-blue"></i> Hussle free</span>
                    </p>
                    <a href="/direct" class="btn-outline-blue">Sell now <i class='bx bxs-chevron-right ml-3'></i> </a>
                </div>
                <div class="col-lg-6 col-12">
                    <img src="./assets/img/direct.jpg" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </section><!-- End Direct Section -->

    <!-- ======= Current Project ======= -->
    <section id="project" class="container bg-white mt-5">
        <div class="container">

            <div class="section-title d-block d-md-flex justify-content-between" data-aos="zoom-out">
                <h1 class="text-blue">Our past and current projects</h1>
                    <a href="/our-work" class="btn-outline-blue">See all <i class='bx bxs-chevron-right ml-3'></i></a>
            </div>

            <div class="row content" data-aos="fade-up">

                <div class="container">
                    <div class=" masonry">

                        @forelse($projects as $key => $item)
                            <div class="item">
                                <div class="inner">
                                    <div class="comparison-slider-wrapper mt-0">
                                        <div class="comparison-slider">
                                            <div class="overlay">AFTER</div>
                                            <img
                                                src="{{$item->after && file_exists(public_path(Storage::url($item->after))) ? Voyager::image($item->after) : asset('assets/img/no-img.png')}}"
                                                alt="Project After">
                                            <div class="resize">
                                                <div class="overlay">BEFORE</div>
                                                <!-- <img src="./assets/img/mask.png" alt="marioPhoto 1"> -->
                                                <img
                                                    src="{{$item->before && file_exists(public_path(Storage::url($item->before))) ? Voyager::image($item->before) : asset('assets/img/no-img.png')}}"
                                                    alt="Project Fefore">
                                            </div>
                                            <div class="divider">
                                                <div class="handle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="col-lg-12 col-12">
                                <p>No projects found</p>
                            </div>
                        @endforelse

                    </div>
                </div>

            </div>
        </div>
    </section><!-- End Current Project Section -->

    <!-- ======= Why Us ======= -->
    <section id="why-us" class="container why-us bg-blue mt-5">
        <div class="container">
            <div class="row content" data-aos="fade-up">

                <div class="col-lg-12 col-12">
                    <div class="testimonial-item text-center text-white">
                        <h1>Why {{ env('APP_NAME') }}?</h1>
                        <p class="my-4">
                            We help you to not loosing your home while you have money to move
                        </p>
                        <a href="/about-us" class="btn btn-outline-white">Learn more</a>
                    </div>
                </div>

            </div>
        </div>
    </section><!-- End Why Us Section -->
    <style>
        .card-header {
             background-color: white;
        }
    </style>
    <section id="faqs" class="container bg-white mt-5" style="border: 1px solid #00c3ff ">
        <div class="container">
            <div class="row content" data-aos="fade-up">
                <div class="col-lg-12 col-12">
                    <div class="testimonial-item text-center text-blue">
                        <h1>{{ env('APP_NAME') }} FAQs</h1>
                    </div>
                    <div class="accordion row" id="accordionExample">
                        @foreach($faqs as $item)
                            <div class="card col-12 offset-md-2 col-md-8">
                                <div class="card-header" id="heading_{{ $loop->iteration }}">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button"
                                                data-toggle="collapse" data-target="#collapse_{{ $loop->iteration }}"
                                                aria-expanded="true" aria-controls="collapse_{{ $loop->iteration }}">
                                            {{ $loop->iteration }}. {{$item->title}}
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapse_{{ $loop->iteration }}" class="collapse"
                                     aria-labelledby="heading_{{ $loop->iteration }}"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        {{$item->content}}
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>

                </div>

            </div>
    </section><!-- End Why Us Section -->
    <section id="guild" class="container bg-white mt-5" style="border: 1px solid #00c3ff ">
        <div class="container">
            <div class="row content" data-aos="fade-up">
                <div class="col-lg-12 col-12">
                    <div class="testimonial-item text-center text-blue">
                        <h1>{{ env('APP_NAME') }} Blog</h1>
                        Everything you need to know about iFixerup.
                    </div>
                    <div class="px-md-5 pt-4">
                        <div class="row justify-content-between" align="center">
                            @foreach($pages as $page)
                                <a href="{{ route('blogs',['title'=>\Illuminate\Support\Str::slug($page->title),'id'=>$page->id]) }}"
                                   class="col-md-4 col-12  text-white text-decoration-none p-2" >
                                   <div style="border-radius:10%; min-height: 350px" class="bg-blue">
                                       <div style="height: 220px">
                                           <img  class="img-fluid" src="{{ Voyager::image($page->image) }}" alt="">
                                       </div>
                                       <div style="height: 200px" class="p-4">
                                           <h3>
                                               {{ $page->title }}
                                           </h3>
                                           <p class="">
                                               {{ $page->excerpt }}
                                           </p>
                                       </div>
                                   </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="container contact bg-white mt-5">
        <div class="container">

            <div class="row">

                <div class="col-lg-6 col-12" data-aos="fade-right">
                    <h1 class="text-blue mb-3">Contact Us</h1>
                    <p class="my-4">Get in touch and we'll get back to you!</p>
                    <div class="info">
                        <div class="phone mt-5">
                            <i class="icofont-phone mr-3"></i>
                            <h6>1-987-654-3210</h6>
                        </div>

                        <div class="email my-4">
                            <i class="icofont-envelope mr-3"></i>
                            <h6>info@example.com</h6>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-12" data-aos="fade-left">

                    <form action="{{route('contact')}}" method="post" role="form" class="php-email-form">
                        @csrf
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name"
                                   data-rule="minlen:4" data-msg="Please enter at least 4 chars (excluding spaces)"
                                   maxlength="100"/>
                            <div class="validate"></div>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" id="subject" placeholder="Email"
                                   data-rule="minlen:4"
                                   data-msg="Please enter at least 8 chars of email (excluding spaces)"
                                   maxlength="100"/>
                            <div class="validate"></div>
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control" name="message" rows="5" data-rule="required"
                                      data-msg="Please write something for us" placeholder="Message"
                                      maxlength="1000"></textarea>
                            <div class="validate"></div>
                        </div>
                        <div class="mb-3">
                            <div class="loading">Loading</div>
                            <div class="error-message"></div>
                            <div class="sent-message">Your message has been sent. Thank you!</div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-blue btn-block">Send Message</button>
                        </div>
                    </form>

                </div>

            </div>

        </div>
    </section><!-- End Contact Section -->

    <!-- </main>End #main -->
@endsection
