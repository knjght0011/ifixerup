@extends('landingpage.app')
@section('title', 'About Us')
@section('content')
<!-- ======= About Section ======= -->
<section id="about" class="container about bg-white mt-5">
      
    <div class="container">

        <div class="section-title d-block p-0" data-aos="fade-up">
            <h1 class="text-blue">Our Mission</h1>
        </div>

        <div class="row content mt-4" data-aos="fade-up">
            <div class="col-12">
                <div class="mission">
                    {!! isset($content->content) ? $content->content : '<p class="mb-3">No content found</p>' !!}
                    <!-- <ul>
                        <li>
                            <i class='bx bxs-square text-blue mr-3'></i>With U-preforeclosure, we help the people who are losing their homes. Real estate business is not about selling as many houses as possible, but how many windows are lit up every night. We don’t want to see more homes in our neighborhood with all windows and doors boarded up and covered with graffiti.
                        </li>
                        <li>
                            <i class='bx bxs-square text-blue mr-3'></i>Our goal to keep homeowners get out of their current predicament, and move a better place.
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>

        <div class="section-title d-block p-0" data-aos="fade-up">
            <h1 class="text-blue">Our Team</h1>
        </div>

        <div class="row content mt-4" data-aos="fade-up">
            
            <div class="col-12 d-md-flex d-block justify-content-center our-team">
                @forelse ($teams as $team)
                    <div class="team-item text-center">
                        <img src="{{$team->avatar && file_exists(public_path(Storage::url($team->avatar))) ? Voyager::image($team->avatar) : asset('assets/img/no-img.png')}}" alt="team" class="rounded-circle">
    
                        <div class="info">
                            <h4 class="text-blue">{{$team->name}}</h4>
                            <p>{{$team->position}}</p>
                        </div>
                    </div>
                @empty
                    <p>data not available</p>
                @endforelse                
            </div>
        </div>
</section><!-- End About Section -->
<script>
    $(document).ready(function() {
        $('.mission ul li').append('<i class="bx bxs-square text-blue mr-3"></i>')
    });
</script>
@endsection