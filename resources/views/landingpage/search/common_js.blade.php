@push('script')
    <script>
        var timeout = null;
        $('#search').keyup(function () {
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    data: {search:$('#search').val()},
                    url: '{{ route('api.map.search') }}',
                    success: function (result) {
                        var items  ='';
                        if(result.status=='success') {
                            $.each(result.data, function (key,value){
                                items+=' <a href="{{ route('search') }}?search='+key+'"><div class="p-3">'+ value +'</div></a>';
                            })
                        }
                        $('.autocomplete-items').html(items)
                    }
                });
            }, 500);
        });

    </script>
@endpush
