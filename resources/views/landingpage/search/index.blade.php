@extends('landingpage.app')
@section('title', 'Real Estimate')
@section('content')
<section id="" class="container mt-5 py-0">
    <div class="row">

        <div class="col-lg-5 col-12 bg-white " data-aos="flip-up">
            <div class="input-group h-100">
                <input type="text"
                       class="form-control no-border h-100 no-outline"
                       style="height: 100%" aria-label="Amount (to the nearest dollar)"
                       placeholder="Enter an address, city or ZIP code"
                       id="search" name="search"
                       value="{{ Request::input('search','') }}"
                >
                <div class="input-group-append">
                    <span class="input-group-text bg-white no-border"><i class="icofont-search"></i></span>
                </div>
            </div>

        </div>

        <div class="col-lg-5 col-12 px-lg-2 px-0 my-lg-0 my-2" data-aos="flip-up">
            <nav class="nav-menu bg-white">
                <ul class="d-block d-md-flex justify-content-between text-center">
                    <li class="w-100 bg-white"><a href="/">Price</a></li>
                    <li class="w-100 bg-white border-left border-right"><a href="/our-work">Beds & Baths</a></li>
                    <li class="w-100 bg-white"><a href="/#contact">Home Type</a></li>
                </ul>
            </nav>

        </div>

        <div class="col-lg-2 col-12 bg-white p-0" data-aos="flip-up">
            <button class="btn btn-block btn-blue h-100">More</button>
        </div>
        <div class="search autocomplete-items" align="left" style="margin-top: 52px; left: auto; transform: none">

        </div>

    </div>
</section><!-- End Contact Section -->

<section id="" class="container bg-white mt-4 p-0">
    <div class="container p-0">

        <div class="row">
            <div class="col-12" data-aos="fade-right">
                <div id="map"></div>
            </div>
        </div>

        <div class="row p-3">
            <div class="col-md-6 col-12 mt-3" data-aos="zoom" data-toggle="modal" data-target="#exampleModal">
                <div class="card font-weight-bold">
                    <img class="card-img-top" src="https://photos.zillowstatic.com/fp/1b2841e077e70eb1d270a28ac0722f02-cc_ft_576.jpg" alt="Card image cap">
                    <div class="card-body">
                        <h3 class="card-title">$205,000</h3>
                        <p class="card-text">
                            <span class="mr-3"><i class="fas fa-bed text-blue mr-1"></i>3</span>
                            <span class="mr-3"><i class="fas fa-bath text-blue mr-1"></i>3</span>
                            <span class="mr-3"><i class="fas fa-square text-blue mr-1"></i>1780 sqft</span>
                        </p>
                        <p class="ellipsis">14 Split Rock Dr, Waterbury, CT 06706 </p>
                        <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-12 mt-3" data-aos="zoom" data-toggle="modal" data-target="#exampleModal">
                <div class="card font-weight-bold">
                    <img class="card-img-top" src="https://photos.zillowstatic.com/fp/1b2841e077e70eb1d270a28ac0722f02-cc_ft_576.jpg" alt="Card image cap">
                    <div class="card-body">
                        <h3 class="card-title">$205,000</h3>
                        <p class="card-text">
                            <span class="mr-3"><i class="fas fa-bed text-blue mr-1"></i>3</span>
                            <span class="mr-3"><i class="fas fa-bath text-blue mr-1"></i>3</span>
                            <span class="mr-3"><i class="fas fa-square text-blue mr-1"></i>1780 sqft</span>
                        </p>
                        <p class="ellipsis">14 Split Rock Dr, Waterbury, CT 06706, 14 Split Rock Dr, Waterbury, CT 06706  14 Split Rock Dr, Waterbury, CT 06706 </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section><!-- End Contact Section -->
@include('landingpage.search.detail-modal')
@include('landingpage.search.common_js')
<script>
    $( document ).ready(function() {
        initMap();
        // getDataFromZillow();
    });

    function initMap() {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'address': "{{ Request::input('search') }}"
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {

                showMap(results[0].geometry.location.lat(),results[0].geometry.location.lng())

            }
            else
                showMap()
        });
    }
    function showMap(lat= 40.7858267,lng=-74.0050448)
    {
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 18,
            center: { lat: lat, lng: lng },
            mapTypeId: 'satellite'

        });
        // Create an array of alphabetical characters used to label the markers.
        const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
        new google.maps.Marker({
            position: { lat: lat, lng: lng },
            map
        });
        // const markers = locations.map((location, i) => {
        //     return new google.maps.Marker({
        //         position: location,
        //         label: labels[i % labels.length],
        //     });
        // });
        // // Add a marker clusterer to manage the markers.
        // new MarkerClusterer(map, markers, {
        //     imagePath:
        //         "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
        // });
    }
    const locations = [
        { lat: -31.56391, lng: 147.154312 },
        { lat: -33.718234, lng: 150.363181 },
        { lat: -33.727111, lng: 150.371124 },
        { lat: -33.848588, lng: 151.209834 },
        { lat: -33.851702, lng: 151.216968 },
        { lat: -34.671264, lng: 150.863657 },
        { lat: -35.304724, lng: 148.662905 },
        { lat: -36.817685, lng: 175.699196 },
        { lat: -36.828611, lng: 175.790222 },
        { lat: -37.75, lng: 145.116667 },
        { lat: -37.759859, lng: 145.128708 },
        { lat: -37.765015, lng: 145.133858 },
        { lat: -37.770104, lng: 145.143299 },
        { lat: -37.7737, lng: 145.145187 },
        { lat: -37.774785, lng: 145.137978 },
        { lat: -37.819616, lng: 144.968119 },
        { lat: -38.330766, lng: 144.695692 },
        { lat: -39.927193, lng: 175.053218 },
        { lat: -41.330162, lng: 174.865694 },
        { lat: -42.734358, lng: 147.439506 },
        { lat: -42.734358, lng: 147.501315 },
        { lat: -42.735258, lng: 147.438 },
        { lat: -43.999792, lng: 170.463352 },
    ];

    function getDataFromZillow(year) {
        // let param = '?searchQueryState={"pagination":{},"usersSearchTerm":"987","mapBounds":{"west":-66.87721502807618,"east":-65.79780828979493,"south":17.83777453123829,"north":18.603990776344915},"regionSelection":[{"regionId":58194,"regionType":7}],"isMapVisible":true,"filterState":{"isAllHomes":{"value":true}},"isListVisible":true}&wants={"cat1":["listResults","mapResults"],"cat2":["total"]}'
        // $.ajax({
        //     type:'GET',
        //     url: 'http://www.zillow.com/webservice/GetSearchResults.htm'+param,
        //     success: function(res) {
        //         console.log(res);
        //     },
        //     error: function(err) {
        //         console.log(err)
        //     }
        // })
        const settings = {
            // "async": true,
            // "crossDomain": true,
            "url": "http://www.zillow.com/webservice/GetSearchResults.htm?zws-id=X1-ZWz16pq6mflxqj_1fz14&address=2114+Bigelow+Ave&citystatezip=Seattle%2C+WA",
            "method": "GET",
            // "headers": {
            //     "content-type": "application/x-www-form-urlencoded",
            //     "x-rapidapi-key": "2fdd756ad7msh51da7149fd9c33fp1d7851jsn83a0d80002f6",
            //     "x-rapidapi-host": "ZillowdimashirokovV1.p.rapidapi.com"
            // },
            // "data": {
            //     "zws-id": "X1-ZWz16pq6mflxqj_1fz14",
            //     "citystatezip": "81237",
            //     "address": "ohio city",
            //     "rentzestimate": "[true,false]"
            // }
        };

        $.ajax(settings).done(function (response) {
            console.log(response);
        });
    }


</script>
@endsection
