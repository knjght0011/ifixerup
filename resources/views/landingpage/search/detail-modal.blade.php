<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="row">
                    <div class="col-12 mb-2 p-0">
                        <img class="card-img-top rounded-0" src="https://photos.zillowstatic.com/fp/1b2841e077e70eb1d270a28ac0722f02-cc_ft_576.jpg" alt="Card image cap">
                    </div>
                    <div class="col-12 col-md-6 mb-2 p-0 pr-1">
                        <img class="card-img-top rounded-0" src="https://photos.zillowstatic.com/fp/1b2841e077e70eb1d270a28ac0722f02-cc_ft_576.jpg" alt="Card image cap">
                    </div>
                    <div class="col-12 col-md-6 mb-2 p-0 pl-1">
                        <img class="card-img-top rounded-0" src="https://photos.zillowstatic.com/fp/1b2841e077e70eb1d270a28ac0722f02-cc_ft_576.jpg" alt="Card image cap">
                    </div>
                    <div class="col-12 col-md-6 mb-2 p-0 pr-1">
                        <img class="card-img-top rounded-0" src="https://photos.zillowstatic.com/fp/1b2841e077e70eb1d270a28ac0722f02-cc_ft_576.jpg" alt="Card image cap">
                    </div>
                    <div class="col-12 col-md-6 mb-2 p-0 pl-1">
                        <img class="card-img-top rounded-0" src="https://photos.zillowstatic.com/fp/1b2841e077e70eb1d270a28ac0722f02-cc_ft_576.jpg" alt="Card image cap">
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <h3>$205,000</h3>
                <small><span class="text-muted">Est. payment: </span>$1.176/mo</small>
                <p class="card-text mt-3">
                    <span class="mr-3"><i class="fas fa-bed text-blue mr-1"></i>3</span>
                    <span class="mr-3"><i class="fas fa-bath text-blue mr-1"></i>3</span>
                    <span class="mr-3"><i class="fas fa-square text-blue mr-1"></i>1780 sqft</span>
                </p>
                <p class="ellipsis">14 Split Rock Dr, Waterbury, CT 06706, 14 Split Rock Dr, Waterbury, CT 06706  14 Split Rock Dr, Waterbury, CT 06706 </p>
                <hr/>
                <div>
                    <ul class="nav justify-content-center mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Facts & features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Interior details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Property details</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">Facts and features</div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">Interior details</div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">Property details</div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-outline-blue" data-dismiss="modal">Contact Agent</button>
        <button type="button" class="btn btn-sm btn-primary btn-blue">Take a Tour</button>
      </div>
    </div>
  </div>
</div>