@extends('landingpage.app')
@section('title', 'Estimate')
@section('content')
<?php // dd($property_info) ?>
<section id="" class="container estimate bg-white mt-5">
    <div class="container">

        <div class="row">

            <div class=" col-12 text-center" data-aos="fade-right">
            <h1 class="text-blue">Thank You!</h2>
            </div>
            <div class="col-lg-3 col-12"></div>
            <div class="col-lg-6 col-12 text-center py-4" data-aos="fade-right">
            <p class="text-black text-center">Your property information has been submitted. Someone from our team will contact you shortly</p>
            </div>

            <div class="col-12" data-aos="fade-left">
                <h5 class="text-blue py-3">Property Info</h5>
                <div class="table-responsive">
                <table class="table table-hover table-custom">
                    <!-- <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Handle</th>
                        </tr>
                    </thead> -->
                    <tbody>
                        <tr>
                            <td>Project Name</td>
                            <td>{{$property_info->project_name}}</td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>{{$property_info->address}}, {{$property_info->city}}, {{$property_info->state}}, {{$property_info->zip_code}}</td>
                        </tr>
                        <tr>
                            <td>Date Created</td>
                            <td>{{convertSystemTzToUserTz($property_info->created_at)}}</td>
                        </tr>
                        <tr>
                            <td>Main SQFt</td>
                            <td>{{$property_info->main_square_ft}}</td>
                        </tr>
                        <tr>
                            <td>No. of Bedrooms</td>
                            <td>{{$property_info->number_bedrooms}}</td>
                        </tr>
                        <tr>
                            <td>No. of Bathrooms</td>
                            <td>{{$property_info->number_bathrooms}}</td>
                        </tr>
                        <tr>
                            <td>Year Built</td>
                            <td>{{$property_info->year_build}}</td>
                        </tr>
                        <tr>
                            <td>Comments</td>
                            <td>{!! $property_info->comments !!}</td>
                        </tr>
                    </tbody>
                </table>
                <div>
            </div>

        </div>

        <div class="row mt-3">
            <div class="col-lg-3 col-12" data-aos="fade-left"></div>
            <div class="col-lg-6 col-12" data-aos="fade-left">
                <a href="/estimate">
                    <button class="btn btn-blue btn-block">CLOSE</button>
                </a>
            </div>
            <div class="col-lg-3 col-12" data-aos="fade-left"></div>
        </div>

    </div>
  </section><!-- End Contact Section -->
@endsection