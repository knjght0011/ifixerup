There are customers submitting property estimated information. Here are the details:
<p> Project Name: {{ $project_name }} </p>
<p> Address: {{ $address }} </p>
<p> City: {{$city}} </p>
<p> State: {{$state}} </p>
<p> ZipCode: {{$zip_code}} </p>
<p> Country: {{$county}} </p>
<p> Main Square FT : {{$main_square_ft}} </p>
<p> Year Built: {{$year_build}} </p>
<p> Number Bathrooms: {{$number_bathrooms}} </p>
<p> Number Bedrooms: {{$number_bedrooms}} </p>
<p> Comment: {!! $comments !!} </p>
<p>Access to admin page to manage estimate requirements: <a href="http://house-platform.kendemo.com/admin">http://house-platform.kendemo.com/admin</a>