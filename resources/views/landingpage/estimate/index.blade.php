@extends('landingpage.app')
@section('title', 'Estimate')
@section('content')
    <section id="" class="container estimate contact bg-white mt-5">
        <div class="container">

            <div class="row">

                <div class="col-lg-6 col-12" data-aos="fade-right">
                    <h1 class="text-blue">Estimates</h1>
                    <p class="mb-4 text-muted">Please enter your properties info</p>
                </div>

                <div class="col-12" data-aos="fade-left">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{route('estimate')}}" method="post" role="form" enctype="multipart/form-data"
                        {{--                class="php-email-form"--}}
                    >
                        @csrf
                        <div class="form-row">
{{--                            <div class="col-lg-6 col-12">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label>Project Name</label>--}}
{{--                                    <input type="text"--}}
{{--                                           name="project_name" class="form-control" id="project_name"--}}
{{--                                           placeholder="Project Name"--}}
{{--                                           data-rule="minlen:4"--}}
{{--                                           value="{{ old('project_name') }}"--}}

{{--                                           data-msg="Please enter at least 4 chars (excluding spaces)" maxlength="100"/>--}}
{{--                                    <div class="validate"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label class="label-require"> Address</label>
                                    <input type="text" name="address" class="form-control" id="address"
                                           value="{{ old('address') }}"
                                           placeholder="Address" data-rule="minlen:4"
                                           required
                                           data-msg="Please enter at least 4 chars (excluding spaces)" maxlength="150"/>
                                    <div class="validate"></div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label class="label-require"> City</label>
                                    <input type="text" name="city" class="form-control" id="city"
                                           value="{{ old('city') }}"
                                           placeholder="City" data-rule="required"
                                           required
                                           data-msg="Please enter city (excluding spaces)" maxlength="100"/>
                                    <div class="validate"></div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label class="label-require"> State</label>
                                    <input type="text" name="state" class="form-control"
                                           value="{{ old('state') }}"
                                           required
                                           id="state" placeholder="State" data-rule="required"
                                           data-msg="State is required and must be less than 2 chars (excluding spaces)"
                                           maxlength="2"/>
                                    <div class="validate"></div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label class="label-require">Zip Code</label>
                                    <input type="text" class="form-control" name="zip_code"
                                           value="{{ old('zip_code') }}"
                                           required
                                           id="zip_code" placeholder="Zipcode" onKeyPress="if(this.value.length==5) return false;" data-rule="minlen:5" data-rule="maxlen:5"
                                           oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="5"
                                           data-msg="Please enter 5 chars (excluding spaces)"/>
                                    <div class="validate"></div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label>Country</label>
                                    <input type="text" name="country"
                                           value="{{ old('country') }}"
                                           class="form-control" id="country" placeholder="Your country"
                                           data-rule="minlen:4"
                                           data-msg="Please enter at least 4 chars (excluding spaces)" maxlength="100"/>
                                    <div class="validate"></div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label >Main Square FT </label>
                                    <input type="text" class="form-control" name="main_square_ft"
                                           value="{{ old('main_square_ft') }}"
                                           id="main_square_ft" placeholder="Main Square FT"
                                           oninput="this.value = this.value.replace(/[^0-9.]/g, '');"
                                           data-rule="regexp:^((\d+\.\d{1,2})|(\d+)|(\.\d{1,2}))$"
                                           data-msg="Please enter a valid Main Square FT" maxlength="10"/>
                                    <div class="validate"></div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label>Year Built</label>
                                    <input type="text" name="year_build"
                                           value="{{ old('year_build') }}"
                                           class="form-control"
                                           id="year_build" placeholder="Year Built" data-rule="minlen:4"
                                           data-rule1="maxyear"
                                           oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="4"
                                           data-msg="Please enter 4 chars"
                                           data-msg1="Year Built must be less than current year"/>
                                    <div class="validate"></div>
                                    <div class="validate1"></div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label>Number Bathrooms</label>
                                    <?php
                                    $bathroom = [
                                        '1' => '1',
                                        '1.5' => '1 &#189;',
                                        '2' => '2',
                                        '2.5' => '2 &#189;',
                                        '3' => '3',
                                        '3.5' => '3 &#189;',
                                        '4' => '4',
                                        '4.5' => '4 &#189;',
                                        '5' => '5',
                                        '5.5' => '5 &#189;',
                                    ];
                                    ?>
                                    {!! Form::select('number_bathrooms',$bathroom,old('number_bathrooms'),['class'=>'form-control']) !!}
                                    {{--                  <select class="form-control" name="number_bathrooms" value="{{old('number_bathrooms') ? old('number_bathrooms') : ''}}" id="slNumberBathrooms">--}}
                                    {{--                      <option value="1">1</option>--}}
                                    {{--                      <option value="1.5">1 &#189;</option>--}}
                                    {{--                      <option value="2">2</option>--}}
                                    {{--                      <option value="2.5">2 &#189;</option>--}}
                                    {{--                      <option value="3">3</option>--}}
                                    {{--                      <option value="3.5">3 &#189;</option>--}}
                                    {{--                      <option value="4">4</option>--}}
                                    {{--                      <option value="4.5">4 &#189;</option>--}}
                                    {{--                      <option value="5">5</option>--}}
                                    {{--                      <option value="5.5">5 &#189;</option>--}}
                                    {{--                  </select>--}}
                                    <div class="validate"></div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label>Number Bedrooms</label>
                                    <input type="text" class="form-control" name="number_bedrooms" id="number_bedrooms"
                                           placeholder="Number Bedrooms" data-rule="required"
                                           oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="3"
                                           data-msg="Please enter number bedrooms"/>
                                    <div class="validate"></div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label>Comment</label>
                                    <input type="hidden" name="timezone" value="" id="timezone"/>
                                    <textarea class="form-control" name="comments" rows="3" placeholder="Comment"
                                              maxlength="250"></textarea>
                                    <div class="validate"></div>
                                </div>
                                {{--                <div class="mb-3">--}}
                                {{--                  <div class="loading">Loading</div>--}}
                                {{--                  <div class="error-message"></div>--}}
                                {{--                  <div class="sent-message">Successfully estimated. Thank you!</div>--}}
                                {{--                </div>--}}
                            </div>

                        </div>
                        @if(\Auth::guest() || (\Auth::check() && !\Auth::user()->hasRole('Customer')))
                            <h1 class="text-blue">Your Info</h1>
                            <div class="form-row">
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="label-require">Email</label>
                                        <input type="email" required name="email" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="label-require">Name</label>
                                        <input type="text" maxlength="255" required name="name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="label-require">Phone</label>
                                        <input type="number" required name="phone" class="form-control">
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="form-row mt-4">
                            <div class="col-lg-6 col-12 offset-lg-3">
                                <div class="form-group text-center">
                                    <label for="uploadImage" class="mb-3">
                                        <input type="checkbox" name="upload_image" value="1" id="uploadImage">
                                        I want to upload the image and {{ env('APP_NAME') }} complete the budget
                                    </label>

                                    <div align="left" id="inputImage" class="mb-5 d-none">
                                        <label for="" class="label-require">Choose images to upload</label>
                                        <input type="file" name="images[]"  accept="image/*" multiple>
                                    </div>
                                    <!-- <a href="/scope-of-work"> -->
                                    <button type="submit" class="btn btn-blue btn-block">SUBMIT</button>
                                    <!-- <button type="button" class="btn btn-blue btn-block">SUBMIT</button> -->
                                    <!-- </a> -->
                                </div>
                            </div>
                        </div>

                    </form>

                </div>

            </div>

        </div>
    </section><!-- End Contact Section -->
    <script>
        $(document).ready(function () {
            document.getElementById('timezone').value = Intl.DateTimeFormat().resolvedOptions().timeZone
        })
        $('#uploadImage').click(function () {
            if ($('#uploadImage').is(':checked')) {
                $('#inputImage > input').prop('required','required')
                $('#inputImage').removeClass('d-none');
            } else {
                $('#inputImage > input').prop('required','')
                $('#inputImage').addClass('d-none')
            }
        })
    </script>
@endsection
