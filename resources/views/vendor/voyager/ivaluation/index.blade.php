@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' iValuation')
    {{--@section('breadcrumbs')--}}
    {{--@endsection--}}
@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
           <i class="voyager-dollar"></i> iValuation
        </h1>
    </div>
@stop

@section('content')

    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        {{--                        @if ($isServerSide)--}}

                        {{--                        @endif--}}
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>

                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Address
                                    </th>
                                    <th>
                                        Main Square FT
                                    </th>
                                    <th>
                                        Number Bedrooms
                                    </th>
                                    <th>
                                        Home Type
                                    </th>
                                    <th>
                                        Customer
                                    </th>
                                    <th>
                                        Valuation
                                    </th>
                                    <th>
                                        Created At
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $datum)
                                    <tr>
                                        <td>
                                            {{ $loop->iteration }}
                                        </td>
                                        <td>
                                            {{ $datum->address }}
                                        </td>
                                        <td>
                                            {{ number_format($datum->main_square_ft) }}
                                        </td>
                                        <td>
                                            {{ number_format($datum->bedroom) }}
                                        </td>
                                        <td>
                                            {{ \App\Models\iValuation::home_type()[$datum->home_type] }}
                                        </td>
                                        <td>
                                            {{ $datum->user->name }}
                                            <br>
                                            {{ $datum->user->phone }}
                                            <br>
                                            {{ $datum->user->email }}
                                        </td>
                                        <td>
                                            {{ formatCurrency($datum->valuation) }}
                                        </td>
                                        <td>
                                            {{ morphDate($datum->created_at) }}
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {!! $data->appends(Request::input())->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop


@section(/** @lang text */'javascript')

@stop
