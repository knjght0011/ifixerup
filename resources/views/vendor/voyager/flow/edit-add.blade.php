@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif

                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body ">
                        <div class="col-md-8">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)

                                @php
                                    $display_options = $row->details->display ?? null;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}"
                                            style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif
								 @if($row->field == 'name' || $row->field == 'description' || $row->field == 'hourly_labor_rate' || $row->field == 'tenant_responsibility' || $row->field == 'service_type')
                                <div
                                    class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}

                                    <label class="control-label {{ ($row->required ||
(isset($row->details) && isset($row->details->required)))?"label-require":"" }}
"
                                           for="name">{{ $row->getTranslatedAttribute('display_name') }}
                                        @if($row->details && property_exists($row->details,'currency'))
                                            ({{ env('CURRENCY_PREFIX',env('CURRENCY_SUFFIX','$')) }})
                                        @endif
                                    </label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                    @elseif ($row->type == 'relationship')

                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach



                                </div>

							  @endif

                            @endforeach
							</div>
							<div class="col-md-4">
								@if(isset($flow->updated_at))
									<p>Updated: <span>{{ date('m/d/Y H:i:s', strtotime($flow->updated_at)) }}</span></p>
								@endif
								@if(isset($flow->updated_at))
									<p>Updated By: <span>@if(isset($flow->update_by)){{ $flow->user->name }}@endif</span></p>
								@endif
							</div>

							<div class="table-responsiv">
							   <table class="table flowAddGrid">
								  <thead>
									 <tr>
										<th class="col-md-2"> </th>
										<th class="col-md-1">Material </th>
										<th class="col-md-1">Store Number </th>
										<th class="col-md-1">SKU </th>
										<th class="col-md-1">Coverage Per SKU </th>
										<th class="col-md-1">Labor </th>
										<th class="col-md-1">Waste Factor % </th>
										<th class="col-md-1">Removal   </th>
									 </tr>
								  </thead>
								  <tbody>
									 <tr>
										<td>Carpet Per SqYd : </td>
										<td>
										   <label id="CarpetMaterialLabel" for="CarpetMaterial" class="sr-only">Carpet Material</label>
										   <input name="CarpetMaterial" value="@if(isset($flow->CarpetMaterial) && $flow->CarpetMaterial != ''){{$flow->CarpetMaterial}}@endif" type="number" max="9999999999999.9999" class="form-control" id="CarpetMaterial" placeholder="" >

										</td>
										<td>
										   <label id="CarpetStoreNumberLabel" for="CarpetStoreNumber" class="sr-only">Carpet Store Number</label>
										   <input name="CarpetStoreNumber" value="@if(isset($flow->CarpetStoreNumber) && $flow->CarpetStoreNumber != ''){{$flow->CarpetStoreNumber}}@endif" type="number" max="9999"class="form-control" id="CarpetStoreNumber" placeholder="" maxlength="4">

										</td>
										<td>
										   <label id="CarpetSKULabel" for="CarpetSKU" class="sr-only">Carpet SKU</label>
										   <input name="CarpetSKU" value="@if(isset($flow->CarpetSKU)){{$flow->CarpetSKU}}@endif" type="number" max="999999999" class="form-control" id="CarpetSKU" placeholder="" maxlength="10">

										</td>
										<td>  </td>
										<td>
										   <label id="CarpetLaborLabel" for="CarpetLabor" class="sr-only">Carpet Labor</label>
										   <input name="CarpetLabor" value="@if(isset($flow->CarpetLabor) && $flow->CarpetLabor != ''){{$flow->CarpetLabor}}@endif" type="number" max="9999999999999.9999" class="form-control" id="CarpetLabor" placeholder="" >

										</td>
										<td>
										   <span class="input-group">
											  <label id="CarpetWasteFactorLabel" for="CarpetWasteFactor" class="sr-only">Carpet Waste Factor</label>
											  <input name="CarpetWasteFactor" value="@if(isset($flow->CarpetWasteFactor) && $flow->CarpetWasteFactor != ''){{$flow->CarpetWasteFactor}}@endif" min="0" max="999.99" step="0.01" type="number" max="999999999" class="form-control" id="CarpetWasteFactor" placeholder="" >

											  <span class="input-group-addon">%</span>
										   </span>
										</td>
										<td>
										   <label id="CarpetRemovalCostLabel" for="CarpetRemovalCost" class="sr-only">Carpet Removal</label>
										   <input name="CarpetRemovalCost" value="@if(isset($flow->CarpetRemovalCost) && $flow->CarpetRemovalCost != ''){{$flow->CarpetRemovalCost}}@endif" type="number" max="9999999999999.9999" class="form-control" id="CarpetRemovalCost" placeholder="" >

										</td>
									 </tr>
									 <tr>
										<td>Carpet Pad Per SqYd : </td>
										<td>
										   <label id="CarpetPadMaterialLabel" for="CarpetPadMaterial" class="sr-only">Carpet Pad Material</label>
										   <input name="CarpetPadMaterial" value="@if(isset($flow->CarpetPadMaterial) && $flow->CarpetPadMaterial != ''){{$flow->CarpetPadMaterial}}@endif" type="number" max="9999999999999.9999" class="form-control" id="CarpetPadMaterial" placeholder="" >

										</td>
										<td>
										   <label id="CarpetPadStoreNumberLabel" for="CarpetPadStoreNumber" class="sr-only">Carpet Pad Store Number</label>
										   <input name="CarpetPadStoreNumber" value="@if(isset($flow->CarpetPadStoreNumber) && $flow->CarpetPadStoreNumber != ''){{$flow->CarpetPadStoreNumber}}@endif" type="number" max="9999" class="form-control" id="CarpetPadStoreNumber" placeholder="" maxlength="4">

										</td>
										<td>
										   <label id="CarpetPadSKULabel" for="CarpetPadSKU" class="sr-only">Carpet Pad SKU</label>
										   <input name="CarpetPadSKU" value="@if(isset($flow->CarpetPadSKU)){{$flow->CarpetPadSKU}}@endif" type="number" max="999999999" class="form-control" id="CarpetPadSKU" placeholder="" maxlength="10">

										</td>
										<td>
										   <label id="CarpetPadSqYdPerSKULabel" for="CarpetPadSqYdPerSKU" class="sr-only">Carpet Pad Coverage</label>
										   <input name="CarpetPadSqYdPerSKU" value="@if(isset($flow->CarpetPadSqYdPerSKU) && $flow->CarpetPadSqYdPerSKU != ''){{$flow->CarpetPadSqYdPerSKU}}@endif" type="number" max="9999.99" class="form-control" id="CarpetPadSqYdPerSKU" placeholder="" >

										</td>
										<td>
										   <label id="CarpetPadLaborLabel" for="CarpetPadLabor" class="sr-only">Carpet Pad Labor</label>
										   <input name="CarpetPadLabor" value="@if(isset($flow->CarpetPadLabor) && $flow->CarpetPadLabor != ''){{$flow->CarpetPadLabor}}@endif" type="number" max="9999999999999.9999" class="form-control" id="CarpetPadLabor" placeholder="" >

										</td>
										<td>
										   <span class="input-group">
											  <label id="CarpetPadWasteFactorLabel" for="CarpetPadWasteFactor" class="sr-only">Carpet Pad Waste Factor</label>
											  <input name="CarpetPadWasteFactor" value="@if(isset($flow->CarpetPadWasteFactor) && $flow->CarpetPadWasteFactor != ''){{$flow->CarpetPadWasteFactor}}@endif" min="0" max="999.99" step="0.01" type="number" max="999999999" class="form-control" id="CarpetPadWasteFactor" placeholder="" >

											  <span class="input-group-addon">%</span>
										   </span>
										</td>
										<td>   </td>
									 </tr>
									 <tr>
										<td>Linoleum Per SqFt : </td>
										<td>
										   <label id="LinoleumMaterialLabel" for="LinoleumMaterial" class="sr-only">Linoleum Material</label>
										   <input name="LinoleumMaterial" value="@if(isset($flow->LinoleumMaterial) && $flow->LinoleumMaterial != ''){{$flow->LinoleumMaterial}}@endif" type="number" max="999999999" class="form-control" id="LinoleumMaterial" placeholder="" >

										</td>
										<td>
										   <label id="LinoleumStoreNumberLabel" for="LinoleumStoreNumber" class="sr-only">Linoleum Store Number</label>
										   <input name="LinoleumStoreNumber" value="@if(isset($flow->LinoleumStoreNumber) && $flow->LinoleumStoreNumber != ''){{$flow->LinoleumStoreNumber}}@endif" type="number" max="9999" class="form-control" id="LinoleumStoreNumber" placeholder="" maxlength="4">

										</td>
										<td>
										   <label id="LinoleumSKULabel" for="LinoleumSKU" class="sr-only">Linoleum SKU</label>
										   <input name="LinoleumSKU" value="@if(isset($flow->LinoleumSKU)){{$flow->LinoleumSKU}}@endif" type="number" max="999999999" class="form-control" id="LinoleumSKU" placeholder="" maxlength="10">

										</td>
										<td>  </td>
										<td>
										   <label id="LinoleumLaborLabel" for="LinoleumLabor" class="sr-only">Linoleum Labor</label>
										   <input name="LinoleumLabor" value="@if(isset($flow->LinoleumLabor) && $flow->LinoleumLabor != ''){{$flow->LinoleumLabor}}@endif" type="number" max="9999999999999.9999" class="form-control" id="LinoleumLabor" placeholder="" >

										</td>
										<td>
										   <span class="input-group">
											  <label id="LinoleumWasteFactorLabel" for="LinoleumWasteFactor" class="sr-only">Linoleum Waste Factor</label>
											  <input name="LinoleumWasteFactor" value="@if(isset($flow->LinoleumWasteFactor) && $flow->LinoleumWasteFactor != ''){{$flow->LinoleumWasteFactor}}@endif" type="number" max="999999999" min="0" max="999.99" step="0.01" class="form-control" id="LinoleumWasteFactor" placeholder="" >

											  <span class="input-group-addon">%</span>
										   </span>
										</td>
										<td>
										   <label id="LinoleumRemovalCostLabel" for="LinoleumRemovalCost" class="sr-only">Linoleum Removal</label>
										   <input name="LinoleumRemovalCost" value="@if(isset($flow->LinoleumRemovalCost) && $flow->LinoleumRemovalCost != ''){{$flow->LinoleumRemovalCost}}@endif" type="number" max="9999999999999.9999" class="form-control" id="LinoleumRemovalCost" placeholder="" >

										</td>
									 </tr>
									 <tr>
										<td>Tile Per SqFt : </td>
										<td>
										   <label id="TileMaterialLabel" for="TileMaterial" class="sr-only">Tile Material</label>
										   <input name="TileMaterial" value="@if(isset($flow->TileMaterial) && $flow->TileMaterial != ''){{$flow->TileMaterial}}@endif" type="number" max="9999999999999.9999" class="form-control" id="TileMaterial" placeholder="" >

										</td>
										<td>
										   <label id="TileStoreNumberLabel" for="TileStoreNumber" class="sr-only">Tile Store Number</label>
										   <input name="TileStoreNumber" value="@if(isset($flow->TileStoreNumber) && $flow->TileStoreNumber != ''){{$flow->TileStoreNumber}}@endif" type="number" max="9999" class="form-control" id="TileStoreNumber" placeholder="" maxlength="4">

										</td>
										<td>
										   <label id="TileMaterialSKULabel" for="TileMaterialSKU" class="sr-only">Tile SKU</label>
										   <input name="TileMaterialSKU" value="@if(isset($flow->TileMaterialSKU)){{$flow->TileMaterialSKU}}@endif" type="number" max="999999999" class="form-control" id="TileMaterialSKU" placeholder="" maxlength="10">

										</td>
										<td>
										   <label id="TileSqFtPerSKULabel" for="TileSqFtPerSKU" class="sr-only">Tile Coverage</label>
										   <input name="TileSqFtPerSKU" value="@if(isset($flow->TileSqFtPerSKU) && $flow->TileSqFtPerSKU != ''){{$flow->TileSqFtPerSKU}}@endif" type="number" max="9999.99" class="form-control" id="TileSqFtPerSKU" placeholder="" >

										</td>
										<td>
										   <label id="TileLaborLabel" for="TileLabor" class="sr-only">Tile Labor</label>
										   <input name="TileLabor" value="@if(isset($flow->TileLabor) && $flow->TileLabor != ''){{$flow->TileLabor}}@endif" type="number" max="9999999999999.9999" class="form-control" id="TileLabor" placeholder="" >

										</td>
										<td>
										   <span class="input-group">
											  <label id="TileWasteFactorLabel" for="TileWasteFactor" class="sr-only">Tile Waste Factor</label>
											  <input name="TileWasteFactor" value="@if(isset($flow->TileWasteFactor) && $flow->TileWasteFactor != ''){{$flow->TileWasteFactor}}@endif" type="number" max="999999999" min="0" max="999.99" step="0.01" class="form-control" id="TileWasteFactor" placeholder="">

											  <span class="input-group-addon">%</span>
										   </span>
										</td>
										<td>
										   <label id="TileRemovalCostLabel" for="TileRemovalCost" class="sr-only">Tile Removal</label>
										   <input name="TileRemovalCost" value="@if(isset($flow->TileRemovalCost) && $flow->TileRemovalCost != ''){{$flow->TileRemovalCost}}@endif" type="number" max="9999999999999.9999" class="form-control" id="TileRemovalCost" placeholder="" >

										</td>
									 </tr>
									 <tr>
										<td>Tile Backer Per SqFt : </td>
										<td>
										   <label id="TileBackerMaterialLabel" for="TileBackerMaterial" class="sr-only">Tile Backer Material</label>
										   <input name="TileBackerMaterial" value="@if(isset($flow->TileBackerMaterial) && $flow->TileBackerMaterial != ''){{$flow->TileBackerMaterial}}@endif" type="number" max="9999999999999.9999" class="form-control" id="TileBackerMaterial" placeholder="" >

										</td>
										<td>
										   <label id="TileBackStoreNumberLabel" for="TileBackStoreNumber" class="sr-only">Tile Backer Store Number</label>
										   <input name="TileBackStoreNumber" value="@if(isset($flow->TileBackStoreNumber) && $flow->TileBackStoreNumber != ''){{$flow->TileBackStoreNumber}}@endif" type="number" max="9999" class="form-control" id="TileBackStoreNumber" placeholder="" maxlength="4">

										</td>
										<td>
										   <label id="TileBackerSKULabel" for="TileBackerSKU" class="sr-only">Tile Backer SKU</label>
										   <input name="TileBackerSKU" value="@if(isset($flow->TileBackerSKU)){{$flow->TileBackerSKU}}@endif" type="number" max="999999999" class="form-control" id="TileBackerSKU" placeholder="" maxlength="10">

										</td>
										<td>
										   <label id="TileBackerSqFtPerSKULabel" for="TileBackerSqFtPerSKU" class="sr-only">Tile Backer Coverage</label>
										   <input name="TileBackerSqFtPerSKU" value="@if(isset($flow->TileBackerSqFtPerSKU) && $flow->TileBackerSqFtPerSKU != ''){{$flow->TileBackerSqFtPerSKU}}@endif" type="number" max="9999.99" class="form-control" id="TileBackerSqFtPerSKU" placeholder="" >

										</td>
										<td>
										   <label id="TileBackerLaborLabel" for="TileBackerLabor" class="sr-only">Tile Backer Labor</label>
										   <input name="TileBackerLabor" value="@if(isset($flow->TileBackerLabor) && $flow->TileBackerLabor != ''){{$flow->TileBackerLabor}}@endif" type="number" max="9999999999999.9999" class="form-control" id="TileBackerLabor" placeholder="" >

										</td>
										<td>
										   <span class="input-group">
											  <label id="TileBackerWasteFactorLabel" for="TileBackerWasteFactor" class="sr-only">Tile Backer Waste Factor</label>
											  <input name="TileBackerWasteFactor" value="@if(isset($flow->TileBackerWasteFactor) && $flow->TileBackerWasteFactor != ''){{$flow->TileBackerWasteFactor}}@endif" type="number" max="999999999" min="0" max="999.99" step="0.01" class="form-control" id="TileBackerWasteFactor" placeholder="" >

											  <span class="input-group-addon">%</span>
										   </span>
										</td>
										<td>   </td>
									 </tr>
									 <tr>
										<td>Laminate Per SqFt : </td>
										<td>
										   <label id="AllureMaterialLabel" for="AllureMaterial" class="sr-only">Laminate Material</label>
										   <input name="AllureMaterial" value="@if(isset($flow->AllureMaterial) && $flow->AllureMaterial != ''){{$flow->AllureMaterial}}@endif" type="number" max="9999999999999.9999" class="form-control" id="AllureMaterial" placeholder="">

										</td>
										<td>
										   <label id="AllureStoreNumberLabel" for="AllureStoreNumber" class="sr-only">Laminate Store Number</label>
										   <input name="AllureStoreNumber" value="@if(isset($flow->AllureStoreNumber) && $flow->AllureStoreNumber != ''){{$flow->AllureStoreNumber}}@endif" type="number" max="9999" class="form-control " id="AllureStoreNumber" placeholder="" maxlength="4">

										</td>
										<td>
										   <label id="AllureSKULabel" for="AllureSKU" class="sr-only">Laminate SKU</label>
										   <input name="AllureSKU" value="@if(isset($flow->AllureSKU) && $flow->AllureSKU != ''){{$flow->AllureSKU}}@endif" type="number" max="999999999" class="form-control" id="AllureSKU" placeholder="" maxlength="10">

										</td>
										<td>
										   <label id="AllureSqFtPerSKULabel" for="AllureSqFtPerSKU" class="sr-only">Laminate Coverage</label>
										   <input name="AllureSqFtPerSKU" value="@if(isset($flow->AllureSqFtPerSKU) && $flow->AllureSqFtPerSKU != ''){{$flow->AllureSqFtPerSKU}}@endif" type="number" max="9999.99" class="form-control" id="AllureSqFtPerSKU" placeholder="" >

										</td>
										<td>
										   <label id="AllureLaborLabel" for="AllureLabor" class="sr-only">Laminate Labor</label>
										   <input name="AllureLabor" value="@if(isset($flow->AllureLabor) && $flow->AllureLabor != ''){{$flow->AllureLabor}}@endif" type="number" max="9999999999999.9999" class="form-control" id="AllureLabor" placeholder="" >

										</td>
										<td>
										   <span class="input-group">
											  <label id="AllureWasteFactorLabel" for="AllureWasteFactor" class="sr-only">Laminate Waste Factor</label>
											  <input name="AllureWasteFactor" value="@if(isset($flow->AllureWasteFactor) && $flow->AllureWasteFactor != ''){{$flow->AllureWasteFactor}}@endif" type="number" max="999999999" min="0" max="999.99" step="0.01" class="form-control" id="AllureWasteFactor" placeholder="" >

											  <span class="input-group-addon">%</span>
										   </span>
										</td>
										<td>
										   <label id="AllureRemovalCostLabel" for="AllureRemovalCost" class="sr-only">Laminate Removal</label>
										   <input name="AllureRemovalCost" value="@if(isset($flow->AllureRemovalCost) && $flow->AllureRemovalCost != ''){{$flow->AllureRemovalCost}}@endif" type="number" max="9999999999999.9999" class="form-control" id="AllureRemovalCost" placeholder="" >

										</td>
									 </tr>
									 <tr>
										<td>Hardwood Per SqFt : </td>
										<td>
										   <label id="HardwoodMaterialLabel" for="HardwoodMaterial" class="sr-only">Hardwood Material</label>
										   <input name="HardwoodMaterial" value="@if(isset($flow->HardwoodMaterial) && $flow->HardwoodMaterial != ''){{$flow->HardwoodMaterial}}@endif" type="number" max="9999999999999.9999" class="form-control" id="HardwoodMaterial" placeholder="" >

										</td>
										<td>
										   <label id="HardwoodStoreNumberLabel" for="HardwoodStoreNumber" class="sr-only">Hardwood Store Number</label>
										   <input name="HardwoodStoreNumber" value="@if(isset($flow->HardwoodStoreNumber) && $flow->HardwoodStoreNumber != ''){{$flow->HardwoodStoreNumber}}@endif" type="number" max="9999" class="form-control" id="HardwoodStoreNumber" placeholder="" maxlength="4">

										</td>
										<td>
										   <label id="HardwoodSKULabel" for="HardwoodSKU" class="sr-only">Hardwood SKU</label>
										   <input name="HardwoodSKU" value="@if(isset($flow->HardwoodSKU)){{$flow->HardwoodSKU}}@endif" type="number" max="999999999" class="form-control" id="HardwoodSKU" placeholder="" maxlength="10">

										</td>
										<td>
										   <label id="HardwoodSqFtPerSKULabel" for="HardwoodSqFtPerSKU" class="sr-only">Hardwood Coverage</label>
										   <input name="HardwoodSqFtPerSKU" value="@if(isset($flow->HardwoodSqFtPerSKU) && $flow->HardwoodSqFtPerSKU != ''){{$flow->HardwoodSqFtPerSKU}}@endif" type="number" max="9999.99" class="form-control" id="HardwoodSqFtPerSKU" placeholder="" >

										</td>
										<td>
										   <label id="HardwoodLaborLabel" for="HardwoodLabor" class="sr-only">Hardwood Labor</label>
										   <input name="HardwoodLabor" value="@if(isset($flow->HardwoodLabor) && $flow->HardwoodLabor != ''){{$flow->HardwoodLabor}}@endif" type="number" max="9999999999999.9999" class="form-control" id="HardwoodLabor" placeholder="" >

										</td>
										<td>
										   <span class="input-group">
											  <label id="HardwoodWasteFactorLabel" for="HardwoodWasteFactor" class="sr-only">Hardwood Waste Factor</label>
											  <input name="HardwoodWasteFactor" value="@if(isset($flow->HardwoodWasteFactor) && $flow->HardwoodWasteFactor != ''){{$flow->HardwoodWasteFactor}}@endif" type="number" max="999999999" min="0" max="999.99" step="0.01" class="form-control" id="HardwoodWasteFactor" placeholder="" >

											  <span class="input-group-addon">%</span>
										   </span>
										</td>
										<td>
										   <label id="HardwoodRemovalCostLabel" for="HardwoodRemovalCost" class="sr-only">Hardwood Removal</label>
										   <input name="HardwoodRemovalCost" value="@if(isset($flow->HardwoodRemovalCost) && $flow->HardwoodRemovalCost != ''){{$flow->HardwoodRemovalCost}}@endif" type="number" max="9999999999999.9999" class="form-control" id="HardwoodRemovalCost" placeholder="" >

										</td>
									 </tr>
								  </tbody>
							   </table>
							</div>


							<div class="table-responsiv">
							   <table class="table flowAddGrid">
								  <thead>
									 <tr>
										<th class="col-md-2"> </th>
										<th class="col-md-1">Material </th>
										<th class="col-md-1">Store Number </th>
										<th class="col-md-1">SKU </th>
										<th class="col-md-1">Coverage Per SKU </th>
										<th class="col-md-1">Labor </th>
										<th class="col-md-2">Paint Color   </th>
									 </tr>
								  </thead>
								  <tbody>
									 <tr>
										<td>Wall Paint (5 Gallon) SKU : </td>
										<td>
										   <label id="WallPaintFiveGallonMaterialLabel" for="WallPaintFiveGallonMaterial" class="sr-only">Wall Paint Material</label>
										   <input name="WallPaintFiveGallonMaterial" value="@if(isset($flow->WallPaintFiveGallonMaterial) && $flow->WallPaintFiveGallonMaterial != ''){{$flow->WallPaintFiveGallonMaterial}}@endif" type="number" max="9999999999999.9999" class="form-control" id="WallPaintFiveGallonMaterial" placeholder="" >

										</td>
										<td>
										   <label id="WallPaintFiveGallonStoreNumberLabel" for="WallPaintFiveGallonStoreNumber" class="sr-only">Wall Paint Store Number</label>
										   <input name="WallPaintFiveGallonStoreNumber" value="@if(isset($flow->WallPaintFiveGallonStoreNumber) && $flow->WallPaintFiveGallonStoreNumber != ''){{$flow->WallPaintFiveGallonStoreNumber}}@endif" type="number" max="9999" class="form-control" id="WallPaintFiveGallonStoreNumber" placeholder="" maxlength="4">

										</td>
										<td>
										   <label id="WallPaintFiveGallonSKULabel" for="WallPaintFiveGallonSKU" class="sr-only">Wall Paint SKU</label>
										   <input name="WallPaintFiveGallonSKU" value="@if(isset($flow->WallPaintFiveGallonSKU)){{$flow->WallPaintFiveGallonSKU}}@endif" type="number" max="999999999" class="form-control" id="WallPaintFiveGallonSKU" placeholder="" maxlength="10">

										</td>
										<td>
										   <label id="WallPaintFiveGallonSqFtPerSKULabel" for="WallPaintFiveGallonSqFtPerSKU" class="sr-only">Wall Paint Coverage</label>
										   <input name="WallPaintFiveGallonSqFtPerSKU" value="@if(isset($flow->WallPaintFiveGallonSqFtPerSKU) && $flow->WallPaintFiveGallonSqFtPerSKU != ''){{$flow->WallPaintFiveGallonSqFtPerSKU}}@endif" type="number" max="9999.99" class="form-control" id="WallPaintFiveGallonSqFtPerSKU" placeholder="" >

										</td>
										<td>
										   <label id="WallPaintFiveGallonLaborLabel" for="WallPaintFiveGallonLabor" class="sr-only">Wall Paint Labor</label>
										   <input name="WallPaintFiveGallonLabor" value="@if(isset($flow->WallPaintFiveGallonLabor) && $flow->WallPaintFiveGallonLabor != ''){{$flow->WallPaintFiveGallonLabor}}@endif" type="number" max="9999999999999.9999" class="form-control" id="WallPaintFiveGallonLabor" placeholder="" >

										</td>
										<td>
										   <label id="WallPaintFiveGallonColorLabel" for="WallPaintFiveGallonColor" class="sr-only">Wall Paint Color</label>
										   <input name="WallPaintFiveGallonColor" value="@if(isset($flow->WallPaintFiveGallonColor)){{$flow->WallPaintFiveGallonColor}}@endif" type="text" class="form-control" id="WallPaintFiveGallonColor" placeholder="" maxlength="60">

										</td>
									 </tr>
									 <tr>
										<td>Ceiling Paint (5 Gallon) SKU : </td>
										<td>
										   <label id="CeilingPaintFiveGallonMaterialLabel" for="CeilingPaintFiveGallonMaterial" class="sr-only">Ceiline Paint Material</label>
										   <input name="CeilingPaintFiveGallonMaterial" value="@if(isset($flow->CeilingPaintFiveGallonMaterial) && $flow->CeilingPaintFiveGallonMaterial != ''){{$flow->CeilingPaintFiveGallonMaterial}}@endif" type="number" max="9999999999999.9999" class="form-control" id="CeilingPaintFiveGallonMaterial" placeholder="" >

										</td>
										<td>
										   <label id="CeilingPaintFiveGallonStoreNumberLabel" for="CeilingPaintFiveGallonStoreNumber" class="sr-only">Ceiling Paint Store Number</label>
										   <input name="CeilingPaintFiveGallonStoreNumber" value="@if(isset($flow->CeilingPaintFiveGallonStoreNumber) && $flow->CeilingPaintFiveGallonStoreNumber != ''){{$flow->CeilingPaintFiveGallonStoreNumber}}@endif" type="number" max="9999" class="form-control" id="CeilingPaintFiveGallonStoreNumber" placeholder="" maxlength="4">

										</td>
										<td>
										   <label id="CeilingPaintFiveGallonSKULabel" for="CeilingPaintFiveGallonSKU" class="sr-only">Ceiling Paint SKU</label>
										   <input name="CeilingPaintFiveGallonSKU" value="@if(isset($flow->CeilingPaintFiveGallonSKU)){{$flow->CeilingPaintFiveGallonSKU}}@endif" type="number" max="999999999" class="form-control" id="CeilingPaintFiveGallonSKU" placeholder="" maxlength="10">

										</td>
										<td>
										   <label id="CeilingPaintFiveGallonSqFtSKULabel" for="CeilingPaintFiveGallonSqFtSKU" class="sr-only">Ceiling Paint Coverage</label>
										   <input name="CeilingPaintFiveGallonSqFtSKU" value="@if(isset($flow->CeilingPaintFiveGallonSqFtSKU) && $flow->CeilingPaintFiveGallonSqFtSKU != ''){{$flow->CeilingPaintFiveGallonSqFtSKU}}@endif" type="number" max="999999999" class="form-control" id="CeilingPaintFiveGallonSqFtSKU" placeholder="" >

										</td>
										<td>  </td>
										<td>
										   <label id="CeilingPaintFiveGallonColorLabel" for="CeilingPaintFiveGallonColor" class="sr-only">Ceiling Paint Color</label>
										   <input name="CeilingPaintFiveGallonColor" value="@if(isset($flow->CeilingPaintFiveGallonColor)){{$flow->CeilingPaintFiveGallonColor}}@endif" type="text" class="form-control" id="CeilingPaintFiveGallonColor" placeholder="" maxlength="60">

										</td>
									 </tr>
									 <tr>
										<td>Trim Paint (1 Gallon) SKU : </td>
										<td>
										   <label id="TrimPaintOneGallonMaterialLabel" for="TrimPaintOneGallonMaterial" class="sr-only">Trim Paint Material</label>
										   <input name="TrimPaintOneGallonMaterial" value="@if(isset($flow->TrimPaintOneGallonMaterial) && $flow->TrimPaintOneGallonMaterial != ''){{$flow->TrimPaintOneGallonMaterial}}@endif" type="number" max="9999999999999.9999" class="form-control" id="TrimPaintOneGallonMaterial" placeholder="">

										</td>
										<td>
										   <label id="TrimPaintOneGallonStoreNumberLabel" for="TrimPaintOneGallonStoreNumber" class="sr-only">Trim Paint Store Number</label>
										   <input name="TrimPaintOneGallonStoreNumber" value="@if(isset($flow->TrimPaintOneGallonStoreNumber) && $flow->TrimPaintOneGallonStoreNumber != ''){{$flow->TrimPaintOneGallonStoreNumber}}@endif" type="number" max="9999" class="form-control" id="TrimPaintOneGallonStoreNumber" placeholder="" maxlength="4">

										</td>
										<td>
										   <label id="TrimPaintOneGallonSKULabel" for="TrimPaintOneGallonSKU" class="sr-only">Trim Paint SKU</label>
										   <input name="TrimPaintOneGallonSKU" value="@if(isset($flow->TrimPaintOneGallonSKU)){{$flow->TrimPaintOneGallonSKU}}@endif" type="number" max="999999999" class="form-control" id="TrimPaintOneGallonSKU" placeholder="" maxlength="10">

										</td>
										<td>
										   <label id="TrimPaintOneGallonSqFtSKULabel" for="TrimPaintOneGallonSqFtSKU" class="sr-only">Trim Paint Coverage</label>
										   <input name="TrimPaintOneGallonSqFtSKU" value="@if(isset($flow->TrimPaintOneGallonSqFtSKU) && $flow->TrimPaintOneGallonSqFtSKU != ''){{$flow->TrimPaintOneGallonSqFtSKU}}@endif" type="number" max="999999999" class="form-control" id="TrimPaintOneGallonSqFtSKU" placeholder="" >

										</td>
										<td>  </td>
										<td>
										   <label id="TrimPaintOneGallonColorLabel" for="TrimPaintOneGallonColor" class="sr-only">Trim Paint Color</label>
										   <input name="TrimPaintOneGallonColor" value="@if(isset($flow->TrimPaintOneGallonColor)){{$flow->TrimPaintOneGallonColor}}@endif" type="text" class="form-control" id="TrimPaintOneGallonColor" placeholder="" maxlength="60">

										</td>
									 </tr>
								  </tbody>
							   </table>
							</div>


                            <div class="row">
                                <div class="col-md-6 ">
                                    <h3>
                                        Groups
                                    </h3>
									<div class="form-inline w-100">
                                        <div class="">
                                            <input style="min-width:60%" type="text" placeholder="SEARCH GROUP" id="searchItem" class="form-control">
                                            <button id="search" type="button" class="btn btn-warning">
                                                SEARCH
                                            </button>
											<button id="filterClearBtn" type="button" class="btn btn-danger">
                                                CLEAR
                                            </button>
                                        </div>
                                    </div>
                                    <ol class="drop_targets vertical drag-holder py-3">
                                        @foreach($groups as $item)

                                            <li itemid="{{ $item->id }}" class="dragging">
                                                <b>{{ $item->name }}</b>
                                            </li>
                                        @endforeach
                                    </ol>
                                </div>
                                <div class="col-md-6 ">
                                    <h3>
                                        Flow Groups
                                    </h3>
                                    <p>
                                        Drag and Drop Groups

                                    </p>
                                    <ol class="drop_targets vertical drop-holder py-3">
                                        @if($edit && $dataTypeContent->items)
                                            @foreach($dataTypeContent->items as $item)
												@if(isset($item->group->name))
                                                <li itemid="{{ $item->group->id }}" class="dragging">
                                                    <b>{{ $item->group->name }}</b>
                                                </li>
												@endif
                                            @endforeach
                                        @endif
                                    </ol>
                                </div>
                            </div>
                            <input type="hidden" name="items" id="items"
                                   value="{{ $edit?implode(',',$dataTypeContent->items()->pluck('group_id')->toArray()):'' }}">
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" id="ck_before"
                                        class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }}?
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <script src=""></script>
    <!-- End Delete File Modal -->
    <style>


        .dragged {
            position: absolute;
            opacity: 0.5;
            z-index: 2000;
        }

        ol.drop_targets li.placeholder {
            position: relative;
            /** More li styles **/
            padding: 15px;

        }

        ol.drop_targets li.placeholder:before {
            position: absolute;
            /** Define arrowhead **/
        }
    </style>

@stop
@section('javascript')
	<script>
        $('#searchItem').focus(function(){
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    searchItems();
                    return false;
                }
            });
        })

        $('#search').click(function(){
            searchItems();
        })
		$('#filterClearBtn').click(function(){
			$('#searchItem').val('');
            $.ajax({
                url:"{{ route('group.lists') }}",
                data:{
                    search:'',
					items:$('#items').val(),
                },
                success:function(data){
                    $('.drag-holder').empty();
                    var listItems = '';
                    $.each(data.data,function(id,name){
                        listItems+='<li itemid="'+id+'" class="dragging"><b>'+name+'</b></li>';
                    })
                    $('.drag-holder').html(listItems);

                },
                error:function(data)
                {
                    toastr.error(data.statusText+':'+data.responseText)
                }

            })
        })
        function searchItems(){
            $.ajax({
                url:"{{ route('group.lists') }}",
                data:{
                    search:$('#searchItem').val(),
                    items:$('#items').val(),
                },
                success:function(data){
                    $('.drag-holder').empty();
                    var listItems = '';
                    $.each(data.data,function(id,name){
                        listItems+='<li itemid="'+id+'" class="dragging"><b>'+name+'</b></li>';
                    })
                    $('.drag-holder').html(listItems);

                },
                error:function(data)
                {
                    toastr.error(data.statusText+':'+data.responseText)
                }

            })
        }
    </script>
    <script src="https://johnny.github.io/jquery-sortable/js/jquery-sortable.js"></script>
    <script>
        var group = $("ol.drop_targets").sortable({
            group: 'drop_targets',

            onDrop: function ($item, container, _super) {
                $('#serialize_output').text(
                    group.sortable("serialize").get().join("\n"));
                _super($item, container);
                dropped();

            },
            serialize: function (parent, children, isContainer) {
                return isContainer ? children.join() : parent.text();
            },
            // tolerance: 6,
            // distance: 10
        });

        function dropped() {
            var result = [];
            $('.drop-holder').children('li').each(function () {
                result.push($(this).attr('itemid'))
            })
            $('#items').val(result.join())
        }
    </script>
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
            return function () {
                $file = $(this).siblings(tag);

                params = {
                    slug: '{{ $dataType->slug }}',
                    filename: $file.data('file-name'),
                    id: $file.data('id'),
                    field: $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                }

                //$('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: ['YYYY-MM-DD']
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function (i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function () {
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if (response
                        && response.data
                        && response.data.status
                        && response.data.status == 200) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function () {
                            $(this).remove();
                        })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
			// $('#ck_before').on('click', function () {
				// if($('input[name="name"]').val() == ''){
					// toastr.error("Please enter name.");
					// event.preventDefault();
                    // return false;
				// }else if($('input[name="description"]').val() == ''){
					// toastr.error("Please enter description.");
					// event.preventDefault();
                    // return false;
				// }else if($('#items').val() == ''){
					// toastr.error("Please choose group.");
					// event.preventDefault();
                    // return false;
				// }else $('.form-edit-add').submit();
			// });
        });
    </script>
@stop
