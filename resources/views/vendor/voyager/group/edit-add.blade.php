@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif

                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                            <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? null;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}"
                                            style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif

                                <div
                                    class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label {{ ($row->required ||
(isset($row->details) && isset($row->details->required)))?"label-require":"" }}"
                                           for="name">{{ $row->getTranslatedAttribute('display_name') }}
                                        @if($row->details && property_exists($row->details,'currency'))
                                            ({{ env('CURRENCY_PREFIX',env('CURRENCY_SUFFIX','$')) }})
                                        @endif
                                    </label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach

                                </div>
                            @endforeach


                            <div class="row">
                                <div class="col-md-6 ">
                                    <h3>
                                        Items
                                    </h3>
                                    <div class="form-inline w-100">
                                        <div class="form-group w-100">
                                            <input type="text" placeholder="SEARCH ITEMS" id="searchItem"
                                                   class="form-control">
                                            <input type="hidden" id="searchType"
                                                   @if($edit && $dataTypeContent->group_type_id == \App\Models\GroupType::PROPERTY_INFO)
                                                   value="{{ \App\Models\ItemTypesModel::BASIC_COMMENT }}"
                                                @endif>
                                            <button id="search" type="button" cl
                                                    ass="btn btn-warning">
                                                SEARCH
                                            </button>
                                        </div>
                                    </div>
                                    <ol class="drop_targets vertical drag-holder py-3">
                                        @foreach($items as $item)

                                            <li itemid="{{ $item->id }}" class="drag-item">
                                                <b>{{ $item->name }}</b>
                                            </li>
                                        @endforeach
                                    </ol>
                                </div>
                                <div class="col-md-6 ">
                                    <h3>
                                        Group Items
                                    </h3>
                                    <p>
                                        Drag and Drop Items

                                    </p>
                                    <ol class="drop_targets vertical drop-holder py-3">
                                        @if($edit)
                                            @foreach($dataTypeContent->items as $item)
                                                <li itemid="{{ $item->item->id }}" class="drag-item">
                                                    <b>{{ $item->item->name }}</b>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ol>
                                </div>
                            </div>
                            <input type="hidden" name="items" id="items"
                                   value="{{ $edit?implode(',',$dataTypeContent->items()->pluck('item_id')->toArray()):'' }}">
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit"
                                        class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }}?
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <script src=""></script>
    <!-- End Delete File Modal -->
    <style>


        .dragged {
            position: absolute;
            opacity: 0.5;
            z-index: 2000;
        }

        ol.drop_targets li.placeholder {
            position: relative;
            /** More li styles **/
            padding: 15px;

        }

        ol.drop_targets li.placeholder:before {
            position: absolute;
            /** Define arrowhead **/
        }
    </style>
@stop


@section(/** @lang text */'javascript')
    <script src="https://johnny.github.io/jquery-sortable/js/jquery-sortable.js"></script>

    <script>
        @if(old('group_type_id') || $edit)
        {{--$('#group_type_id > select').val({{old('group_type_id')}}); // Change the value or make some change to the internal state--}}
        {{--$('#group_type_id > select').trigger('change.select2'); // Notify only Select2 of changes--}}
        @if($edit)
            changeGroupType({{$dataTypeContent->group_type_id}},false)
        @else
            changeGroupType({{old('group_type_id')}},false)
        @endif
        @endif
        $('#group_type_id > select').change(function () {
            changeGroupType($('#group_type_id > select').val())
        })


        $('#searchItem').focus(function () {
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    searchItems();
                    return false;
                }
            });
        })

        $('#search').click(function () {
            searchItems();
        })

        function searchItems() {
            $.ajax({
                url: "{{ route('items.lists') }}",
                data: {
                    search: $('#searchItem').val(),
                    type: $('#searchType').val(),
                    except: $('#items').val()
                },
                success: function (data) {
                    $('.drag-holder').empty();
                    var listItems = '';
                    $.each(data.data, function (id, name) {
                        listItems += '<li itemid="' + id + '" class="drag-item"><b>' + name + '</b></li>';
                    })
                    $('.drag-holder').html(listItems);

                },
                error: function (data) {
                    toastr.error(data.statusText + ':' + data.responseText)
                }

            })
        }

        var group = $("ol.drop_targets").sortable({
            group: 'drop_targets',

            onDrop: function ($item, container, _super) {
                $('#serialize_output').text(
                    group.sortable("serialize").get().join("\n"));
                _super($item, container);
                dropped();

            },
            serialize: function (parent, children, isContainer) {
                return isContainer ? children.join() : parent.text();
            },
            // tolerance: 6,
            // distance: 10
        });

        function dropped() {
            updateSelectedItem();

        }

        function updateSelectedItem() {
            var result = [];
            $('.drop-holder').children('li').each(function () {
                result.push($(this).attr('itemid'))
            })
            $('#items').val(result.join())
        }

        function changeGroupType(groupTypeId, updateDropZone = true) {
            // $('#group_type_id > select').val()
            if (groupTypeId == '{{ \App\Models\GroupType::PROPERTY_INFO }}') {

                $('#floor').hide();

                $('#floor > .toggle > input[name="flooring"]').val('');

                $('#paint').hide();
                $('#paint > .toggle > input[name="paint"]').val('');
                $('#comment').hide();
                $('#comment > .toggle > input[name="comment"]').val('');
                $('#photo_group').hide();
                $('#photo_group > .toggle > input[name="photo_group"]').val('');
                $('#gl_code').hide();
                $('#gl_code >  input[name="gl_code"]').val('');


                $('#searchType').val('{{ \App\Models\ItemTypesModel::BASIC_COMMENT }}')

            } else {
                $('#floor').show();
                $('#gl_code').show();
                $('#paint').show();
                $('#comment').show();
                $('#photo_group').show();

                $('#searchType').val('')

            }
            updateSelectedItem()
            searchItems();
            if (updateDropZone)
            {
                $('.drop-holder').empty()

            }
        }
    </script>
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
            return function () {
                $file = $(this).siblings(tag);

                params = {
                    slug: '{{ $dataType->slug }}',
                    filename: $file.data('file-name'),
                    id: $file.data('id'),
                    field: $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                }

                //$('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: ['YYYY-MM-DD']
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function (i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function () {
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if (response
                        && response.data
                        && response.data.status
                        && response.data.status == 200) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function () {
                            $(this).remove();
                        })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
