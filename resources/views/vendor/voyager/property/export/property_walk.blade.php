<style>
    td {
        width:30;
        wrap-text:true;
        vertical-align: middle;
        padding: 5px;
        margin: 5px;
    }
</style>
<table>
    <tr></tr>
    <tr>
        <td colspan="3" align="center">
            <b style="font-size: 20px">{{ $property->project_name }}</b>
        </td>
    </tr>
    <tr>
        <td style="word-wrap:break-word;wrap-text: true;width: 20">
            <b>Project Name:</b>
        </td>
        <td style="word-wrap:break-word;wrap-text: true;width: 30">
            {{ $property->project_name }}
        </td>
    </tr>
    <tr>
        <td>
            <b>Address:</b>
        </td>
        <td>
            {{ $property->full_address }}
        </td>
    </tr>
    <tr>
        <td>
            <b>Date Created:</b>
        </td>
        <td>
            {{ convertSystemTzToUserTz($property->created_at) }}
        </td>
    </tr>
    <tr>
        <td>
            <b>Main SQFt:</b>
        </td>
        <td>
            {{ $property->main_square_ft?$property->main_square_ft:0 }} sf
        </td>
    </tr>
    <tr>
        <td>
            <b>Measured SQFT:</b>
        </td>
        <td>
            {{ $summary['measuredSQFT'] }} sf
        </td>
    </tr>
    <tr>
        <td>
            <b>Bed/Bath:</b>
        </td>
        <td >
            {{ $property->number_bedrooms?$property->number_bedrooms:0 }}
            bd,
            {{ $property->number_bathrooms?$property->number_bathrooms:0 }}
            ba
        </td>
    </tr>

    <tr>
        <td>
            <b>Year Built:</b>
        </td>
        <td align="left">
            {{ $property->year_build?$property->year_build:0 }}
        </td>
    </tr>
    <tr>
        <td>
            <b>Budget total</b>
        </td>
        <td>
            {{ formatCurrency( $summary['totalPrice'] ) }}
        </td>
    </tr>
</table>
<table class="table table-bordered">
    <thead>
    <tr>
        <th class="th-center" colspan="6">

        </th>
    </tr>
    </thead>
    <tbody>

    @php
        $sumPrice = 0;
    @endphp
    @foreach($result as $item)
        <tr>
            <td colspan="6" align="center">
                <b>{{ $item['group']['name'] }}
                    {{ $item['group']['measure']?$item['group']['measure'].' ('.$item['group']['measure'].' sqft)':'' }}</b>
            </td>
        </tr>

        <tr>
            <td align="center" style="font-weight: bold">SKU</td>
            <td align="center" style="font-weight: bold;word-wrap:break-word;wrap-text: true">Product Name</td>
            <td align="center" style="font-weight: bold;word-wrap:break-word;wrap-text: true; width: 30">Description</td>
            <td align="center" style="font-weight: bold">Qty</td>
            <td align="center" style="font-weight: bold">Unit Price</td>
            <td align="center" style="font-weight: bold">Price</td>
        </tr>
        @foreach($item['products'] as $product)
            @php
                $sumPrice+=$price = ($product['labor']+$product['price'])* $product['qty'];

            @endphp
            <tr>
                <td align="center">{{ $product['SKU'] }}</td>
                <td>{{ $product['name'] }}</td>
                <td style="word-wrap:break-word">{{ $product['description'] }}</td>
                <td align="center">{{ $product['qty'] }}</td>
                <td align="center"> {{ formatCurrency($product['price']) }}</td>
                <td align="center">{{ formatCurrency($price) }}</td>
            </tr>
        @endforeach
        @foreach($item['customItems'] as $product)
            @php
                $sumPrice+=$price = $product['price'];

            @endphp
            <tr>
                <td align="center"></td>
                <td>{{ $product['item_name'] }}</td>
                <td style="word-wrap:break-word">{{ $product['description'] }}</td>
                <td align="center">1</td>
                <td align="center"> {{ formatCurrency($product['price']) }}</td>
                <td align="center">{{ formatCurrency($price) }}</td>
            </tr>
        @endforeach

        <tr></tr>
    @endforeach
    <tr>
        <td colspan="5" align="right">
            Scope of Work Sub-Total
        </td>
        <td>
            <b>{{ formatCurrency($sumPrice) }}</b>
        </td>
    </tr>
    </tbody>
</table>
