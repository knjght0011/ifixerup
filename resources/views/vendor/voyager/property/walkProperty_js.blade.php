<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
<script>
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',

        // These options are needed to round to whole numbers if that's what you want.
        minimumFractionDigits: 2, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)loaditem
    });
    @if($edit)
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if (e.target.getAttribute('aria-controls') === 'review') {
            $.ajax({
                url: "{{ route('property.quickReview') }}",
                data: {
                    propertyId: "{{ $dataTypeContent->id }}"
                },
                success: function (data) {
                    if (data.data === 'success') {
                        var result = '';
                        $.each(data.items, function (key, value) {

                            $.each(value.items, function (index, item) {
                                var images = '';
                                $.each(item.product_image, function (imageIndex, image) {
                                    if (image !== null)
                                        images += '<a target="_blank" href="{{ url('storage') }}/' + image + '">' +
                                            '<img width="35px" src="{{ url('storage') }}/' + image + '">' +
                                            '</a>';
                                })
                                approveClass = item.approved === 1 ? 'item_unapproved fas fa-check-circle' : 'item_approved fas  fa-times-circle';
                                var approve = '<span title="Approved" item-id="' + item.id + '" group_walk_id="' + value.group_walk + '"' +
                                    ' class=" approved_' + item.id + '  btn btn-xs ' + approveClass + ' delete" ></span> '
                                result += '<tr>' +
                                    '<td>' + (item.name===null?item.item_name:item.name) + ' (' + item.product_image.length + ' products)</td>' +
                                    '<td>' + value.group.name + '</td>' +
                                    '<td>' + (item.gl_code == null ? "" : item.gl_code) + '</td>' +
                                    '<td style="max-width: 200px">' + (item.description == null ? "" : item.description) + '</td>' +
                                    '<td align="center">' + formatter.format(item.price) + '</td>' +
                                    '<td align="center">' + approve + '</td>' +
                                    '<td align="center">' + images + '</td>' +
                                    '</tr>';
                            })
                        })
                        $('#table_review_body').html(result);
                        $('#table_review').DataTable();
                        $('#countApproved').html(data.summary.countApproved)
                        $('#countDenied').html(data.summary.countDenied)
                        $('#totalBudget').html(formatter.format(data.summary.totalBudget))
                        $('#approvedBudget').html(formatter.format(data.summary.approvedBudget))
                    }
                }
            })
        }
        if (e.target.getAttribute('aria-controls') === 'budget') {
            $.ajax({
                url: "{{ route('property.budget') }}",
                data: {
                    propertyId: "{{ $dataTypeContent->id }}"
                },
                success: function (data) {
                    if (data.data === 'success') {
                        var result = '';
                        $.each(data.items, function (key, value) {
                            result += '<table class="table "><thead><tr><th class="th-center" colspan="6">'
                                + value.group.name + (value.group.measure !== 0 ? " (" + value.group.measure + " sqft)" : "") +
                                '</th></tr></thead><tbody>' +
                                '<tr>' +
                                '<th>SKU</th>' +
                                '<th>Product Name</th>' +
                                '<th>Description</th>' +
                                '<th>Qty</th>' +
                                '<th>Unit Price</th>' +
                                '<th>Price</th>' +
                                '</tr>';
                            $.each(value.products, function (index, product) {
                                var desc = product.description ? product.description.slice(0, 55) + (product.description.length > 55 ? "..." : "") : "";
                                var price = (Number(product.labor) + Number(product.price)) * Number(product.qty);
                                result += '<tr>' +
                                    '<td>' + product.SKU + '</td>' +
                                    '<td>' + product.name + '</td>' +
                                    '<td>' + desc + '</td>' +
                                    '<td>' + product.qty + '</td>' +
                                    '<td>' + formatter.format(product.price) + '</td>' +
                                    '<td>' + formatter.format(price) + '</td>' +
                                    '</tr>';
                            })
                            if(value.customItems.length)
                            {
                                $.each(value.customItems, function (index, product) {
                                    var desc = product.description ? product.description.slice(0, 55) + (product.description.length > 55 ? "..." : "") : "";
                                    result += '<tr>' +
                                        '<td></td>' +
                                        '<td>' + product.item_name + '</td>' +
                                        '<td>' + desc + '</td>' +
                                        '<td>1</td>' +
                                        '<td>' + formatter.format(product.price) + '</td>' +
                                        '<td>' + formatter.format(product.price) + '</td>' +
                                        '</tr>';
                                })
                            }

                            result += '</tbody></table>'
                        })
                        result += '<p align="right">Scope of Work Sub-Total: <b class="h4">'
                            + formatter.format(data.summary.totalPrice) +
                            '</b></p>';
                        $('#total_budget').html('<b class="h4">'
                            + formatter.format(data.summary.totalPrice) +
                            '</b>')
                        $('#measuredSQFT').html(data.summary.measuredSQFT)
                        $('#productLists').html(result);
                    }
                }
            })
        }

    });
    @endif

    function addGroupToWalk(id, name) {
        $.ajax({
            url: "{{ route('property.addGroupToWalk') }}",
            type: "POST",
            dataType: "json",
            data: {
                'property_id': $('#property_id').val(), 'id': id, '_token': '{{ csrf_token()}}',
            },
            success: function (response) {
                if (response.data == 'success') {
                    var row =
                        "<div class='list-group'>" +
                        "<div data-id='" + response.group_walk_id + "' data-comment='" + response.comment + "' class='list-group-item clickable ng-scope'> " +
                        "<span class='group_item'> " +
                        "<span >" + name + "</span> " +
                        "<span class='glyphicon glyphicon-pencil edit pull-right clickable'></span> " +
                        "</span>" +
                        "</div>" +
                        "</div>"

                    $('.walkContainerSidebar .panel-default').append(row);
                } else toastr.error("Can not creat new walk.");
                // console.log('data:' + response.data);
            }

        })
    }

    function qtyChange(property_product_id) {
        var id = $('#group_items_walk_id').val();
        $.ajax({
            url: "{{ route('property.updateQty') }}",
            type: "POST",
            dataType: "json",
            data: {
                'id': property_product_id,
                'qty': $('#qty-' + property_product_id).val(),
                '_token': '{{ csrf_token()}}',
            },
            success: function (response) {
                if (response.data == 'success') {

                    $('#price_' + $('#item_id').val()).html(formatter.format(response.new_price));
                    $('#price-' + property_product_id).html(formatter.format(response.updatedPrice));
                    if (parseFloat(response.new_price) > 0)
                        $('#item_' + $('#item_id').val()).addClass('show_item');
                    else $('#item_' + $('#item_id').val()).removeClass('show_item');
                    toastr.success("Change quantity!");
                } else toastr.error("Error!");
                // console.log('data:' + response.new_price);
            }
        })
    }

    function laborChange(property_product_id) {
        $.ajax({
            url: "{{ route('property.updateLabor') }}",
            type: "POST",
            dataType: "json",
            data: {
                'id': property_product_id,
                'labor': $('#labor-' + property_product_id).val(),
                '_token': '{{ csrf_token()}}',
            },
            success: function (response) {
                if (response.data == 'success') {
                    toastr.success("Change labor!");
                    $('#price-' + property_product_id).html(formatter.format(response.updatedPrice));

                    // $('#price_' + $('#item_id').val()).html(formatter.format(response.new_price));

                } else toastr.error("Can not creat new walk.");
                // console.log('data:' + response.data);
            }
        })
    }

    function itemChange(id, key) {
        $.ajax({
            url: "{{ route('property.updateItem') }}",
            type: "POST",
            dataType: "json",
            data: {
                'id': id, 'key': key, 'value': $('.' + key + '_' + id).val(), '_token': '{{ csrf_token()}}',
            },
            success: function (response) {
                if (response.data == 'success') {
                    toastr.success("Updated");

                } else toastr.error("Can not update");
            }
        })
    }

    function measurementChange(id, key, value) {
        $.ajax({
            url: "{{ route('property.updateMeasurement') }}",
            type: "POST",
            dataType: "json",
            data: {
                'id': id, 'key': key, 'value': value, '_token': '{{ csrf_token()}}',
            },
            success: function (response) {
                if (response.data == 'success') {
                    toastr.success("Updated");
                    $('#totalMeasured').html(response.totalMeasured)
                    $('#sumMeasure').html(response.sumMeasure)

                } else toastr.error("Can not update");
            }
        })
    }
    $('form').submit(function(){
        $('button[type=submit]', this).attr('disabled', 'disabled');
    });

    $('#ShowAllGroups').click(function () {
        if ($('#ShowAllGroups').is(':checked')) {
            // price in property_group_items > 0
            $('.hide_incheck').show();
            $('#noGroups').show();
            $('.list-group-item').removeClass('active');
        } else {
            $('#listTable').hide();
            $('#measurementTable').hide();
            $('.hide_incheck').hide();
            $('#noGroups').show();
        }
    })
    $('.walkContainerSidebar').on('click', '.list-group-item', function () {
        if ($(this).hasClass('active')) {

        } else {
            $('.list-group-item').removeClass('active');
            $(this).addClass('active');
            $('#listTable').show();
            $('#measurementTable').show();
            $('#noGroups').hide();
            $('.group_name').html($(this).find('.group_item span').html());
            $('#group_walk_id').val($(this).attr('data-id'));
            if ($(this).attr('data-comment') == 1) $('.group_comment').show();
            else $('.group_comment').hide();
            $.ajax({
                url: "{{ route('property.loadItem') }}",
                type: "GET",
                dataType: "json",
                data: {
                    'property_id': $('#property_id').val(),
                    'group_id': $(this).attr('data-id'),
                    '_token': '{{ csrf_token()}}',
                },
                success: function (response) {
                    if (response.data == 'success') {
                        // $('.html_append').remove();
                        // $('#listTable tbody').append(response.items);
                        // $('.products tr').remove();
                        // $('.products').append(response.measurement);
                        $('#comments').val(response.groupWalkInfo.comment_text);
                        // $('#sumMeasure').html(response.sumMeasure);
                        var sumMeasurement = 0;
                        if(response.measurement.length)
                        {
                            var resultMeasurement='';
                            $.each(response.measurement,function (index,measurement){
                                    sumMeasurement+=(Number(measurement.measurement_l)*Number(measurement.measurement_w))
                                resultMeasurement+='<tr class="row_measurement'+measurement.id+' well well-sm">' +
                                    '<td><label>W</label> </td>' +
                                    '<td><input data="'+measurement.id+'" id="measurement_w'+measurement.id+'" value="'+(measurement.measurement_w==null?'':measurement.measurement_w)+'" type="number" step="1" class="measurement_w form-control" style="width: 100px"></td>' +
                                    '<td><label>L</label></td>' +
                                    '<td><input data="'+measurement.id+'" id="measurement_l'+measurement.id+'" value="'+(measurement.measurement_l==null?'':measurement.measurement_l)+'" type="number" step="1" class="measurement_l form-control" style="width: 100px"></td>' +
                                    '<td><select data="'+measurement.id+'" id="measurement_select'+measurement.id+'" class="measurement_select form-control">' +
                                    '<option value="">Select Remove </option>';
                                @foreach(\App\Models\PropertyGroupMeasure::measureType() as $key=>$value)
                                    resultMeasurement+='<option '+(measurement.measurement_select==="{{ $key }}"?"selected":"")+' value="{{ $key }}"> {{ $value }}</option>';
                                @endforeach
                                    resultMeasurement+='</select></td>'
                                resultMeasurement+='<td><label>Keep <input data="'+measurement.id+'" class="keepReplace'+measurement.id+'" '+(measurement.keepReplace===1?"checked":"")+' type="radio" name="keepReplace'+measurement.id+'" value="1"></label>   </td>';
                                resultMeasurement+='<td> <label>Replace With <input data="'+measurement.id+'" class="keepReplace'+measurement.id+'" '+(measurement.keepReplace===2?"checked":"")+' type="radio" name="keepReplace'+measurement.id+'" value="2"></label>   </td>';
                                resultMeasurement+='<td><select data="'+measurement.id+'" id="flooring_select'+measurement.id+'" class="flooring_select form-control '+(measurement.keepReplace!==2?"hidecustom":"")+'">';
                                resultMeasurement+='<option value="">Select Flooring </option>';
                                @foreach(\App\Models\PropertyGroupMeasure::measureType() as $key=>$value)
                                    resultMeasurement+='<option '+(measurement.flooring_select==="{{ $key }}"?"selected":"")+' value="{{ $key }}"> {{ $value }}</option>';
                                @endforeach
                                    resultMeasurement+='</select></td>';
                                resultMeasurement+='<td><span data="'+measurement.id+'" id="'+(index===0?"addMeasure":"delMeasure")+'" class="fas '+(index===0?"fa-plus":"fa-times")+' btn btn-xs delete " ></span></td>' +
                                    '</tr>';

                            }
                            )
                            $('.products').html(resultMeasurement);
                        }
                        else
                        {
                            $('.products').html('<tr class="row_first well well-sm"><td></td><td class="noResultMessage">Click to add Measurement Data </td><td><span data="0" id="addMeasure" title="Add" class="fas fa-plus btn btn-xs delete " ></span></td></tr>')
                        }
                        $('#sumMeasure').html(sumMeasurement)

                        $('#listTable tbody').html(renderItems(response.groupWalkInfo,response.items));
                    }
                }
            })
        }
    })

    function renderItems(group,items)
    {
        var result='';
        if(!Array.isArray(items))
            items=[items];
        $.each(items, function (index, value) {


            var approved = value.approved == 1 ? 'item_unapproved  fa-check-circle' : 'item_approved   fa-times-circle';
            result +=
                "<tr  id='item_" + value.item_id + "' class=\"html_append " + (value.price > 0 ? "show_item" : "") + "\">";

            if (!value.item_id) {
                result += '<td><input type="text" onchange="itemChange(' + value.id + ',\'item_name\')"  name="item_name" value="' + value.item_name + '" class="item_name_' + value.id + ' form-control">';
                {{--if(value.item_type_id==={{\App\Models\ItemTypesModel::PACKAGE_ROW_TYPE}})--}}
                {{--    result+='<input type="number" size="3"></td>'--}}
                {{--else--}}
                    result+='</td>'
                result += '<td><input type="text" onchange="itemChange(' + value.id + ',\'GL_code\')"  name="GL_code" value="' + (value.GL_code===null?"":value.GL_code) + '" class="GL_code_' + value.id + ' form-control"></td>'
                result += '<td align="center"><input type="number" min="0" max="9999999999.99" step="0.01" onchange="itemChange(' + value.id + ',\'price\')"  name="price" value="' + value.price + '" class="price_' + value.id + ' form-control"></td>'
                result +="<td >" +
                    "<textarea maxlength='255' onchange=\"itemChange(" + value.id + ",'description')\" id=\"" + value.id + "\"   item-id=\"" + value.item_id + "\"  "  +
                    "name=\"description\" class=\"description_textarea description_" + value.id + " form-control\" >" + (value.description == null ? '' : value.description) + "</textarea>" +
                    "</td>";

            } else {
                result += '<td>' + (value.name === null ? value.item_name : value.name) + '</td>';
                result += '<td>' + (value.gl_code === null ? "" : value.gl_code) + '</td>';
                result += '<td id="price_'+value.item_id+'" align="center">' + (value.price == null ? 0 : formatter.format(value.price)) + '</td>';
                result +="<td >" +
                    "<textarea maxlength='255' onclick='focusTextarea("+value.id+","+value.item_id+",this)'  onchange=\"itemChange(" + value.id + ",'description')\" id=\"" + value.id + "\"   item-id=\"" + value.item_id + "\"  "  +
                    "name=\"description\" class=\"description_textarea description_" + value.id + " form-control\" >" + (value.description == null ? '' : value.description) + "</textarea>" +
                    "</td>";

            }
            result += "<td align='center' class='action_column' width='170px'>  <span group_walk_id=\"" + group.id + "\" item-id=\"" + value.id + " \"" +
                "class=\" approved_" + value.id + "  fas p-2 " + approved + " delete\" ></span>" +
                "<span item_walk_id="+value.item_id+" item-id=\"" + value.id + "\" group_walk_id=\"" + group.id + "\" " +
                "class=\"item_delete far fa-trash-alt p-2 delete \" ></span>";
            if (value.has_product)
                result += "<span  item-id='"+value.item_id+"' group_walk_id='"+group.id+"'" +
                    " class='item_pull_product p-2  fas fa-angle-double-right' ></span></td>";
            else
                result += "</td>";

            result += "</tr>";

        })
        return result;
    }
    function focusTextarea(id,itemId,e){
        $('.autocomplete-items').remove();
        $('.autocomplete').removeClass('autocomplete');
        $(e).parent().addClass('autocomplete');
        var result = '';
        $.ajax({
            url:'{{route('property.loadItemDesc')}}',
            data:{id:itemId},
            success:function(data){
                if(data.items.length) {
                    $.each(data.items, function (key, item) {
                        result += '<div class="bindDesc" onclick="bindDesc('+id+',this)" >' + item.description + '</div>'
                    })
                    $(e).parent().append('<div class="autocomplete-items" >' + result + '</div>');
                }

            }
        })

    }
    $(document).click(function(event) {
        if(!$(event.target).hasClass('bindDesc') && !$(event.target).hasClass('description_textarea') )
        {
            $('.autocomplete-items').remove();
            $('.autocomplete').removeClass('autocomplete');
        }
    });
    function bindDesc(id,e)
    {
        var oldvalue = $('#'+id).val();

        if(oldvalue!=='')
            oldvalue+='\n';
        if(oldvalue.length>255)
        {
            toastr.error("Maximum 255 character!");
        }
        else
        {
            $('#' + id).val(oldvalue + $(e).html());

            itemChange(id, 'description')
        }
        $('.autocomplete-items').remove();
        $('.autocomplete').removeClass('autocomplete');

    }

    $('.walkContainerContent').on('click', '.item_pull_product', function () {
        $('.propertyWalkSection').hide();
        $('.skuDisplay').show();
        var id = $(this).attr('item-id');
        $('.item_name').html($('.html_append').find('span.name-' + id).html());
        $('#group_walk_id').val($(this).attr('group_walk_id'));
        $('#item_id').val($(this).attr('item-id'));
        $('#group_items_walk_id').val($(this).attr('id'));
        /*load product item*/
        $.ajax({
            url: "{{ route('property.loadProduct') }}",
            type: "GET",
            dataType: "json",
            data: {
                'property_id': $('#property_id').val(),
                'group_walk_id': $(this).attr('group_walk_id'),
                'item_id': $(this).attr('item-id'),
                '_token': '{{ csrf_token()}}',
            },
            success: function (response) {
                if (response.data == 'success') {
                    // $('#walk .list_product div').remove();
                    var resultItem = '';
                    $.each(response.items, function(key,item){
                        var totalPrice = formatter.format((Number(item.price) + Number(item.labor))*Number(item.qty));
                        var image = item.image? '{{ url('storage') }}/'+item.image: '{{url('storage/'.setting('site.product_def_image'))}}';
                        resultItem+='<div class="col-md-4 col-sm-6 col-xs-12 col-12  col-lg-4  p-2" style=" min-height: 215px">'+
                            '<div class="p-3" style="border: 1px solid gray;min-height: 225px;">'+
                            '<div class="row">'+
                            '<div class="col-xs-3 col-3 " align="center">'+
                            '<img class="img-fluid img-fluid img img-responsive" style="" src="'+image+'">'+
                            '</div>'+
                            '<div class="col-xs-9 col-9 ">'+
                            '<p class="product_name">'+item.name+'</p>'+
                            '<p>Price: <b>$</b><span style="font-weight: bold">'+item.price+'</span></p>'+
                            '<div class="form-inline"><div class="form-group qty-form">' +
                            '<label>Qty</label><div style="display: flex;flex-wrap: nowrap;align-items: center;"><button type="button" onclick="addQty(this)" ' +
                            'class="btn ">' +
                            '<i class="fas fa-plus"></i>' +
                            '</button>' +
                            '<input  style="width: 70px; text-align: center" id="qty-'+item.property_product_id+'" ' +
                            'onchange="qtyChange('+item.property_product_id+')" type="number" ' +
                            'onKeyPress="if(this.value.length==4) return false;" class="form-control" name="qty" value="'+item.qty+'" >' +
                            '<button type="button"  onclick="subQty(this)" class="btn ">' +
                            '<i class="fas fa-minus"></i>' +
                            '</button>' +
                            '</div></div></div>' +
                            '<input id="firstprice-'+item.property_product_id+'" type="hidden" value="'+item.price+'" >'+
                            '</div>'+
                            '</div>'+
                            '<div style="width:100%; text-align:center">'+
                            '</div>' +
                            '<div class="form-inline">'+
                            '<label style="width:30%; padding: 10px; float: left">Labor</label>' +
                            '<div class="input-group" style="width:70%; ">' +
                            '  <span style="    background: none;border-right: none;" class=" input-group-addon ">$</span>' +
                            '<input id="labor-'+item.property_product_id+'" onblur="laborChange('+item.property_product_id+')"' +
                            ' type="number" min="0" max="999999.99" step="0.01" name="labor" value="'+item.labor+'" ' +
                            'class="form-control" style=" padding: 10px; float: left;border-right: none;border-color: #cccccc;" >' +
                            '<span style="    background: none;border-left: none;" class="input-group-addon">/items</span>' +
                            '</div>' +
                            '</div><hr><p align="center">Total <span style="color: #c1bebe">(Unit price + Labor)x Qty</span>:' +
                            ' <b id="price-'+item.property_product_id+'">'+totalPrice+'</b></p>'+
                            '</div></div>';
                    })
                    $('.list_product').html(resultItem);
                }
            }
        })
    })
    function addQty(ele)
    {
        var current = Number($(ele).next().val());
        if (current < 9999) {
            $(ele).next().val(current + 1);
            $(ele).next().trigger("change");
        } else {
            $(ele).next().val(current)
        }
    }
    function subQty(ele)
    {
        var current = Number($(ele).prev().val());
        if (current > 0) {
            $(ele).prev().val(current - 1)
            $(ele).prev().trigger("change");

        } else {
            $(ele).prev().val(current)
        }
    }
    $('#walk').on('click', '.backto_item', function () {
        $('.skuDisplay').hide();
        $('.propertyWalkSection').show();
    })
    $('#ShowAllItems').click(function () {
        $('.group_comment').hide();
        if ($('#ShowAllItems').is(':checked')) {
            // if price in property_group_items_walk > 0 */
            $('.html_append').show();
        } else {
            $('.html_append').hide();
            $('.show_item').show();

        }
    })

    $('.walkContainerContent').on('click', '.item_approved', function () {
        var id = $(this).attr('item-id');
        $.ajax({
            url: "{{ route('property.approvedItem') }}",
            type: "POST",
            dataType: "json",
            data: {
                'id': $(this).attr('item-id'), '_token': '{{ csrf_token()}}',
            },
            success: function (response) {
                if (response.data == 'success') {
                    $('.approved_' + id).removeClass(' fa-times-circle');
                    $('.approved_' + id).removeClass('item_approved');
                    $('.approved_' + id).addClass('item_unapproved');
                    $('.approved_' + id).addClass('fa-check-circle');
                }
            }
        })
    })
    $('.walkContainerContent').on('click', '.item_unapproved', function () {
        var id = $(this).attr('item-id');
        $.ajax({
            url: "{{ route('property.unApprovedItem') }}",
            type: "POST",
            dataType: "json",
            data: {
                'id': $(this).attr('item-id'), '_token': '{{ csrf_token()}}',
            },
            success: function (response) {
                if (response.data == 'success') {
                    $('.approved_' + id).removeClass('fa-check-circle');
                    $('.approved_' + id).removeClass('item_unapproved');

                    $('.approved_' + id).addClass(' fa-times-circle');
                    $('.approved_' + id).addClass('item_approved');
                }
            }
        })
    })
    $('.walkContainerContent').on('click', '.item_delete', function () {
        var id = $(this).attr('item-id')
        var item_walk_id = $(this).attr('item_walk_id')
        $.ajax({
            url: "{{ route('property.deleteItem') }}",
            type: "POST",
            dataType: "json",
            data: {
                'id': id, '_token': '{{ csrf_token()}}',
            },
            success: function (response) {
                if (response.data == 'success') {
                    $('#item_' + item_walk_id).remove();
                }
            }
        })
    })
    $('.walkContainerContent').on('click', '#addItemButton', function () {
        var name = $('input[name="item_name"]').val();
        var price = $('input[name="item_price"]').val();
        var description = $('textarea[name="description"]').val();
        var GL_code = $('input[name="GL_code"]').val();
        if (name === '')
            toastr.error("Item name not be null!");
        else {
            $.ajax({
                url: "{{ route('property.addItem') }}",
                type: "POST",
                dataType: "json",
                data: {
                    'property_id': $('#property_id').val(),
                    'group_walk_id': $('#group_walk_id').val(),
                    'item_name': name,
                    'price': price,
                    'GL_code': GL_code,
                    'description': description,
                    '_token': '{{ csrf_token()}}',
                },
                success: function (response) {
                    if (response.data == 'success') {

                        $('#listTable tbody').append(renderItems({id:$('#group_walk_id').val()},response.items));
                        $('.fix_row input[name="item_name"]').val('');
                        $('.fix_row input[name="item_price"]').val('');
                        $('.fix_row textarea[name="description"]').val('');
                        $('.fix_row input[name="GL_code"]').val('');
                    }
                }
            })
        }
    })
    $('#modalcomments').on('click', '#add_comment', function () {
        $.ajax({
            url: "{{ route('property.addComment') }}",
            type: "POST",
            dataType: "json",
            data: {
                'property_id': $('#property_id').val(),
                'group_walk_id': $('#group_walk_id').val(),
                'comment': $('#comments').val(),
                '_token': '{{ csrf_token()}}',
            },
            success: function (response) {
                if (response.data == 'success') {
                    $('.close').click();
                    toastr.success("Save comments success!");
                }
            }
        })
    })
    $('#measurementTable').on('click', 'input[type="radio"]', function () {
        var id = $(this).attr('data');
        if ($(this).val() == 2) $('#flooring_select' + id).show();
        else $('#flooring_select' + id).hide();
        /*call function save this item*/
        measurementChange(id, key = 'keepReplace', value = $(this).val());
    })
    $('#measurementTable').on('change', '.measurement_w', function () {
        var id = $(this).attr('data');
        measurementChange(id, key = 'measurement_w', $(this).val());
    })
    $('#measurementTable').on('change', '.measurement_l', function () {
        var id = $(this).attr('data');
        measurementChange(id, key = 'measurement_l', $(this).val());
    })
    $('#measurementTable').on('change', '.measurement_select', function () {
        var id = $(this).attr('data');
        measurementChange(id, key = 'measurement_select', $(this).val());
    })
    $('#measurementTable').on('change', '.flooring_select', function () {
        var id = $(this).attr('data');
        measurementChange(id, key = 'flooring_select', $(this).val());
    })
    $('#measurementTable').on('click', '#addMeasure', function () {
        var data = $(this).attr('data');
        if (data == 0) {
            var btnclass = 'fa-plus';
            var idname = 'addMeasure';
        } else {
            var btnclass = ' fa-times';
            var idname = 'delMeasure';
        }
        $.ajax({
            url: "{{ route('property.addMeasure') }}",
            type: "POST",
            dataType: "json",
            data: {
                'property_id': $('#property_id').val(),
                'group_walk_id': $('#group_walk_id').val(),
                '_token': '{{ csrf_token()}}',
            },
            success: function (response) {
                if (response.data == 'success') {
                    //toastr.success("Add item success!"); add here
                    var result = '';
                    result += "<tr class='row_measurement" + response.id + " well well-sm'>" +
                        "<td><label>W</label> </td>" +
                        "<td><input data='" + response.id + "' id='measurement_w" + response.id + "' value='0' type='text' class='measurement_w form-control' style='width: 50px'></td>" +
                        "<td><label>L</label></td>" +
                        "<td><input data='" + response.id + "' id='measurement_l" + response.id + "' value='0' type='text' class=' measurement_l form-control' style='width: 50px'></td>" +
                        "<td>" +
                        "<select data='" + response.id + "' id='measurement_select" + response.id + "' class='measurement_select form-control'>" +
                        "<option value=''>Select Remove </option>" +
                        "<option value='Carpet'>Carpet </option>" +
                        "<option value='Tile'>Tile </option>" +
                        "<option value='Vinyl'>Vinyl </option>" +
                        "<option value='Laminate'>Laminate </option>" +
                        "<option value='Concrete'>Concrete </option>" +
                        "<option value='Hardwood'>Hardwood </option>" +
                        "</select>" +
                        "</td>" +
                        "<td>  <label>Keep <input data='" + response.id + "' class='keepReplace" + response.id + " ' type='radio' name='keepReplace" + response.id + "' value='1'></label> </td>" +
                        "<td> <label> Replace With <input data='" + response.id + "' class='keepReplace" + response.id + "' type='radio' name='keepReplace" + response.id + "' value='2'></label>  </td>" +
                        "<td>" +
                        "<select data='" + response.id + "' id='flooring_select" + response.id + "' class='form-control flooring_select hidecustom'>" +
                        "<option value=''>Select Flooring </option>" +
                        "<option value='Carpet'>Carpet </option>" +
                        "<option value='Tile'>Tile </option>" +
                        "<option value='Vinyl'>Vinyl </option>" +
                        "<option value='Laminate'>Laminate </option>" +
                        "<option value='Concrete'>Concrete </option>" +
                        "<option value='Hardwood'>Hardwood </option>" +
                        "</select>" +
                        "</td>" +
                        "<td>" +
                        "<span data='" + response.id + "' id='" + idname + "' class='fas " + btnclass + " btn btn-xs delete ' ></span>" +
                        "</td>" +
                        "</tr>";
                    if (data == 0) $('.row_first').remove();
                    $('#measurementTable .products').append(result);
                }
            }
        })
    })
    $('#measurementTable').on('click', '#delMeasure', function () {
        if(confirm('Are you sure to delete this measurement items?')) {
            var id = $(this).attr('data');
            $.ajax({
                url: "{{ route('property.delMeasurement') }}",
                type: "POST",
                dataType: "json",
                data: {
                    'id': $(this).attr('data'), '_token': '{{ csrf_token()}}',
                },
                success: function (response) {
                    if (response.data == 'success') {
                        $('#totalMeasured').html(response.totalMeasured)
                        $('#sumMeasure').html(response.sumMeasure)
                        $('.row_measurement' + id).remove();
                        toastr.success("Delete row success!");
                    }
                }
            })
        }
    })
    $('document').ready(function () {
        $('#listTable').hide();
        $('#measurementTable').hide();
        $('.skuDisplay').hide();
        $('.group_comment').hide();
    });
</script>
