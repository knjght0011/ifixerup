@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <input type="hidden" value="@if($edit){{$id}}@endif" name="property_id" id="property_id"/>
                <div class="panel panel-bordered">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="{{ !\Illuminate\Support\Facades\Session::get('walk')?'active':'' }}"><a href="#edit" aria-controls="edit" role="tab"
                                              data-toggle="tab">Property</a></li>
                        <li class="{{ \Illuminate\Support\Facades\Session::get('walk')?'active':'' }} @if(!$edit)disabled @endif"><a href="#@if($edit)walk @endif" aria-controls="walk"
                                                                  role="tab" data-toggle="tab">Property Walk</a></li>
                        <li class="@if(!$edit)disabled @endif"><a href="#@if($edit)review @endif" aria-controls="review"
                                                                  role="tab" data-toggle="tab">Quick Review</a></li>
                        <li class="@if(!$edit)disabled @endif"><a href="#@if($edit)budget @endif" aria-controls="budget"
                                                                  role="tab" data-toggle="tab">Budget</a></li>
                        @if($edit)
                            <li ><a href="#uploadImage" aria-controls="uploadImage"
                                    role="tab" data-toggle="tab">Images</a></li>
                        @endif
                        @if($edit && $dataTypeContent->walked)
                            <li class="@if(!$edit)disabled @endif"><a href="#@if($edit)project_tools @endif"
                                                                      aria-controls="project_tools"
                                                                      role="tab" data-toggle="tab">Project Tools</a>
                            </li>
                        @endif
                    </ul>
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane {{ !\Illuminate\Support\Facades\Session::get('walk')?'active':'' }}" id="edit">
                            <!-- form start -->
                            <form role="form"
                                  class="form-edit-add"
                                  action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                                  method="POST" enctype="multipart/form-data">
                                <!-- PUT Method if we are editing -->
                            @if($edit)
                                {{ method_field("PUT") }}
                            @endif

                            <!-- CSRF TOKEN -->
                                {{ csrf_field() }}

                                <div class="panel-body">

                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                <!-- Adding / Editing -->
                                    @php
                                        $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                                    @endphp

                                    @foreach($dataTypeRows as $row)
                                    <!-- GET THE DISPLAY OPTIONS -->
                                        @php
                                            $display_options = $row->details->display ?? null;
                                            if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                                $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                            }
                                        @endphp
                                        @if (isset($row->details->legend) && isset($row->details->legend->text))
                                            <legend class="text-{{ $row->details->legend->align ?? 'center' }}"
                                                    style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                        @endif

                                        <div
                                            class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                            {{ $row->slugify }}
                                            <label class="control-label {{ ($row->required ||
		(isset($row->details) && isset($row->details->required)))?"label-require":"" }}"
                                                   for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                                            @if (isset($row->details->view))
                                                @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                            @elseif ($row->type == 'relationship')
                                                @include('voyager::formfields.relationship', ['options' => $row->details])
                                            @else
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            @endif

                                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                            @endforeach
                                            @if ($errors->has($row->field))
                                                @foreach ($errors->get($row->field) as $error)
                                                    <span class="help-block">{{ $error }}</span>
                                                @endforeach
                                            @endif
                                        </div>
                                    @endforeach
                                </div><!-- panel-body -->

                                <div class="panel-footer">
                                    @section('submit-buttons')
                                        <button type="submit"
                                                class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                                    @stop
                                    @yield('submit-buttons')
                                </div>
                            </form>

                        </div><!--end #edit-->
                        @if($edit)
                            <div role="tabpanel" class="tab-pane "
                                 id="uploadImage">
                                <!-- Button trigger modal -->


                                <!-- Modal -->
                                <div class="modal fade" id="addImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel"> Add an image</h4>

                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            {!! Form::open(['url'=>route('voyager.property.addPropertyImage',$dataTypeContent->id),'files'=>true]) !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="" class="label-require">Choose an image</label>
                                                    <input required type="file" accept="image/*" name="image">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Comment</label>
                                                    <textarea name="comment" rows="3" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-primary">Add</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                {!! Form::open(['url'=>route('voyager.property.updatePropertyImage',$dataTypeContent->id)]) !!}
                                @method('put')
                                <table class="table table-bordered ">
                                    <thead>
                                    <tr>
                                        <th colspan="4" style="text-align: right!important;">
                                            <div  style="display:flex;justify-content: space-between">
                                                <button type="button" class="btn btn-primary "
                                                        data-toggle="modal" data-target="#addImage">
                                                    Add an image
                                                </button>
                                                <button type="submit" class="btn btn-success">Update</button>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th align="center">No</th>
                                        <th align="center">Image</th>
                                        <th align="center">Comment</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataTypeContent->images as $image)
                                        <tr>
                                            <td align="center">{{ $loop->iteration }}</td>
                                            <td align="center">
                                                <a href=" {{$image->image_url}}" target="_blank">
                                                    <img src="{{ $image->image_url }}" width="50px" alt="">
                                                </a>
                                            </td>
                                            <td>
                                            <textarea placeholder="Comment" name="comment_{{ $image->id }}"
                                                      class="form-control" rows="3">{{ $image->comment }}</textarea>
                                            </td>
                                            <td width="20px">
                                                <a href="{{ route('voyager.property.deletePropertyImage',$image->id) }}" class="btn btn-danger btn-sm">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! Form::close() !!}

                            </div>
                        <div role="tabpanel" class="tab-pane {{ \Illuminate\Support\Facades\Session::get('walk')?'active':'' }}" id="walk">

                            <!--Check if property added flow-->
                            @if(isset($property_groups) && count($property_groups)>0)
                                <div class="propertyWalkSection">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <h4>Groups
                                                <div class="pull-right ng-binding" >
                                                    <span id="totalMeasured">{{ $totalMeasured }} </span>/ {{ $dataTypeContent->main_square_ft }} SqFt
                                                </div>
                                            </h4>
                                        </div>
                                        <div class="col-md-9">
                                            <ul class="nav nav-pills">
                                                <li role="presentation" class="dropdown">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" href=""
                                                       role="button" aria-haspopup="true" aria-expanded="false">
                                                        <span class="glyphicon glyphicon-plus-sign"></span> Add Group
                                                        <span class="caret"></span>
                                                    </a>
                                                    <ul class="dropdown-menu shortDropdown">
                                                        @foreach($groups as $item)
                                                            <li class="ng-scope"><a
                                                                    onclick="addGroupToWalk({{ $item->id }},name='{{$item->name}}')"
                                                                    class="addGroupToWalk ng-binding">{{ $item->name }}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                                <li class="group_comment">
                                                    <a href="#" data-toggle="modal" data-target="#modalcomments"> <span
                                                            class="glyphicon glyphicon-comment"></span><span
                                                            class="glyphicon glyphicon-plus edit"
                                                            style="position:relative; right:10px; top:5px; font-size: 65%"></span>Comment
                                                    </a>
                                                </li>
                                                @if(!$dataTypeContent->walked)
                                                    <li role="presentation" class="dropdown pull-right">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" href=""
                                                           role="button" aria-haspopup="true" aria-expanded="false">
                                                            Actions
                                                            <span class="caret"></span></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li class="ng-scope">
                                                                <a href="{{ route('voyager.property.walked',$dataTypeContent->id) }}">Set
                                                                    Walked</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 walkContainerSidebar">
                                            <div class="panel panel-default">
                                                <div class="panel-heading p-3">
                                                    {{--                                                    <div class="checkchowall">--}}
                                                    {{--                                                        <label for="ShowAllGroups" id="ShowAllGroupsLabel"--}}
                                                    {{--                                                               class="showAllLabel">--}}
                                                    {{--                                                            <input checked type="checkbox" id="ShowAllGroups"--}}
                                                    {{--                                                                   name="ShowAllGroups" class=""> Show All Groups--}}
                                                    {{--                                                        </label>--}}
                                                    {{--                                                    </div>--}}
                                                    Groups
                                                </div>
                                                @foreach($active_groups as $item)
                                                    @php
                                                        $price = \App\Models\PropertyGroupItemWalk::where('property_id',$id)->where('group_walk_id',$item->group_walk_id)->sum('price');
                                                    @endphp
                                                    <div class="list-group @if($price <= 0)hide_incheck  @endif">
                                                        <div data-id="{{$item->group_walk_id}}"
                                                             data-comment="{{$item->setcomment}}"
                                                             class="list-group-item clickable ng-scope">
													<span class="group_item">
														<span class="ng-binding">{{$item->name}}</span>
														<span
                                                            class="glyphicon glyphicon-pencil edit pull-right clickable"></span>
													</span>
                                                        </div>

                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-md-9 walkContainerContent">
                                            <div class="panel panel-default ng-hide" id="flowGrp" disabled="disabled">
                                                <div class="table-responsive">
                                                    <table id="measurementTable"
                                                           class="table table-striped table-hover groupMeasurements">
                                                        <thead>
                                                        <tr>
                                                            <th colspan="10">Measurement <small> <span class=""> (<span
                                                                            id="sumMeasure"></span> SqFt) </span>
                                                                </small>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="products">
                                                        <!--<tr class="well well-sm">
                                                           <td></td>
                                                           <td class="noResultMessage">Click to add Measurement Data </td>--
                                                           <td><label>W</label> </td>
                                                           <td><input type="text" class="form-control" style="width: 50px"></td>
                                                           <td><label>L</label></td>
                                                           <td><input type="text" class="form-control" style="width: 50px"></td>
                                                           <td>
                                                              <select class="form-control">
                                                                 <option value="">Select Remove </option>
                                                                 <option value="Carpet">Carpet </option>
                                                                 <option value="Tile">Tile </option>
                                                                 <option value="Vinyl">Vinyl </option>
                                                                 <option value="Laminate">Laminate </option>
                                                                 <option value="Concrete">Concrete </option>
                                                                 <option value="Hardwood">Hardwood </option>
                                                              </select>
                                                           </td>
                                                           <td> <input type="radio" name="keepReplace0" value="No"> Keep </td>
                                                           <td> <input type="radio" name="keepReplace0" value="Yes"> Replace With </td>
                                                           <td>
                                                              <select class="form-control invisible">
                                                                 <option value="">Select Flooring </option>
                                                                 <option value="Carpet">Carpet </option>
                                                                 <option value="Tile">Tile </option>
                                                                 <option value="Vinyl">Vinyl </option>
                                                                 <option value="Laminate">Laminate </option>
                                                                 <option value="Concrete">Concrete </option>
                                                                 <option value="Hardwood">Hardwood </option>
                                                              </select>
                                                           </td>
                                                           <td> </td>
                                                           <td>
                                                              <span id="addMeasure" title="Add" class="glyphicon glyphicon-plus btn btn-xs delete " ></span>
                                                          </td>
                                                        </tr>-->
                                                        </tbody>
                                                    </table>
                                                </div>


                                                <div class="table-responsive">

                                                    <table id="listTable" class="table table-striped table-hover table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th colspan="5">
                                                                    <label for="ShowAllItems" id="ShowAllItemsLabel"
                                                                    >
                                                                        <b>Show All Items</b>
                                                                    </label>
                                                                    <input
                                                                        type="checkbox" checked id="ShowAllItems"
                                                                        name="ShowAllItems" class="">
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th style="min-width: 150px">Item Name</th>
                                                            <th style="min-width: 150px">GL Code</th>
                                                            <th style="min-width: 150px">Price($)</th>
                                                            <th style="min-width: 150px">Description</th>

                                                            <th style="min-width: 150px">Approved</th>

                                                        </tr>
                                                        <tr class="fix_row" style="background: white;">
                                                            {{--                                                            <td></td>--}}
                                                            <td>
                                                                <input type="text" name="item_name"
                                                                       class="form-control" maxlength="255">
                                                            </td>
                                                            {{--                                                            <td></td>--}}
                                                            <td>
                                                                <input name="GL_code" maxlength="10" class="form-control" type="text">
                                                            </td>

                                                            <td>
                                                                <input type="number" id="price" name="item_price"
                                                                       class="form-control" min="0" max="99999.99"
                                                                       onKeyPress="if(this.value.length==5) return false;"
                                                                       step="0.01">
                                                            </td>
                                                            <td  >
                                                            <textarea name="description" type="text" maxlength="255"
                                                                      class="form-control itemDescription"></textarea>

                                                            </td>
                                                            <td >
                                                                <button id="addItemButton" class="btn btn-warning">
                                                                    Add Item
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        <!--<tr>
                                                             <td></td>
                                                             <td>
                                                                <span class="ng-binding ng-scope">Appliances</span>
                                                             </td>
                                                             <td></td>
                                                             <td></td>
                                                             <td>
                                                                <textarea type="text" class="form-control" ></textarea>
                                                              </td>
                                                             <td>
                                                                 <input type="text" id="itemAmt0" name="itemAmt0" class="form-control"   readonly="readonly">
                                                             </td>
                                                             <td> <span title="Approved" class="glyphicon btn btn-xs glyphicon-minus-sign delete" ></span>   </td>
                                                             <td>
                                                                <span id="itemHistoryBtn" title="View Item History" class="fa fa-history fa-lg clickable" ></span>
                                                             </td>
                                                             <td><span title="Delete" class="glyphicon glyphicon-remove-circle btn btn-xs delete pull-right" ></span> </td>
                                                             <td>
                                                                <span class="glyphicon glyphicon-menu-right btn btn-xs pull-right" ></span>
                                                             </td>
                                                          </tr>-->


                                                        </tbody>
                                                    </table>
                                                </div>


                                            </div>
                                            <div id="noGroups" class="well noResultMessage ng-scope">Select Group to
                                                show details
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="skuDisplay">
                                    <div class="page-header">

                                        <button type="button" class="backto_item btn btn-success pull-right ">Save
                                        </button>
                                        <button type="button" class="backto_item btn btn-warning mr-3 pull-right">Back
                                        </button>
                                        <h4 class="breadcrumbs"><span class="group_name"></span> &gt; <span
                                                class="item_name"></span></h4>
                                    </div>
                                    <div class="row list_product">
                                    </div>
                                </div>
                                <input type="hidden" id="group_walk_id"/>
                                <input type="hidden" id="item_id"/>
                                <input type="hidden" id="group_items_walk_id"/>
                            @else
                                {!! Form::open(['url'=>route('voyager.property.createWalk',$dataTypeContent->id)]) !!}
                                <button type="submit" id="addWalkBtn" class="btn btn-warning">
                                    CREATE NEW WALK
                                </button>
                                {!! Form::close() !!}

                            @endif
                            {{--                            </div>--}}
                        </div><!--end #walk-->

                        {{--                        </div><!--end #walk-->--}}
                        <div role="tabpanel" class="tab-pane" id="review">
                            <div>
                                <span id="countApproved"></span> Approved, <span id="countDenied"></span> Denied, Total Budget: <span id="totalBudget"></span>, Approved Budget: <span id="approvedBudget"></span>
                            </div>
                            <table class="table table-bordered walkContainerContent" id="table_review">
                                <thead>
                                <tr>
                                    <th>
                                        Item
                                    </th>
                                    <th>
                                        Group
                                    </th>
                                    <th>
                                        GL Code
                                    </th>
                                    <th>
                                        Description
                                    </th>
                                    <th>
                                        Price ($)
                                    </th>
                                    <th>
                                        Approved
                                    </th>
                                    <th>
                                        Image
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="table_review_body">

                                </tbody>
                            </table>
                        </div><!--end #review-->
                        <div role="tabpanel" class="tab-pane" id="budget">
                            <table class="table ">
                                <thead>
                                <tr>
                                    <th colspan="2" class="th-center">
                                        Scope of Work
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <b>Project Name:</b>
                                    </td>
                                    <td>
                                        {{ $dataTypeContent->project_name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Address:</b>
                                    </td>
                                    <td>
                                        {{ $dataTypeContent->full_address }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Date Created:</b>
                                    </td>
                                    <td>
                                        {{ convertSystemTzToUserTz($dataTypeContent->created_at) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Main SQFt:</b>
                                    </td>
                                    <td>
                                        {{ $dataTypeContent->main_square_ft?$dataTypeContent->main_square_ft:0 }} sf
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Measured SQFT:</b>
                                    </td>
                                    <td>
                                        <span id="measuredSQFT"></span> sf
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Bed/Bath:</b>
                                    </td>
                                    <td>
                                        {{ $dataTypeContent->number_bedrooms?$dataTypeContent->number_bedrooms:0 }}
                                        bd,
                                        {{ $dataTypeContent->number_bathrooms?$dataTypeContent->number_bathrooms:0 }}
                                        ba
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>Year Built:</b>
                                    </td>
                                    <td>
                                        {{ $dataTypeContent->year_build?$dataTypeContent->year_build:0 }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Budget total</b>
                                    </td>
                                    <td style="font-weight: bold" id="total_budget">

                                    </td>
                                </tr>

                                </tbody>
                            </table>
                            <div id="productLists">

                            </div>


                        </div><!--end #budget-->
                        @endif
                        @if($edit && $dataTypeContent->walked)
                            <div role="tabpanel" class="tab-pane" id="project_tools">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <h2>
                                            Property Docs
                                            <hr>
                                        </h2>
                                        <a href="{{ route('voyager.property.export',['id'=>$dataTypeContent->id,'export'=>'property_excel']) }}"
                                           class="btn btn-warning">
                                            Export Excel
                                        </a>
                                        <br>
                                        <a href="{{ route('voyager.property.export',['id'=>$dataTypeContent->id,'export'=>'pdf']) }}"
                                           class="btn btn-warning">
                                            Export PDF
                                        </a>
                                        <br>
                                        <a href="{{ route('voyager.property.export',['id'=>$dataTypeContent->id,'export'=>'approved_items']) }}"
                                           class="btn btn-warning">
                                            Export Approved Product (Excel)
                                        </a>
                                        <br>
                                        <a href="{{ route('voyager.property.export',['id'=>$dataTypeContent->id,'export'=>'approved_items_pdf']) }}"
                                           class="btn btn-warning">
                                            Export Approved Product (PDF)
                                        </a>
                                    </div>
                                    {{--                                <div class="col-xs-12 col-md-6">--}}
                                    {{--                                    <h2>--}}
                                    {{--                                        Property Ordering--}}
                                    {{--                                        <hr>--}}
                                    {{--                                    </h2>--}}
                                    {{--                                    <a href="{{ route('voyager.property.export',['id'=>$dataTypeContent->id,'export'=>'product_csv']) }}" class="btn btn-warning">--}}
                                    {{--                                        Export Product SKU CSV--}}
                                    {{--                                    </a>--}}
                                    {{--                                </div>--}}
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title"><i
                            class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }}?
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalcomments" tabindex="-1" role="dialog"
         aria-labelledby="modalcommentsTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalcommentsTitle">
                        Comments
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea id="comments" name="comments" type="text" class="form-control" style="height: 300px;"
                              maxlength="4000"></textarea>
                </div>
                <div class="modal-footer">
                    <button id="add_comment" class="btn btn-warning pull-left" type="button">OK</button>
                    <small style="color: grey;"><span>4000</span> characters remaining</small>
                </div>
            </div>
        </div>
    </div>

    <!-- End Delete File Modal -->
    <style>
        .voyager .nav-tabs > li.active > a:hover {
            background-color: #62a8ea;
        }
    </style>
    <div id="loading-overlay">
        <div class="loading-icon"></div>
    </div>
    <style>

        .autocomplete {
            position: relative;

        }

        .autocomplete-items {
            z-index: 1;
            position: absolute;
            margin-top: 3px;
            width: 100%;
            padding: 10px 10px 0 10px;
            -webkit-transform: translate(-50%);
            left: 50%;
            /*top: -80%;*/
            max-height: 200px;
            overflow-y: auto;
        }

        .autocomplete-items div {
            cursor: pointer;
            background: white;
            margin-bottom: 1px;
            border: 1px solid black;
            padding: 10px;
        }

        .autocomplete-items div:hover {
            /*when hovering an item:*/
            background-color: #e9e9e9;
        }
        #loading-overlay {
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            display: none;
            align-items: center;
            background-color: #000;
            z-index: 999;
            opacity: 0.5;
        }

        .loading-icon {
            position: absolute;
            border-top: 2px solid #fff;
            border-right: 2px solid #fff;
            border-bottom: 2px solid #fff;
            border-left: 2px solid #767676;
            border-radius: 25px;
            width: 25px;
            height: 25px;
            margin: 0 auto;
            position: absolute;
            left: 50%;
            margin-left: -20px;
            top: 50%;
            margin-top: -20px;
            z-index: 4;
            -webkit-animation: spin 1s linear infinite;
            -moz-animation: spin 1s linear infinite;
            animation: spin 1s linear infinite;
        }

        @-moz-keyframes spin {
            100% {
                -moz-transform: rotate(360deg);
            }
        }

        @-webkit-keyframes spin {
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
@stop
@section('head')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"
          integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin="anonymous"/>
@endsection
@section(/** @lang text */'javascript')

    <script>
        $(document).ajaxStart(function () {

            $('#loading-overlay').show()
        })
        $(document).ajaxComplete(function () {

            $('#loading-overlay').hide()
        })
        $(document).ajaxError(function () {

            $('#loading-overlay').hide()
        })

    </script>
@include('vendor.voyager.property.walkProperty_js')
@stop
