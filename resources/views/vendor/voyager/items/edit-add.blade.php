@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
    $haveProduct = $edit && $dataTypeContent->type->include_product;
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif

                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                            <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? null;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}"
                                            style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif

                                <div
                                    class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label {{ ($row->required ||
(isset($row->details) && isset($row->details->required)))?"label-require":"" }}"
                                           for="name">{{ $row->getTranslatedAttribute('display_name') }}
                                        @if($row->details && property_exists($row->details,'currency'))
                                            ({{ env('CURRENCY_PREFIX',env('CURRENCY_SUFFIX','$')) }})
                                        @endif
                                    </label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach

                                </div>
                            @endforeach


                            <div id="fieldItemType">
                                @if($edit)
                                    @foreach($dataTypeContent->type->form as $field)
                                        @if($field->field_type=='checkbox')
                                            <div class="form-group col-md-2">
                                                <label class="control-label  {{ $field->required?"label-require":"" }}"
                                                       for="">
                                                    {{ $field->name }}
                                                </label><br/>
                                                <input type="checkbox" name="{{ $field->slug }}"
                                                       {{ $field->required?"required":"" }}
                                                       {{ ($dataTypeContent->fieldValue($field->id) && $dataTypeContent->fieldValue($field->id)->value)?'checked':'' }}
                                                       class="toggleswitch" data-on="Yes" data-off="No">
                                            </div>
                                        @elseif($field->field_type=='number')
                                            <div class="form-group col-md-6">
                                                <label class="control-label  {{ $field->required?"label-require":"" }}"
                                                       for="">
                                                    {{ $field->name }} {{ $field->currency?" ($)":"" }}
                                                </label><br/>
                                                <input min="0" max="999999.99" step="0.01" type="number"
                                                       name=" {{ $field->slug }}"
                                                       {{ $field->required?"required":"" }}
                                                       value="{{ $dataTypeContent->fieldValue($field->id)?$dataTypeContent->fieldValue($field->id)->value:'' }}"
                                                       class="form-control">
                                            </div>
                                        @else
                                            <div class="form-group col-md-6">
                                                <label class="control-label  {{ $field->required?"label-require":"" }}"
                                                       for="">
                                                    {{ $field->name }}
                                                </label><br/>
                                                <input maxlength="255" type="text" name=" {{ $field->slug }}"
                                                       {{ $field->required?"required":"" }}
                                                       value="{{ $dataTypeContent->fieldValue($field->id)?$dataTypeContent->fieldValue($field->id)->value:'' }}"
                                                       class="form-control">
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>


                            <div class="col-md-12" style="padding: 10px; border: 1px solid black" id="addProducts">
                                <h3>
                                    <button type="button" data-toggle="modal" data-target="#modalResultProduct"
                                            class="btn btn-warning">
                                        <i class="icon voyager-plus"></i>
                                        Add Products
                                    </button>

                                </h3>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>
                                            SKU
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Description
                                        </th>
                                        <th>
                                            Price ($)
                                        </th>
                                        <th>

                                        </th>
                                        <th>
                                            Default Labor
                                        </th>
                                        <th>
                                            Default Hours
                                        </th>
                                        <th>
                                            GL Code
                                        </th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody id="resultProduct">
                                    @if($haveProduct)
                                        @foreach($dataTypeContent->products as $item )
                                            @php
                                                $product = $item->product;
                                            @endphp
                                            <tr id="addedProduct_{{ $product->id }}" itemid="{{ $product->id }}">
                                                <td>{{ $product->SKU }}</td>
                                                <td> {{ $product->name }} </td>
                                                <td> {{ \Illuminate\Support\Str::limit($product->description,55) }}  </td>
                                                <td> {{ $product->price }}  </td>
                                                <td><img src='{{ $product->image_url }}' style='width: 30px' alt=''>
                                                </td>

                                                <td>
                                                    <input type='number' min='0' max='999999.99' step='0.01'
                                                           class='form-control'
                                                           name='labor_product_{{ $product->id }}'
                                                           value='{{ $item->labor }}'/></td>

                                                <td><input type='number' min='0' max='999999.99' step='0.01'
                                                           class='form-control'
                                                           name='hours_product_{{ $product->id }}'
                                                           value='{{ $item->hours }}'/></td>

                                                <td><input type='text' maxlength='255' class='form-control'
                                                           name='glcode_product_{{ $product->id }}'
                                                           value='{{ $item->gl_code }}'/></td>

                                                <td>
                                                    <button class='btn btn-danger btn-sm' type="button"
                                                            onclick='removeAddedProduct({{ $product->id }})'><i
                                                            class=' voyager-x'></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>

                                </table>

                            </div>

                            <input type="hidden" name="addedProduct" id="addedProduct"
                                   value="{{ $haveProduct?implode(',',$dataTypeContent->products()->pluck('product_id')->toArray()):'' }}">

                            @if(!$edit || ($edit && $dataTypeContent->item_type_id!=\App\Models\ItemTypesModel::WHOLE_BUDGET_PERCENT) )
                                <div id="sectionDescription" class="form-group col-md-12">
                                    <label class="control-label">
                                        Description
                                    </label>
                                    {!! Form::select('description[]',
$edit?$dataTypeContent->description()->pluck('description','description')->toArray():[],
$edit?$dataTypeContent->description()->pluck('description')->toArray():'',['class'=>'form-control description','multiple'=>'true']) !!}
                                </div>
                            @endif
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit"
                                        class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }}?
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalResultProduct" tabindex="-1" role="dialog"
         aria-labelledby="modalResultProductTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalResultProductTitle">
                        Search Products
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div align="center">
                        <div class="row">
{{--                            <div class="">--}}
                            <div class="form-group col-xs-3">
                                <input type="text" id="searchSku" class=" form-control" placeholder="SKU">
                            </div>
                            <div class="form-group col-xs-4">
                                <input type="text" id="searchName" class="col-xs-3 form-control"
                                       placeholder="Product name">
                            </div>
                            <div class="form-group col-xs-5">
                                {!! Form::select('category',
\App\Models\ProductCategory::pluck('name','id')->prepend('--- Select Category ---','')
,'',['class'=>'form-control select2 col-xs-3','id'=>'category']) !!}
                            </div>


                                <button type="button" class="btn btn-warning" id="searchProduct">
                                    Search
                                </button>
{{--                            </div>--}}
                        </div>

                        <hr>
                    </div>
                    <hr>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>
                                SKU
                            </th>
                            <th>
                                Name
                            </th>

                            <th>
                                Price ($)
                            </th>
                            <th>
                                Image
                            </th>

                            <th>

                            </th>
                        </tr>
                        </thead>

                        <tbody id="resultProductModal">
                        <tr>
                            <td colspan="4" align="center">
                                No data. Please click "SEARCH" to search by SKU or product name.
                            </td>
                        </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- End Delete File Modal -->
    <style>
        .select2-container--default .select2-selection--single {
            border: 1px solid #e4eaec;
        }
    </style>
@stop
@section(/** @lang text */'javascript')
    <script src="https://johnny.github.io/jquery-sortable/js/jquery-sortable.js"></script>
    <script>
        $('.description').select2({
            tags: true,
            placeholder: "",
            "language": {
                "noResults": function () {
                    return "Enter a description for an item";
                }
            }
        })
        $(function () {
            $('.sorted_table').sortable({
                containerSelector: 'table',
                itemPath: '> tbody',
                itemSelector: 'tr',
                placeholder: '<tr class="placeholder"/>'
            });
        });

    </script>
    <script>
        @if($haveProduct)
        $('#addProducts').show();
        @else
        $('#addProducts').hide();
        @endif

        $('#row_type > select').change(function () {

            if ($('#row_type > select').val() == '{{ \App\Models\ItemTypesModel::WHOLE_BUDGET_PERCENT }}')
                $('#sectionDescription').hide();
            else
                $('#sectionDescription').show();
            $.ajax({
                url: "{{ route('items.detailItemType') }}",
                data: {
                    itemType: $('#row_type > select').val()
                },
                success: function (data) {
                    if (data.data.include_product) {
                        $('#addProducts').show();
                    } else {
                        $('#resultProduct').empty();
                        updateAddedProduct();
                        $('#addProducts').hide();
                    }
                    var formFeild = '';
                    var checkbox = false;
                    $.each(data.data.form, function (key, value) {
                        switch (value.field_type) {
                            case 'checkbox': {
                                checkbox = true;
                                formFeild += '<div class="form-group col-md-2"> ' +
                                    '<label class="control-label ' + (value.required ? "label-require" : "") + '" for="">' +
                                    value.name +
                                    '</label><br/>' +
                                    '<input type="checkbox" name="' + value.slug + '"' +
                                    (value.required ? "required" : "") +
                                    ' class="toggleswitch" data-on="Yes"  data-off="No">' +
                                    '</div>';
                                break;

                            }
                            case 'number': {
                                formFeild += '<div class="form-group col-md-6"> ' +
                                    '<label class="control-label ' + (value.required ? "label-require" : "") + '" for="">' +
                                    value.name + (value.currency ? ' ($)' : '') +
                                    '</label>' +
                                    '<input min="0" max="999999.99" step="0.01" type="number" name="' + value.slug + '"' +
                                    (value.required ? "required" : "") +
                                    ' class="form-control">' +
                                    '</div>';
                                break;

                            }
                            default: {
                                formFeild += '<div class="form-group col-md-6"> ' +
                                    '<label class="control-label ' + (value.required ? "label-require" : "") + '" for="">' +
                                    value.name +
                                    '</label>' +
                                    '<input maxlength="255" type="text" name="' + value.slug + '"' +
                                    (value.required ? "required" : "") +
                                    ' class="form-control">' +
                                    '</div>';
                            }
                        }
                        $('#fieldItemType').html(formFeild);
                        if (checkbox) {
                            $('.toggleswitch').bootstrapToggle();
                        }
                    })
                }
            })

        })

        $("#searchSku,#searchName").on('keyup', function (e) {
            if (e.keyCode === 13)
                search();
        })

        function search() {
            var sku = $('#searchSku').val();
            var category = $('#category').val();
            var name = $('#searchName').val();
            var except = $('#addedProduct').val();
            $.ajax({
                url: '{{ route('products.lists') }}',
                data: {
                    sku: sku,
                    name: name,
                    category: category,
                    except: except,
                    limit:15,
                },
                success: function (data) {
                    $('#modalResultProduct').modal('show');
                    var result = '';
                    if (data.data.length) {
                        $.each(data.data, function (index, value) {
                            var json = JSON.stringify(value).replace("'","&#39;")
                            result +=
                                "<tr id='product_" + value.id + "' data-item='" + json + "'>" +
                                "<td>" + value.SKU + "</td>" +
                                "<td>" + value.name + "</td>" +
                                "<td>" + value.price + "</td>" +
                                "<td><img src='" + value.image_url + "' style='width: 30px' alt=''></td>" +
                                "<td><button type='button' class='btn btn-success' " +
                                "onclick='selectProduct(" + value.id + ")'>Select</button></td>" +
                                "</tr>"
                        })
                    } else
                        result = '<tr><td colspan="4" align="center">No results related to the keyword/SKU you entered.</td></tr>';
                    $('#resultProductModal').empty().html(result)

                }
            })
        }

        $('#searchProduct').click(function () {
            search();
        })

        function selectProduct(id) {

            var resultProduct = '';
            var data = $('#product_' + id).attr('data-item');
            data = JSON.parse(data);
            var castData = [];
            $.each(data, function (index, value) {
                if (value == null) {
                    castData[index] = '';
                } else
                    castData[index] = value;
            })
            resultProduct =
                "<tr id='addedProduct_" + castData['id'] + "' itemid='" + castData['id'] + "' >" +
                "<td>" + castData['SKU'] + "</td>" +
                "<td>" + castData['name'] + "</td>" +
                // "<td>" + castData['description'].replace(/(.{55})..+/, "$1…") + "</td>" +
                "<td>" + castData['description'].slice(0, 55) + (castData['description'].length > 55 ? "..." : "") + "</td>" +
                "<td>" + castData['price'] + "</td>" +
                "<td><img src='" + castData['image_url'] + "' style='width: 30px' alt=''></td>" +

                "<td><input type='number' min='0' max='999999.99' step='0.01' class='form-control' " +
                "name='labor_product_" + castData['id'] + "' value='" + castData['default_labor'] + "'/></td>" +

                "<td><input type='number' min='0' max='999999.99' step='0.01' class='form-control' " +
                "name='hours_product_" + castData['id'] + "' value='" + castData['default_hours'] + "'/></td>" +

                "<td><input type='text' maxlength='255' class='form-control' " +
                "name='glcode_product_" + castData['id'] + "' value='" + castData['gl_code'] + "'/></td>" +

                "<td><button class='btn btn-danger btn-sm' type='button' onclick='removeAddedProduct(" + castData['id'] + ")'><i class=' voyager-x'></i></button></td>" +
                "</tr>"
            $('#resultProduct').append(resultProduct);

            updateAddedProduct();

            $('#product_' + id).remove();
            // $('#modalResultProduct').modal('hide')

        }

        function removeAddedProduct(id) {
            $('#addedProduct_' + id).remove();
            updateAddedProduct();
        }

        function updateAddedProduct() {
            var listProduct = [];
            $('#resultProduct').children('tr').each(function () {
                listProduct.push($(this).attr('itemid'))
            })
            $('#addedProduct').val(listProduct.join())
        }

        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
            return function () {
                $file = $(this).siblings(tag);

                params = {
                    slug: '{{ $dataType->slug }}',
                    filename: $file.data('file-name'),
                    id: $file.data('id'),
                    field: $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                }

                //$('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: ['YYYY-MM-DD']
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function (i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function () {
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if (response
                        && response.data
                        && response.data.status
                        && response.data.status == 200) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function () {
                            $(this).remove();
                        })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
