@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Home Value Index - All Home - Metro')
@section('breadcrumbs')
@endsection
@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">Home Value Index Download
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="">Data Type</label>
                                <br>
                                <select class="form-control"
                                        id="hvi">
                                    <option
                                        value="ZHVI All Homes (SFR, Condo/Co-op) Time Series, Smoothed, Seasonally Adjusted($)">
                                        ZHVI All Homes (SFR, Condo/Co-op) Time Series, Smoothed, Seasonally Adjusted($)
                                    </option>
                                    <option value="ZHVI All Homes (SFR, Condo/Co-op) Time Series, Raw, Mid-Tier ($)">
                                        ZHVI All Homes (SFR, Condo/Co-op) Time Series, Raw, Mid-Tier ($)
                                    </option>
                                    <option value="ZHVI All Homes- Top Tier Time Series ($)">ZHVI All Homes- Top Tier
                                        Time Series ($)
                                    </option>
                                    <option value="ZHVI All Homes- Bottom Tier Time Series ($)">ZHVI All Homes- Bottom
                                        Tier Time Series ($)
                                    </option>
                                    <option value="ZHVI Single-Family Homes Time Series ($)">ZHVI Single-Family Homes
                                        Time Series ($)
                                    </option>
                                    <option value="ZHVI Condo/Co-op Time Series ($)">ZHVI Condo/Co-op Time Series ($)
                                    </option>
                                    <option value="ZHVI 1-Bedroom Time Series ($)">ZHVI 1-Bedroom Time Series ($)
                                    </option>
                                    <option value="ZHVI 2-Bedroom Time Series ($)">ZHVI 2-Bedroom Time Series ($)
                                    </option>
                                    <option value="ZHVI 3-Bedroom Time Series ($)">ZHVI 3-Bedroom Time Series ($)
                                    </option>
                                    <option value="ZHVI 4-Bedroom Time Series ($)">ZHVI 4-Bedroom Time Series ($)
                                    </option>
                                    <option value="ZHVI 5+ Bedroom Time Series ($)">ZHVI 5+ Bedroom Time Series ($)
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Geography</label>
                                <br>
                                <select class="form-control"
                                        id="median-home-value-zillow-home-value-index-zhvi-dropdown-2"
                                        data-post-name="median-home-value-zillow-home-value-index-zhvi">
                                    <option value="" disabled="" selected="">Choose one...</option>
                                    <option
                                        value="https://files.zillowstatic.com/research/public_v2/zhvi/Metro_zhvi_uc_sfrcondo_tier_0.33_0.67_sm_sa_mon.csv?t=1618215835">
                                        Metro &amp; U.S.
                                    </option>
                                    <option
                                        value="https://files.zillowstatic.com/research/public_v2/zhvi/State_zhvi_uc_sfrcondo_tier_0.33_0.67_sm_sa_mon.csv?t=1618215835">
                                        State
                                    </option>
                                    <option
                                        value="https://files.zillowstatic.com/research/public_v2/zhvi/County_zhvi_uc_sfrcondo_tier_0.33_0.67_sm_sa_mon.csv?t=1618215835">
                                        County
                                    </option>
                                    <option
                                        value="https://files.zillowstatic.com/research/public_v2/zhvi/City_zhvi_uc_sfrcondo_tier_0.33_0.67_sm_sa_mon.csv?t=1618215835">
                                        City
                                    </option>
                                    <option
                                        value="https://files.zillowstatic.com/research/public_v2/zhvi/Zip_zhvi_uc_sfrcondo_tier_0.33_0.67_sm_sa_mon.csv?t=1618215835">
                                        ZIP Code
                                    </option>
                                    <option
                                        value="https://files.zillowstatic.com/research/public_v2/zhvi/Neighborhood_zhvi_uc_sfrcondo_tier_0.33_0.67_sm_sa_mon.csv?t=1618215835">
                                        Neighborhood
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop


@section(/** @lang text */'javascript')
    <script>
        $('#hvi').change(function(){

        })
    </script>
@stop
