@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Home Value Index - All Home - Metro')
@section('breadcrumbs')
@endsection
@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-upload"></i> Home Value Index - All Home - Zipcode
        </h1>
        {{--            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">--}}
        {{--                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>--}}
        {{--            </a>--}}
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#importModal">
            Import
        </button>
    </div>
@stop

@section('content')
    <div class="modal fade" id="importModal" tabindex="-1" aria-labelledby="importModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Home Value Index - All Home - Zipcode</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['url'=>route('admin.hvi.allhome.zipcode.import'),'files'=>'true']) !!}
                <div class="modal-body">
                    <div class="form-group">
                        <label class="label-require" for="">Choose CSV/XLSX file</label>
                        <input type="file" accept=".xlsx,.csv" name="import" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Import</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        {{--                        @if ($isServerSide)--}}
                        <form method="get" class="form-search">
                            <div id="search-input">
                                <div class="col-md-2 pr-0 pl-0">
{{--                                    <select id="search_key" name="key" style="width: 100%">--}}
{{--                                        @foreach($searchSelect as $key => $name)--}}
{{--                                            <option value="{{ $key }}"--}}
{{--                                                    @if($search->key == $key || (empty($search->key) && $key ==--}}
{{-- $defaultSearchKey)) selected @endif>{{ $name }}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
                                    {!! Form::select('key',$searchSelect,Request::input('key','regionid'),['class'=>'form-control']) !!}
                                </div>
                                <div class="col-3">
                                    <select id="filter" name="filter" class="form-control">
                                        <option value="contains" @if(Request::input('filter') == "contains") selected @endif>
                                            contains
                                        </option>
                                        <option value="equals" @if(Request::input('filter') == "equals") selected @endif>=
                                        </option>
                                    </select>
                                </div>
                                <div class="input-group col-md-12">
                                    <input type="text" class="form-control"
                                           placeholder="{{ __('voyager::generic.search') }}" name="s"
                                           value="{{ Request::input('s') }}">
                                    <span class="input-group-btn">
                                                                    <button class="btn btn-info btn-lg" type="submit">
                                                                        <i class="voyager-search"></i>
                                                                    </button>
                                                                </span>
                                </div>
                            </div>
                        </form>
                        {{--                        @endif--}}
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>

                                    <th>
                                        RegionID
                                    </th>
                                    <th>
                                        SizeRank
                                    </th>
                                    <th>
                                        RegionName
                                    </th>
                                    <th>
                                        RegionType
                                    </th>
                                    <th>
                                        StateName
                                    </th>
                                    <th>
                                        State
                                    </th>
                                    <th>
                                        City
                                    </th>
                                    <th>
                                        Metro
                                    </th>
                                    <th>
                                        CountyName
                                    </th>


                                    <th class="actions text-center dt-not-orderable">
                                        {{ __('voyager::generic.actions') }}
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $datum)
                                    <tr>
                                        <td>
                                            {{ $datum->regionid }}
                                        </td>
                                        <td>
                                            {{ $datum->sizerank }}
                                        </td>
                                        <td>
                                            {{ $datum->regionname }}
                                        </td>
                                        <td>
                                            {{ $datum->regiontype }}
                                        </td>
                                        <td>
                                            {{ $datum->statename }}
                                        </td>
                                        <td>
                                            {{ $datum->state }}
                                        </td>
                                        <td>
                                            {{ $datum->city }}
                                        </td>
                                        <td>
                                            {{ $datum->metro }}
                                        </td>
                                        <td>
                                            {{ $datum->countyname }}
                                        </td>

                                        <td class="no-sort no-click bread-actions" align="center">
                                            <a href="{{ route('admin.hvi.allhome.zipcode.view',$datum->id) }}" class="btn btn-primary">
                                                <i class="voyager-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {!! $data->appends(Request::input())->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop


@section(/** @lang text */'javascript')

@stop
