@extends('voyager::master')

@section('page_title', __('voyager::generic.view').'  Home Value Index - All Home - Metro')
@section('breadcrumbs')
@endsection
@section('page_header')
    <h1 class="page-title">
        {{ __('voyager::generic.viewing') }} Home Value Index - All Home - Metro
        &nbsp;

        <a href="{{ route('admin.hvi.allhome.metro.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered" style="padding-bottom:5px;">
                    <!-- form start -->
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>

                            <th>
                                RegionID
                            </th>
                            <th>
                                SizeRank
                            </th>
                            <th>
                                RegionName
                            </th>
                            <th>
                                RegionType
                            </th>
                            <th>
                                StateName
                            </th>

                        </tr>

                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                {{ $datum->regionid }}
                            </td>
                            <td>
                                {{ $datum->sizerank }}
                            </td>
                            <td>
                                {{ $datum->regionname }}
                            </td>
                            <td>
                                {{ $datum->regiontype }}
                            </td>
                            <td>
                                {{ $datum->statename }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <h3>
                        History Home Index Value
                    </h3>
                    <?php
                        $history = json_decode($datum->value,true);
                    ?>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>

                            <th>
                                Date
                            </th>
                            <th>
                                Value
                            </th>
                        </tr>

                        </thead>
                        <tbody>
                        @foreach($history as $date=>$value)
                        <tr>
                            <td>
                                {{ $date }}
                            </td>
                            <td>
                                {{ $value }}
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
@stop

@section('javascript')
@stop
